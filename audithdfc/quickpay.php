<?php 
session_start();
$yr = date('y');

define('WP_USE_THEMES', true);
include('../configuration.php');
require('../wp-config.php');
date_default_timezone_set("Asia/Calcutta");
get_header();

?>
<style>
h2.panel-heading{
font-weight:bold;	
}

</style>

<div id="primary" class="content-area">
  <?php //if($smof_data['show_page_title_blog'] || $smof_data['show_page_breadcrumb_blog']) { ?>
  <div class="header-site-wrap">
    <div class="container container-md-height">
      <div class="row row-md-height cs-titile-bar">
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
          <h2 class="page-title"> Quick Pay
            <?php //if($smof_data['show_page_title_blog']) echo cshero_page_title(); ?>
          </h2>
        </div>
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
          <div id="breadcrumb" class="cs-breadcrumb">
            <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
           Quick Pay </div>
        </div>
      </div>
    </div>
  </div>
  <?php //} ?>
  <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
        <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
      </div>
      <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
        <div class="big_title">
          <h3>Pay Online </h3>
          <p></p>
        </div>
       <!-- <div style="text-align:right">
          <h4> <span style="color: red">Existing User Click Here</span></h4>
        </div>-->
        <main id="main" class="site-main" role="main">
        
        
        <div class="col-md-12" style="margin-top:7%;">
				<div class="panel panel-default">
					<div class="panel-body tabs">
                    
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab"><b style="color:#000">Online Payment</b></a></li>
							<li><a href="#tab2" data-toggle="tab"><b style="color:#000">Check Dropbox Location</b></a></li>
							<li><a href="#tab3" data-toggle="tab"><b style="color:#000">Cash Deposit </b></a></li>
						</ul>
		
                         <div class="tab-content">
                         
                         
           <!------------------------------Start Tab-1------------------------>                 
							<div class="tab-pane fade in active" id="tab1">
                           
                          <form class="form-horizontal" action="payment.php" method="post" >           
                                 <div class="col-md-6 col-md-offset-3"> 
                                 <div class="panel-heading" style="color:#f00;">
                              <h4>Please enter the following information :</h4>
                              <br>
                              <p class="panel-heading" style="color:#000;"> CRN Number : (Please Enter Valid 11 Digit CRN Number)</p>
                                </div>        
              
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">11 DIGIT CRN NO</label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="crn_no" name="crn_no" maxlength="11"  onfocus="crnAjax(this.value);" onkeyup="crnAjax(this.value);" onblur="crnAjax(this.value);"  >
                   <input type="hidden" name="rands" value="<?php echo rand('1111111111','9999999999'); ?>" />
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              <div id="crnbox"></div>
              
              
             <!-- <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label"  for="connectiontype">Name  </label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="name" name="name" >
                </div></div>
              </div>
               <div class="clearfix"></div>
               
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">Email  </label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="email" id="email" name="email" >
                </div></div>
              </div>
               <div class="clearfix"></div>
               
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label"  for="connectiontype">Mobile No.  </label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="mobile" name="mobile" maxlength="10" >
                </div>  </div>
              </div>
               <div class="clearfix"></div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label"  for="connectiontype">Amount  </label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="amount" name="amount" >
                </div></div>
              </div>
               <div class="clearfix"></div>-->
              
              
           
          </div>
    
                               <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-3">                                 <div class="form-group" align="center">
                                <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                                </div>
                                </div>  -->  
                                     
                            </form>         
                                
                                 
                                </div>
          <!------------------------------End Tab-1------------------------>         
           <!------------------------------Start Tab-2------------------------>         
							<div class="tab-pane fade" id="tab2">
							  <br />
                                 <div class="col-md-12">
                                <div class="col-md-6">
                                   <div class="form-group">
									<h2 class="panel-heading" align="center">LUCKNOW</h2>
                                  
 1. Eldeco Elegance   -   Vibhuti khand , Gomtinagar<br />
                                        2. Omax Height       -   Vibhuti Khand, Gomtinagar<br />
                                        3. GGL CNG Station   -   Vibhuti Khand, Gomtinagar<br />
                                        4. Awadh School      -   Vipul Khand, Gomtinagar<br />
                                        5. Shipra Apt.       -    Ashiyana Colony<br />
                                        6. AWHO Colony       -   Tyagi Vihar<br />
                                        7. GGL CNG Station   -   Vrindavan Sec-6C,                                  
                                  <!--  1. 	Indra Automobiles PP-1, Sector-M, Ashiana, Lucknow<br />
                                    2. 	DRS (District Regulating System) near Awadh School Boundary, Vipul Khand, Gomti Nagar, Lucknow.<br /> 3. 	Commercial Plot No.-4, Vibhuti Khand, Gomti Nagar Lucknow.<br />
                                    4. 	CSI Tower, Vipin Khand, Gomti Nagar, Lucknow-->
                                    </div>
                                </div>
                            <div class="col-md-6"><div class="form-group">
									<h2 class="panel-heading" align="center">Agra</h2>
                                    
                                    1. 	Friends Ashiyana (Opp. Khandri Campus). <br />  
                                     2. 	Emerald Square Apt. (Near Hanuman Crossing). <br />  
                                     3. 	Old Siddharth Apt. (Near Civil Court).<br />  
                                      	4. 	Pushpanjali Tower Ext. (Opp. Delhi Gate)<br /> 
                                        5. 	HIG Flats (Sanjay Place). <br /> 
                                         	6. 	CNG Mother Station (ISBT Compound, T.P Nagar)
								</div></div>
                                </div>
                                
							
							</div>
                                    
         <!------------------------------End Tab-2------------------------>     
        <!----------------------------Start Tab-3------------------------>    
							<div class="tab-pane fade" id="tab3">
                                <br />
                                 <div class="col-md-12">
                                <div class="col-md-6">
                                   <div class="form-group">
									<h2 class="panel-heading" align="center">LUCKNOW</h2>
                                    
                                    YES Bank Limited , Tulsi Complex , 12 Rani Laxmibai Marg , Hazratganj , Lucknow 
                                    </div>
                                </div>
                            <div class="col-md-6"><div class="form-group">
									<h2 class="panel-heading" align="center">Agra</h2>
                                    
                                    YES Bank Limited , Ground + First Floor, 116/8 Sanjay Palace, Agra       
								</div></div>
                                </div>
                                
							</div>
                            
                       <!------------------------------End Tab-3------------------------>      
                            
						</div>
                        
                     
					</div>
				</div><!--/.panel-->
			</div>
        
        
        
        </main>
        <!-- #main --> 
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
        <?php dynamic_sidebar( 'cshero-widget-right' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- #primary -->


<script>
function crnAjax(str) {
		
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{//alert(str1);
		document.getElementById("crnbox").innerHTML = xmlhttp.responseText; 
	    //alert(xmlhttp.responseText);
		}
		}
        xmlhttp.open("GET", "crnAjax.php?crn_no="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
}

</script>




<?php get_footer();  ?>
