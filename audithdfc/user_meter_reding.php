<?php 


session_start();
$yr = date('y');

define('WP_USE_THEMES', true);
include('../configuration.php');
require('../wp-config.php');
date_default_timezone_set("Asia/Calcutta");
get_header();


?>
     <style>
   @media screen and (-webkit-min-device-pixel-ratio:0){
  .padd{
      text-align:left;
      padding: 4px 10px !important;
  }}

  @-moz-document url-prefix(){
  .padd{
      text-align:left;
      padding: 0px 10px !important;
  } }


.msg{
color:#F00; 
}

option {
  font-size: 14px;
  padding: 5px;
}

</style>


    <script type="text/javascript">


/*
        $(function () {
            $(".date").datepicker({
               dateFormat: "dd/mm/yy" 
            });
        }); */

 function validate(nextFieldID) {
            var intFlag = 0;
            var strErrMsg = "Please complete the following field(s):\n\n";
            var dtDate = document.getElementById(nextFieldID).value // tbDate = name of text box
            var currentDate =  new Date()
            if (dtDate > currentDate) {
                strErrMsg = strErrMsg + "your custom error message \n";
                intFlag++;
            }

            if (intFlag != 0) {
                alert(strErrMsg);
                return false;
            }
            else
                return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

      </script>

<div id="primary" class="content-area">
  <?php //if($smof_data['show_page_title_blog'] || $smof_data['show_page_breadcrumb_blog']) { ?>
  <div class="header-site-wrap">
    <div class="container container-md-height">
      <div class="row row-md-height cs-titile-bar">
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
          <h2 class="page-title"> User Meter Reading
            <?php //if($smof_data['show_page_title_blog']) echo cshero_page_title(); ?>
          </h2>
        </div>
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
          <div id="breadcrumb" class="cs-breadcrumb">
            <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
             User Meter Reading</div>
        </div>
      </div>
    </div>
  </div>
  <?php //} ?>
  <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
        <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
      </div>
      <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
        <div class="big_title">
          <h3>User Meter Reading | Green Gas Limited</h3>
          <p></p>
        </div>


        <main id="main" class="site-main" role="main">
             <?php 
                if($_SESSION['success']==1){ echo "<div class='alert alert-success alert-dismissable fade in' id='alert-success' >
                      <a href='' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                      Successfully Done..
                      </div>";
                       unset($_SESSION['success']);
                      //session_destroy(); 
                     
                   
                     } 
if($_SESSION['success']==2){ echo "<div class='alert alert-success alert-dismissable fade in' id='alert-success' >
                      <a href='' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                      Error Please Try Again!
                      </div>";
                     unset($_SESSION['success']);
                     } 
                     ?>




        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" role="form" onsubmit="return complainValid();">
         <div class="col-md-8 col-md-offset-2">
            <div>


              <?php
             if(isset($_REQUEST['submit'])){      
  
                $crn_no = strtoupper($_REQUEST['crn_no']);
                $contact = $_REQUEST['contact'];
                $meter = $_REQUEST['meter1'].$_REQUEST['meter2'].$_REQUEST['meter3'].$_REQUEST['meter4'].$_REQUEST['meter5'].$_REQUEST['meter6'].$_REQUEST['meter7'].$_REQUEST['meter8'];
                $reason = $_REQUEST['reason'];
                $name = $_REQUEST['name'];
                $email = $_REQUEST['email'];
                $premeter = $_REQUEST['premeter'];
                $mobile = $_REQUEST['phone'];
                $i=0;
                //$bill_cycle = $_REQUEST['cycle'];
                $date = $_REQUEST['date'];
              
                $uploadOk = 1;
                //$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    $imageFileType = pathinfo($_FILES["image"]["name"],PATHINFO_EXTENSION);


                // Check if image file is a actual image or fake image
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check !== false) {
                  //    echo "File is an image - " . $check["mime"] . ".";
                  $uploadOk = 1;
                  } else {
                  echo "<div class='alert alert-danger alert-dismissable fade in' id='alert-danger' >
                      <a href='' class='close' data-dismiss='alert' aria-label='close'>&times;</a>File is not an image.</div>";
                  $uploadOk = 0;
                      }

                // Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF" ) {
                    echo "<div class='alert alert-danger alert-dismissable fade in' id='alert-danger' >
                      <a href='' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
                $uploadOk = 0;
                }
                  // Check file size
                elseif ($_FILES["image"]["size"] > 100000) {
               echo "<div class='alert alert-danger alert-dismissable fade in' id='alert-danger' >
                      <a href='' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Sorry, your file is too large(Max Size 98kb Only).</div>";
                $uploadOk = 0;
              // if everything is ok, try to upload file
                } else {
            /*if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

              //$pic = $crn_no."/".$date."/".$_FILES["image"]["name"]; 
              $pic = $crn_no."/".$date."/".$_FILES["image"]["name"]; */
             
               $sql = mysql_query("INSERT INTO `user_meter_reading` (`crn_no`, `name`, `email`, `contact_person`, `mobile`, `image`, `pre_mtr_read`, `meter_reading`, `reason`, `date`) VALUES ('$crn_no','$name','$email','$contact','$mobile','$pic','$premeter','$meter','$reason','$date')");
               
               $last_id = mysql_insert_id();

            if($sql){
                 $path = "media/meter_reading/".$crn_no."/".$last_id."/";
                $target = mkdir('../'.$path, 0777, true);
                $target_file = '../'.$path . basename($_FILES["image"]["name"]);
             
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

                 $pic = $crn_no."/".$last_id."/".$_FILES["image"]["name"]; 

              $meter_update_img = mysql_query("UPDATE `user_meter_reading` set image='".$path.$_FILES["image"]["name"]."' where id='".$last_id."'");

               }


             /*echo "<div class='alert alert-success alert-dismissable fade in' id='alert-success' >
                      <a href='' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                      Successfully Done..
                      </div>";*/
                  $_SESSION['success'] = 1;
            echo "<script>window.location.href = 'user_meter_reding.php';</script>";
  //header('location: ggluser/user_meter_reding.php');
               

                    }else{
                       $_SESSION['success'] = 2;
                        echo "<script>window.location.href = 'user_meter_reding.php';</script>";
  //header('location: ggluser/user_meter_reding.php');
               
                          }
                        }
                      //}
                    }
                  ?> 
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-4" style="text-align:left;">Customer No.: <span style="color: red;">*</span></label>
                <div class="col-sm-3">
                   <input class="form-control" type="text" name="crn_no" id="crn_no" maxlength="11" onkeyup="customer(this.value)" style="text-align:left;" required="">
                </div>

              </div>
               <div id="getval">


                </div>


                  <div class="form-group form-group-sm"  id="hidedata1" style="display:none;">
                <label class="col-sm-4 control-label" style="text-align:left;"  for="meterreading">Meter Reading:<span style="color: red;">*</span> </label>
                <div class="col-sm-8 meter">
                   <input class="form-control move" name="meter1" id="meter1" onkeypress="return isNumberKey(event)" type="text" maxlength="1"  required="">
                   <input class="form-control move" name="meter2" id="meter2" onkeypress="return isNumberKey(event)"  type="text" maxlength="1" required="">
                   <input class="form-control move" name="meter3" id="meter3" onkeypress="return isNumberKey(event)"  type="text" maxlength="1"  required="">
                   <input class="form-control move" name="meter4" id="meter4"  onkeypress="return isNumberKey(event)"  type="text" maxlength="1" required="">
                   <input class="form-control move" name="meter5" id="meter5" onkeypress="return isNumberKey(event)"  type="text" maxlength="1"  required="">
                   <input class="form-control red move" name="meter6" id="meter6" onkeypress="return isNumberKey(event)" type="text" maxlength="1"  required="">
                   <input class="form-control red move" name="meter7" id="meter7" onkeypress="return isNumberKey(event)"  type="text" maxlength="1"  required="">
                   <input class="form-control red move" name="meter8" id="meter8" onkeypress="return isNumberKey(event)"  type="text" maxlength="1"  required="">
                    <span id="msgmeter" class="msg"></span>
                </div>
              </div>
               <div class="form-group form-group-sm"  id="hidedata2" style="display:none;">
                <label class="col-sm-4 control-label" style="text-align:left;"  for="meterreading">Reason :<span style="color: red;">*</span> </label>
                <div class="col-sm-8">
                    <select class="form-control" type="text" name="reason" id="reason"  required="">
                  <option selected="selected" value="">--SELECT--</option>
                  <option value="Assesed reading">Assesed reading</option>
                  <option value="Flat generally not occupied">Flat generally not occupied</option>
                  <option value="Not available at home during the office hours">Not available at home during the office hours</option>
                  <option value="out of station">out of station</option>
                  <option value="Recieved call from AGL">Recieved call from AGL</option>
                  </select>
                      <span id="msgcomplain_type_id" class="msg"></span>
                </div>
              </div>
              
              <div class="form-group form-group-sm"  id="hidedata3" style="display:none;">
                <label class="col-sm-4 control-label" style="text-align:left;"  for="meterreading">Reading Date: <span style="color: red;">*</span></label>
                <div class="col-sm-3"  id="date">
                   <input type="date" class="form-control datepicker" name="date" id="date" style="text-align:left;" required="">
                </div>
              </div>

                  <div class="form-group form-group-sm"  id="hidedata4" style="display:none;">
              <div class="checkbox">
                  <label>
                      <input value="" type="checkbox" required="">I do hereby declare that I have read all the details mentioned above before submitting Domestic PNG Meter Readings.
                  </label>
              </div>                                                                                                                                            
            
          </div>
        


               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-3"  id="hidedata5" style="display:none;">
           
              <div class="form-group" align="center">
                <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                <input type="reset" name="reset" class="btn btn-primary" value="Reset" />
            
              </div>
              
          </div>
            </div>

  


           </form>
        </main><?php /*   }
      } */?>
        <!-- #main --> 
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
        <?php dynamic_sidebar( 'cshero-widget-right' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- #primary -->

<?php get_footer();  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>


function isletter(txt)
{
  var iChars = "!@#$^&*+=[]\\\/{}|\":<>?0123456789";
  var chk =1;
  for (var i = 0; i < txt.length; i++) {
    if (iChars.indexOf(txt.charAt(i)) != -1) {
      chk=0;
        }
    }
  if(chk){
  return true;
  }else{
  return false;
  }
}

function chkNumeric(val,id){
      for(i=0;i<val.length;i++){
      if(!isNaN(val.substring(1,val.length)) && !isNaN(val)){
      }else{
      document.getElementById(id).value = val.substring(0,val.length-1);
      }
    }
  
  }

function isEmail(text)
{
  
  var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  
  //"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
  var regex = new RegExp( pattern );
  return regex.test(text);
  
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true; 
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {       
     IsNumber = false;
         }
      }
     return IsNumber;
}

function numericWithoutPoint(sText)
{
 var ValidChars = "0123456789";
 var IsNumber=true; 
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {       
     IsNumber = false;
         }
      }
     return IsNumber;
}


function complainValid() {
  var chk=1;
  
  
  if(document.getElementById("name").value==''){
    document.getElementById("msgname").innerHTML = "Required Field";
    chk=0;    
  }
  else if(!isletter(document.getElementById("name").value)){
      document.getElementById("msgname").innerHTML = "Invaild Text";  
    chk=0;    
  }
  else{
      document.getElementById("msgname").innerHTML = "";    
  }
  
  
  
  if(document.getElementById("mobile").value==''){
    document.getElementById("msgmobile").innerHTML = "Required Field";
    chk=0;    
  }
  else if(!numericWithoutPoint(document.getElementById("mobile").value)){
      document.getElementById("msgmobile").innerHTML = "Invaild Number";  
    chk=0;    
  }
  else{
      document.getElementById("msgmobile").innerHTML = "";    
  }
  
  if(document.getElementById("email").value==''){
    document.getElementById("msgemail").innerHTML = "Required Field";
    chk=0;    
  }
  else if(!isEmail(document.getElementById("email").value)){
      document.getElementById("msgemail").innerHTML = "Invaild Email";  
    chk=0;    
  }
  else{
      document.getElementById("msgemail").innerHTML = "";   
  }
  
  
  if(document.getElementById("meter").value==''){
    document.getElementById("msgmeter").innerHTML = "Required Field";
    chk=0;    
  }
  else{
      document.getElementById("msgmeter").innerHTML = "";   
  }
  
  if(document.getElementById("complain_type_id").value==''){
    document.getElementById("msgcomplain_type_id").innerHTML = "Required Field";
    chk=0;    
  }
  else{
      document.getElementById("msgcomplain_type_id").innerHTML = "";    
  }
  
  
  if(document.getElementById("message").value==''){
    document.getElementById("msgmessage").innerHTML = "Required Field";
    chk=0;    
  }
  else{
      document.getElementById("msgmessage").innerHTML = "";   
  }
  
  if(chk){
    return true;
  }else{
    return false;
  }
    
  
  
}



$(document).ready(function() {
    
    $('#feedback').on('change', function()
    { 
      if (this.value == 'PNG') {//alert(this.value);
      $("#crnfullbox").css('display','block');
      
      } else {
      $("#crnfullbox").css('display','none');
      
      } 
    });
  
  
  $('input:radio[name="crn_avail"]').change(function() {
  if ($(this).val() == 'Yes') {
   $("#crnbox").css('display','block');
   
  } else {
    $("#crnbox").css('display','none');
  
  }
});
});


      
$(".move").keyup(function () {
    if (this.value.length == this.maxLength) {
      var $next = $(this).next('.move');
      if ($next.length)
          $(this).next('.move').focus();
      else
          $(this).blur();
    }
});



//AJAX for data
function customer(str) {
 var xmlhttp; 
  if (window.XMLHttpRequest)
  { // code for IE7+, Firefox, Chrome, Opera, Safari 
  xmlhttp=new XMLHttpRequest();
  }
  else
  { // code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  { 
  if (xmlhttp.readyState==4)
  {
  document.getElementById("getval").innerHTML = xmlhttp.responseText; 
 // var vald = document.getElementById("getval").innerHTML;
 //alert(xmlhttp.responseText);
 //var dd = "CRN No. Does Not Match";
 //   if(xmlhttp.responseText==2){
    if(str.length == 11){
		document.getElementById("hidedata1").style.display = 'block';  
		document.getElementById("hidedata2").style.display = 'block';  
    document.getElementById("hidedata3").style.display = 'block';  
    document.getElementById("hidedata4").style.display = 'block'; 
    document.getElementById("hidedata5").style.display = 'block'; 
		}
		else{			 
		document.getElementById("hidedata1").style.display = 'none';  
		document.getElementById("hidedata2").style.display = 'none';  
    document.getElementById("hidedata3").style.display = 'none'; 
    document.getElementById("hidedata4").style.display = 'none'; 
    document.getElementById("hidedata5").style.display = 'none'; 			
		}
  }
  }
  xmlhttp.open("GET", "user_meterAJAX.php?crn_no="+str, true);
  //xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
  xmlhttp.send();
}


</script>


 <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );
  </script>-->



  <link rel="stylesheet" type="text/css" href="http://192.168.1.30/wp/greengas/ggladmin/assets/date_picker/jquery.datetimepicker.css"/>  
<script src="http://192.168.1.30/wp/greengas/ggladmin/assets/date_picker/jquery.js"></script>
<script src="http://192.168.1.30/wp/greengas/ggladmin/assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
$('.datepicker').datetimepicker({
  yearOffset:0,
  lang:'ch',
  timepicker:false,
  format:'Y-m-d',
  formatDate:'Y-m-d',
  //minDate:'-1970/01/02', // yesterday is minimum date
  //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

</script> 