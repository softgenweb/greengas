<?php 
session_start();
$yr = date('y');

define('WP_USE_THEMES', true);
include('../configuration.php');
require('../wp-config.php');
date_default_timezone_set("Asia/Calcutta");
get_header();

?>
<style>
h2.panel-heading{
font-weight:bold;	
}

</style>

<style>

.msg{
color:#F00;	
}

option {
  font-size: 14px;
  padding: 5px;
}

</style>

<div id="primary" class="content-area">
  <?php //if($smof_data['show_page_title_blog'] || $smof_data['show_page_breadcrumb_blog']) { ?>
  <div class="header-site-wrap">
    <div class="container container-md-height">
      <div class="row row-md-height cs-titile-bar">
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
          <h2 class="page-title"> Update Contact Details
            <?php //if($smof_data['show_page_title_blog']) echo cshero_page_title(); ?>
          </h2>
        </div>
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
          <div id="breadcrumb" class="cs-breadcrumb">
            <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
           Update Contact Details </div>
        </div>
      </div>
    </div>
  </div>
  <?php //} ?>
  <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
        <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
      </div>
      <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
        <div class="big_title">
          <h3>Update Contact Details </h3>
          <p></p>
        </div>
       <!-- <div style="text-align:right">
          <h4> <span style="color: red">Existing User Click Here</span></h4>
        </div>-->
        <main id="main" class="site-main" role="main">
        
        
        <div class="col-md-12" style="margin-top:7%;">
				<div class="panel panel-default">
					<div class="panel-body tabs">
                    
                         
                         
           <!------------------------------Start Tab-1------------------------>                 
							<div class="tab-pane fade in active">
                           
                          <form class="form-horizontal" action="update_contact_details_otp.php" method="post" onsubmit="return formValid();"   >           
                                 <div class="col-md-6 col-md-offset-3"> 
                                 <div class="panel-heading" style="color:#f00;">
                              <h4>Please enter the following information :</h4>
                             <p class="panel-heading" style="color:#000;"> CRN Number : (Please Enter Valid 11 Digit CRN Number)</p>
                                </div>        
              
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">11 DIGIT CRN NO</label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="crn_no" name="crn_no" maxlength="11"  onkeyup="crnAjax(this.value);" >
                  <!-- <input class="form-control" type="text" id="crn_no" name="crn_no" maxlength="3" style="width:60px;float:left;margin-right:10px;"><input class="form-control" type="text" id="crn_no1" name="crn_no1" maxlength="8" style="width:240px;float:left;">-->
                   <span id="msgcrn_no" class="msg"></span>
                  <!-- <span id="msgcrn_no1" class="msg"></span>-->
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">Name</label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="name" name="name" >
                   <span id="msgname" class="msg"></span>
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">Mobile No.</label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="mobile" name="mobile" >
                   <span id="msgmobile" class="msg"></span>
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">E-mail ID</label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="email" name="email" >
                   <span id="msgemail" class="msg"></span>
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">Address</label>
                <div class="col-sm-8"><div class="form-group">
                   <input class="form-control" type="text" id="address" name="address" required >
                   <span id="msgaddress" class="msg"></span>
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-4 control-label" for="connectiontype">Address Correct</label>
                <div class="col-sm-8"><div class="form-group">
                  <span style="width:100px;float:left;margin-right:20px;">Yes <input type="radio" id="address_correct" name="address_correct" value="Yes" checked="checked"></span>
                  <span style="width:100px;float:left;"> No <input  type="radio" id="address_correct" name="address_correct" value="No"  ></span>
                </div> </div>
              </div>
              <div class="clearfix"></div>
              
              <div id="crnbox"></div>
              
         
              
              
           
          </div>
    
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-3">                                 <div class="form-group" align="center">
                                <input type="hidden" name="otp" value="<?php echo rand(1000,9999); ?>" />
                                <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                                </div>
                                </div>    
                                     
                            </form>         
                                
                                 
                                </div>
          <!------------------------------End Tab-1------------------------>         
            
                      
                        
                     
					</div>
				</div><!--/.panel-->
			</div>
        
        
        
        </main>
        <!-- #main --> 
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
        <?php dynamic_sidebar( 'cshero-widget-right' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- #primary -->


<script>


function isletter(txt)
{
	var iChars = "!@#$^&*+=[]\\\/{}|\":<>?0123456789";
	var chk	=1;
	for (var i = 0; i < txt.length; i++) {
    if (iChars.indexOf(txt.charAt(i)) != -1) {
   		chk=0;
        }
    }
	if(chk){
	return true;
	}else{
	return false;
	}
}

function chkNumeric(val,id){
  		for(i=0;i<val.length;i++){
			if(!isNaN(val.substring(1,val.length)) && !isNaN(val)){
			}else{
			document.getElementById(id).value = val.substring(0,val.length-1);
			}
		}
	
  }

function isEmail(text)
{
	
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	//"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
	var regex = new RegExp( pattern );
	return regex.test(text);
	
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}

function numericWithoutPoint(sText)
{
 var ValidChars = "0123456789";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}


function formValid() {
	var chk=1;
	
	
	if(document.getElementById("name").value==''){
		document.getElementById("msgname").innerHTML = "Required Field";
		chk=0;		
	}
	else if(!isletter(document.getElementById("name").value)){
	    document.getElementById("msgname").innerHTML = "Invaild Text";	
		chk=0;		
	}
	else{
	    document.getElementById("msgname").innerHTML = "";		
	}
	
	
	
	if(document.getElementById("mobile").value==''){
		document.getElementById("msgmobile").innerHTML = "Required Field";
		chk=0;		
	}
	else if(!numericWithoutPoint(document.getElementById("mobile").value)){
	    document.getElementById("msgmobile").innerHTML = "Invaild Number";	
		chk=0;		
	}
	else{
	    document.getElementById("msgmobile").innerHTML = "";		
	}
	
	if(document.getElementById("email").value==''){
		document.getElementById("msgemail").innerHTML = "Required Field";
		chk=0;		
	}
	else if(!isEmail(document.getElementById("email").value)){
	    document.getElementById("msgemail").innerHTML = "Invaild Email";	
		chk=0;		
	}
	else{
	    document.getElementById("msgemail").innerHTML = "";		
	}
	
	
	if(document.getElementById("crn_no").value==''){
		document.getElementById("msgcrn_no").innerHTML = "Required Field";
		chk=0;		
	}
	/*else if(!isletter(document.getElementById("crn_no").value)){
	    document.getElementById("msgcrn_no").innerHTML = "Invaild Text";	
		chk=0;		
	}*/
	else{
	    document.getElementById("msgcrn_no").innerHTML = "";		
	}
	
	/*if(document.getElementById("crn_no1").value==''){
		document.getElementById("msgcrn_no1").innerHTML = "Required Field";
		chk=0;		
	}
	else if(!numericWithoutPoint(document.getElementById("crn_no1").value)){
	    document.getElementById("msgcrn_no1").innerHTML = "Invaild Text";	
		chk=0;		
	}
	else{
	    document.getElementById("msgcrn_no1").innerHTML = "";		
	}*/
	
	
	
	
	if(chk){
		return true;
	}else{
		return false;
	}
		
	
	
}


</script>

<script>
function crnAjax(str) {
		
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{//
		
		var data = xmlhttp.responseText;
		var result = data.split('#');
		document.getElementById("name").value =result[0]?result[0]:'';
		document.getElementById("mobile").value =result[1]?result[1]:'';
		document.getElementById("email").value =result[2]?result[2]:'';
		document.getElementById("address").value =result[3]?result[3]:'';
		
        //alert(result[0]);
		//alert(result[1]);
		//document.getElementById("crnbox").innerHTML = xmlhttp.responseText; 
	    //alert(xmlhttp.responseText);
		}
		}
        xmlhttp.open("GET", "update_contact_crnAjax.php?crn_no="+str, true);
		//xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
		xmlhttp.send();
}

</script>


<?php get_footer();  ?>
