<?php 
   session_start();
   $yr = date('y');
   
   define('WP_USE_THEMES', true);
   include('../configuration.php');
   require('../wp-config.php');
   date_default_timezone_set("Asia/Calcutta");
   get_header();
   
   ?>
<style>
   h2.panel-heading{
   font-weight:bold;  
   }
</style>
<div id="primary" class="content-area">
   <?php //if($smof_data['show_page_title_blog'] || $smof_data['show_page_breadcrumb_blog']) { ?>
   <div class="header-site-wrap">
      <div class="container container-md-height">
         <div class="row row-md-height cs-titile-bar">
            <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
               <h2 class="page-title"> Domestic PNG Registration
                  <?php //if($smof_data['show_page_title_blog']) echo cshero_page_title(); ?>
               </h2>
            </div>
            <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
               <div id="breadcrumb" class="cs-breadcrumb">
                  <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
                  Domestic PNG Registration 
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php //} ?>
   <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
      <div class="row">
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
            <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
         </div>
         <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
            <div class="big_title">
               <h3>Domestic PNG Registration  </h3>
               <p style="color:#F00; text-align:center;font-size:24px"><b>Domestic PNG registration opens in Areas..</b></p>
            </div>
            <!-- <div style="text-align:right">
               <h4> <span style="color: red">Existing User Click Here</span></h4>
               </div>-->
            <main id="main" class="site-main" role="main">
               <div class="col-md-12" style="margin-top:7%;">
                  <div class="panel panel-default">
                     <div class="panel-body tabs">
                        
                      
                         <a  target="_blank" style="cursor:pointer; font-size:20px; margin-left:250px"  title="Lucknow Area with Society" href="lucknow_png_registration.php"><b style="color: #000">Lucknow</b></a>
                         
                         <a  target="_blank" style="cursor:pointer; float:right; font-size:20px;margin-right:350px" title="Agra Area with Society" href="agra_png_registration.php"><b style="color: #000">Agra</a>                
                           
                          
                      
                     </div>
                  </div>
                  <!--/.panel-->
               </div>
            </main>
            <!-- #main --> 
         </div>
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
            <?php dynamic_sidebar( 'cshero-widget-right' ); ?>
         </div>
      </div>
   </div>
</div>
<!-- #primary -->
<script>
   function crnAjax(str) {
      
      var xmlhttp; 
      if (window.XMLHttpRequest)
      { // code for IE7+, Firefox, Chrome, Opera, Safari 
      xmlhttp=new XMLHttpRequest();
      }
      else
      { // code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function()
      { 
      if (xmlhttp.readyState==4)
      {//alert(str1);
      document.getElementById("crnbox").innerHTML = xmlhttp.responseText; 
        //alert(xmlhttp.responseText);
      }
      }
           xmlhttp.open("GET", "crnAjax.php?crn_no="+str, true);
      //xmlhttp.open("GET","script/ajax.php?control=customer&task=customer_details&acc_id="+str,true);
      xmlhttp.send();
   }
   
</script>
<?php get_footer();  ?>

