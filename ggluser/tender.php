<?php 
session_start();
$yr = date('y');

define('WP_USE_THEMES', true);
include('../configuration.php');
require('../wp-config.php');
date_default_timezone_set("Asia/Calcutta");
get_header();

?>
<style>
.button, input[type="button"], input[type="submit"] {

  margin: 10px !important;
 
}

</style>

<div id="primary" class="content-area">
  <?php //if($smof_data['show_page_title_blog'] || $smof_data['show_page_breadcrumb_blog']) { ?>
  <div class="header-site-wrap">
    <div class="container container-md-height">
      <div class="row row-md-height cs-titile-bar">
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
          <h2 class="page-title"> Tender
            <?php //if($smof_data['show_page_title_blog']) echo cshero_page_title(); ?>
          </h2>
        </div>
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
          <div id="breadcrumb" class="cs-breadcrumb">
            <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
            Tender </div>
        </div>
      </div>
    </div>
  </div>
  <?php //} ?>
  <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
        <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
      </div>
      <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
        <div class="big_title">
          <h3>TENDER LIST</h3>
          <p></p>
        </div>
       <!-- <div style="text-align:right">
          <h4> <span style="color: red">Existing User Click Here</span></h4>
        </div>-->
        <main id="main" class="site-main" role="main">
        
        <table align="center">
        <tr>
            <th width="8%"><div align="center">Sr No.</div></th>
            <th width="12%"><div align="center">Tender Date</div></th>
            <th width="15%"><div align="center">Project / Work Location</div></th>
            <th width="30%"><div align="center">Item</div></th>
            <th width="20%"><div align="center">Tender Close Date</div></th>
            <th width="15%"><div align="center">Action</div></th>
        
        </tr>
        <?php $sel_tender = mysql_query("select * from tender where status = '1' order by tender_date DESC");
				$i= 1;
				while($fetch_tender = mysql_fetch_array($sel_tender))
				{
					$tndrclosedate = explode(' ',$fetch_tender['tender_closing_date']);
			        $tdr_cls = date('d-m-Y', strtotime($tndrclosedate[0]));
					 $static_date = time()-86400;//date('d-m-Y');
					
			
			if(strtotime($tdr_cls) >= $static_date){
		 ?>
        <tr> 
            <td><div align="center"><?php echo $i; ?></div></td>
            <td><div align="center"><?php echo $tdate = date('d-m-Y',strtotime($fetch_tender['tender_date'])); ?></div></td>
            <td><div align="center"><?php echo $fetch_tender['project']; ?></div></td> 
            <td><div align="center"><?php echo $fetch_tender['item']; ?></div></td> 
            <td><div align="center"><?php echo $tdr_cls ." ". $tndrclosedate[1]; ?></div></td>
            <td><div align="center"><a href="tender_detail.php?id=<?php echo $fetch_tender['id']; ?>" id="btn-chat"><input type="button" class="btn btn-primary" value="View Tender" /></a></div></td>
        </tr>
        <?php $i++;} 
		
				}
		?>
        
        </table>
        
        
        
        </main>
        <!-- #main --> 
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
        <?php dynamic_sidebar( 'cshero-widget-right' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- #primary -->

<?php get_footer();  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
	
	$('input:radio[name="crn_avail"]').change(function() {
  if ($(this).val() == 'Yes') {
	 $("#crnbox").css('display','block');
   
  } else {
	  $("#crnbox").css('display','none');
  
  }
});
});
</script>