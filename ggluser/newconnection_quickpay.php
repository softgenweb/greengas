<?php 
   session_start();
   $yr = date('y');
   
   define('WP_USE_THEMES', true);
   include('../configuration.php');
   require('../wp-config.php');
   date_default_timezone_set("Asia/Calcutta");
   get_header();
   
   ?>
<style>
   h2.panel-heading{
   font-weight:bold;  
   }
</style>
<div id="primary" class="content-area">
   <div class="header-site-wrap">
      <div class="container container-md-height">
         <div class="row row-md-height cs-titile-bar">
            <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
               <h2 class="page-title"> New Connection Pay
               </h2>
            </div>
            <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
               <div id="breadcrumb" class="cs-breadcrumb">
                  <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
                 New Connection Pay 
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php //} ?>
   <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
      <div class="row">
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
            <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
         </div>
         <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
            <div class="big_title">
               <h3>Pay Online </h3>
               <p></p>
            </div>
            <!-- <div style="text-align:right">
               <h4> <span style="color: red">Existing User Click Here</span></h4>
               </div>-->
            <main id="main" class="site-main" role="main">
               <div class="col-md-12" style="margin-top:7%;">
                  <div class="panel panel-default">
                     <div class="panel-body tabs">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#tab1" data-toggle="tab"><b style="color:#000">New Connection Online Payment</b></a></li>
                        </ul>
                        <div class="tab-content">
                           <!------------------------------Start Tab-1------------------------>    
                           <?php 
						   $crn = $_GET['crn'];
						   $row = mysql_fetch_array(mysql_query("SELECT * FROM `admin_new_connection` WHERE crn='".$crn."' and verify_status='1' and payment_status!=1")); 
						   if($row){
						   ?>
                           
                                        
                           <div class="tab-pane fade in active" id="tab1">
                                 <div class="col-md-8 col-md-offset-2">
                                    <div class="panel-heading" style="color:#f00;">
                                       <p class="panel-heading" style="color:#000;"> Identification Number : (Please Enter Valid 12 Digit No.)</p>
                                    </div>
                                    <div class="form-group form-group-sm">
                                       <label class="col-sm-4 control-label" for="connectiontype">Identification No.</label>
                                       <div class="col-sm-8">
                                          <div class="form-group">
                                             <input class="form-control" type="text" id="crn_no" name="crn_no"  value="<?php echo $crn;?>"  readonly="readonly">
                                          
                                          </div>
                                       </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="crnbox"></div>
                                     <div class="form-group form-group-sm">
                                       <label class="col-sm-4 control-label"  for="connectiontype">Name  </label>
                                       <div class="col-sm-8"><div class="form-group">
                                          <input class="form-control" type="text" id="name" name="name"  value="<?php echo $row['cust_name'];?>"  readonly="readonly">
                                       </div></div>
                                       </div>
                                       <div class="clearfix"></div>
                                       
                                       <div class="form-group form-group-sm">
                                       <label class="col-sm-4 control-label" for="connectiontype">Email  </label>
                                       <div class="col-sm-8"><div class="form-group">
                                          <input class="form-control" type="email" id="email" name="email" value="<?php echo $row['email_id'];?>"  readonly="readonly">
                                       </div></div>
                                       </div>
                                       <div class="clearfix"></div>
                                       
                                       <div class="form-group form-group-sm">
                                       <label class="col-sm-4 control-label"  for="connectiontype">Mobile No.  </label>
                                       <div class="col-sm-8"><div class="form-group">
                                          <input class="form-control" type="text" id="mobile" name="mobile" maxlength="10" value="<?php echo $row['mobile_no'];?>"  readonly="readonly">
                                       </div>  </div>
                                       </div>
                                       <div class="clearfix"></div>
                                       
                                       <div class="form-group form-group-sm">
                                       <label class="col-sm-4 control-label"  for="connectiontype">Amount  </label>
                                       <div class="col-sm-8"><div class="form-group">
                                          <input class="form-control" type="text" id="amount" name="amount"  value="<?php echo $row['amount'];?>" readonly="readonly">
                                       </div></div>
                                       </div>
                                       <div class="clearfix"></div>
                                 </div>
                                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-3">                                 <div class="form-group" align="center">
                                    <!--<input type="submit" name="submit" class="btn btn-primary" value="Submit" />-->       
                                      <form action="newconnection_hdfc_payment.php" method="post" style="display:block;"> 
                <?php $rand =  $row['crn'].'_'.rand('1111','9999');
				
				$_SESSION['crn_no'] = $row['crn'];
                $_SESSION['mobile'] = $row['mobile_no'];
                $_SESSION['amount'] = $row['amount'];
                $_SESSION['email']  = $row['email_id'];
                $_SESSION['rands']  = $rand;
                ?>                                      
              <input type="hidden" name="crn" value="<?php echo $row['crn'];?>" />
              <input type="hidden" name="amount" value="<?php echo $row['amount']; ?>" />
              <input type="hidden" name="mobile_no" value="<?php echo $row['mobile_no']; ?>" />
              <input type="hidden" name="email_id" value="<?php echo $row['email_id']; ?>" />
              <input type="hidden" name="rands" value="<?php echo $rand; ?>" />
              <input type="submit" name="submit" class="btn btn-primary" value="Payment"  />
              
           
              
                <!--<a href="hdfc_payment.php" /><input type="button" class="btn btn-primary" value="HDFC Payment" /> </a> -->
              </form> 
                                    </div>
                                    </div>    
                            
                              
                         
                              
                           </div>
                           
                           <?php }else{ 
						 
						    $row = mysql_fetch_array(mysql_query("SELECT * FROM `admin_new_connection` WHERE crn='".$crn."' and payment_status=1")); 
						   if($row['payment_status']==1){
							   	echo '<script>window.location.href="newconnection_payment_confirm.php?crn='.$crn.'"</script>';
						   }
						   else{  echo "Data not found"; }
						   
						   } ?>
                           
                           <!------------------------------End Tab-1------------------------>         
                          
                        </div>
                     </div>
                  </div>
                  <!--/.panel-->
               </div>
            </main>
            <!-- #main --> 
         </div>
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
            <?php dynamic_sidebar('cshero-widget-right'); ?>
         </div>
      </div>
   </div>
</div>
<!-- #primary -->

<?php get_footer();  ?>

