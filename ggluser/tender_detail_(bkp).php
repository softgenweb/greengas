<?php 
session_start();
$yr = date('y');

define('WP_USE_THEMES', true);
include('../configuration.php');
require('../wp-config.php');
date_default_timezone_set("Asia/Calcutta");
get_header();

?>
<style>
.button, input[type="button"], input[type="submit"] {
  margin: 10px !important;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}
.table {
  margin-bottom: 20px;
  max-width: 100%;
  width: 100%;
}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
  border-top: 1px solid #ddd;
  line-height: 1.42857;
  padding: 8px;
  vertical-align: top;
  border-left:0px !important;
}
.table td{
 border-left:0px !important;	
}
</style>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div id="primary" class="content-area">
  <?php //if($smof_data['show_page_title_blog'] || $smof_data['show_page_breadcrumb_blog']) { ?>
  <div class="header-site-wrap">
    <div class="container container-md-height">
      <div class="row row-md-height cs-titile-bar">
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-page-title">
          <h2 class="page-title"> Tender
            <?php //if($smof_data['show_page_title_blog']) echo cshero_page_title(); ?>
          </h2>
        </div>
        <div class="col-sx-12 col-sm-12 col-md-6 col-lg-6 col-md-height col-middle cs-breadcrumb">
          <div id="breadcrumb" class="cs-breadcrumb">
            <?php if($smof_data['show_page_breadcrumb_blog']) echo cshero_page_breadcrumb($smof_data['delimiter_page_breadcrumb_blog']); ?>
            Tender </div>
        </div>
      </div>
    </div>
  </div>
  <?php //} ?>
  <div class="<?php if(get_post_meta($post->ID, 'cs_blog_layout', true) === "full"){ echo "no-container";} else { echo "container-fluid"; } ?>">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 left_sidebar" style="border:1px solid #f0f0f0; height:100%; margin-bottom:30px !important;">
        <?php dynamic_sidebar( 'cshero-widget-left' ); ?>
      </div>
      <div class="col-sx-12 col-sm-12 col-md-8 col-lg-8">
        <div class="big_title">
          <h3>VIEW TENDER DETAILS</h3>
          <p></p>
        </div>
      <?php 
	  $sel_tender =  mysql_fetch_array(mysql_query("select * from tender where id = '".$_REQUEST['id']."'"));	
		 ?>
         <main id="main" class="site-main" role="main">
        <form class="form-horizontal" action="" method="post" role="form">
        <table class="table table-responsive table-hover table-bordered tender_detail">
        
        <tbody>
        <tr>
        <td style="width:30%;">TENDER NO  </td>
        <td> <?php echo $sel_tender['tender_no']; ?></td>
        </tr>
        <tr>
        <td>TENDER DATE  </td>
        <td> <?php echo $tdate = date('d-m-Y',strtotime($sel_tender['tender_date'])); ?></td>
        </tr>
        <tr>
        <td>TENDER CLOSING DATE  </td>
        <td> <?php echo $tdate = date('d-m-Y H:i:s',strtotime($sel_tender['tender_closing_date'])); ?></td>
        </tr>
        <!--<tr>
        <td>CLIENT  </td>
        <td> <?php echo $sel_tender['client']; ?></td>
        </tr>-->
        <tr>
        <td>PROJECT/WORK LOCATION </td>
        <td> <?php echo $sel_tender['project']; ?></td>
        </tr>
        <tr>
        <td>ITEM  </td>
        <td> <?php echo $sel_tender['item']; ?></td>
        </tr>
        <tr>
        <td>COORDINATING OFFICE  </td>
        <td> <?php echo $sel_tender['coordinating_office']; ?></td>
        </tr>
        <tr>
        <td>ELIGIBILITY CRITERIA   </td>
        <td> <?php echo $sel_tender['eligibility_criteria']; ?></td>
        </tr>
       <!-- <tr>
        <td>LAST SUBMISSION DATE  </td>
        <td> <?php echo $sel_tender['last_date']; ?></td>
        </tr>
        <tr>
        <td>SALE START DATE </td>
        <td> <?php echo $sel_tender['sale_start_date']; ?></td>
        </tr>-->
        <tr>
        <td>PREBID DATE & TIME  </td>
        <td> <?php echo $sel_tender['prebid_summary']; ?></td>
        </tr>
        <!--<tr>
        <td>SALE END DATE   </td>
        <td> <?php echo $sel_tender['sale_end_date']; ?></td>
        </tr>-->
        <tr>
        <td>DOCUMENT COST  </td>
        <td> <?php echo $sel_tender['document_cost']; ?></td>
        </tr>
        <tr>
        <td>PLACE OF SUBMISSION   </td>
        <td> <?php echo $sel_tender['submission_place']; ?></td>
        </tr>
       <!-- <tr>
        <td>EXECUTION LOCATION  </td>
        <td> <?php echo $sel_tender['execution_location']; ?></td>
        </tr>-->
        <tr>
        <td>TENDER OPENING ON  </td>
        <td> <?php
		$tender_opening = explode(' ',$sel_tender['tender_opening']);
		 echo date('d-m-Y', strtotime($tender_opening[0])) ." ".$tender_opening[1]; ?></td>
        </tr>       
        </tbody>
        </table>
        <div class="col-md-12">
            
              
                <div class="big_title">
                <h3>Download Tender Files</h3>
                <p></p>
             
               <table class="table table-responsive table-hover table-bordered tender_detail">
               <tbody>
        <tr>
            <th>File</th>
            <th>Tender Document Type</th>
        </tr>
         <?php  $sel_tender1 =  mysql_query("select * from tender_attachment where tender_id = '".$_REQUEST['id']."'");
				
				while($fetch_tender1 = mysql_fetch_array($sel_tender1))
				{
			  ?> 
         <tr> 
            <td><div align="center"><a href="./../media/tender/<?php echo $fetch_tender1['tender_file'];?>" title="View" target="_blank"><input type="button" class="btn btn-primary" value="Download" /></a></div></td>
            
            <!--<a href="tender_detail.php?id=<?php echo $fetch_tender['id']; ?>" target="_blank" id="btn-chat"><input type="button" class="btn btn-primary" value="Download" /></a></div></td>-->
            <td><div align="center"> <?php
													 
														$fname = explode('/',$fetch_tender1['tender_file']);													 
														$prefix = $fetch['id'];
														$str = $fname[1];
														
														if (substr($str, 0, strlen($prefix)) == $prefix) {
															$str = substr($str, strlen($prefix));
														}
														 
														
														echo $str;
													  ?></div></td> 
        </tr>
        <?php } ?>
        </tbody>
        </table>
        </div>
              
          </div>
    
          <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-3">
           
              <div class="form-group" align="center">
                <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
           
            
              </div>
              
          </div>-->
           </form>
        </main>
        <!-- #main --> 
      </div>
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 right_sidebar" style="border:1px solid #f0f0f0; height:100%;">
        <?php dynamic_sidebar( 'cshero-widget-right' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- #primary -->

<?php get_footer();  ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
	
	$('input:radio[name="crn_avail"]').change(function() {
  if ($(this).val() == 'Yes') {
	 $("#crnbox").css('display','block');
   
  } else {
	  $("#crnbox").css('display','none');
  
  }
});
});
</script>