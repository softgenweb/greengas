<?php
/*
 * Template Name: Page No Title Bar
 * Description: A Page Template page no title bar.
 *
 * @package cshero
 */

get_header(); ?>
<?php global $post; ?>
	<div id="primary" class="content-area">
    <section class="slider">
    <div class="container-fluid">
            <div class="row" style="background:#fff !important;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <main id="main" class="site-main" role="main">
                     <?php putRevSlider( "slider" ) ?>                  
                    </main><!-- #main -->
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="greenslip">
    <section>
   <p style="text-align: center; line-height: 32px;"><span style="font-size: 32px;"><span style="font-family: arial black,avant garde;">Green Gas Ltd.</span> is looking for <span style="font-family: arial black,avant garde;">distributors</span>! Join our team! <a class="wpb_button wpb_btn-large" href="#">Become A Distributor</a></span></p>
    </section>
    </div>
    <div class="clearfix"></div>
    
    <div class="accordianwp">
    <section>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <h3 class="headline text-center underline  " style="color: #666666">Green Gas Limited (GGL)</h3>
        <div class="wpb_wrapper">
			<p>Pursuant to the directive of Hon'ble Supreme Court of India GAIL had undertaken a study to supply natural gas to the automobile, industrial, commercial and domestic Consumers in the cities of Agra and Lucknow to accomplish improvement of its ambient air quality.</p>

<p>Joint Venture agreement (JV) was signed between GAIL (India) Ltd. and Indian Oil Corporation Limited on May 11, 2005, which was followed by incorporation of Green Gas Ltd (GGL). The company was incorporated On October 07, 2005, with the ....</p>

<p><a href="#" class="readmore">Read More..</a></p>

		</div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <h3 class="headline text-center underline  " style="color: #666666">Why Choose Us</h3>
        <?php echo do_shortcode('[accordion category="18"]'); ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
        <p>&nbsp;</p>
        </div>
        </section>
    </div>
     <div class="clearfix"></div>
    <section class="back_img">
    <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <main id="main" class="site-main" role="main">
                     <div class="container">
                     <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <h3 class="headline text-center underline  " style="color: #666666">I am looking for</h3>
        <ul class="vendor">
            <li><i class="fa fa-user" aria-hidden="true"></i>New Connection</li>
            <li><i class="fa fa-edit"></i>Existing User</li>
            <li><i class="fa fa-user" aria-hidden="true"></i>Tender</li>
            <li><i class="fa fa-inr" aria-hidden="true"></i>Pay Bill</li>
            <li><i class="fa fa-edit"></i>Web mail</li>
            <li><i class="fa fa-user" aria-hidden="true"></i>Coverge</li>
            <li><i class="fa fa-inr" aria-hidden="true"></i>Feedback</li>
            <li><i class="fa fa-edit"></i>Emergency</li>
        </ul>
        </div>
                       <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <h3 class="headline text-center underline  " style="color: #666666">Customer Zone</h3>
        <ul class="vendor">
            <li><i class="fa fa-user" aria-hidden="true"></i> Sign In</li>
            <li><i class="fa fa-edit"></i>Apply New Connection</li>
            <li><i class="fa fa-user" aria-hidden="true"></i> PNG Converge</li>
            <li><i class="fa fa-inr" aria-hidden="true"></i> Quick Pay</li>
            <li><i class="fa fa-edit"></i>Feedback/Complain</li>
        </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <h3 class="headline text-center underline  " style="color: #666666">Vendor Zone</h3>
         <ul class="vendor">
            <li><i class="fa fa-user" aria-hidden="true"></i> Sign In</li>
            <li><i class="fa fa-edit"></i>Register</li>
            <li><i class="fa fa-user" aria-hidden="true"></i> Tender</li>
            <li><i class="fa fa-inr" aria-hidden="true"></i> Corrigendum</li>
            <li><i class="fa fa-edit"></i>Archieve</li>
            <li><i class="fa fa-edit"></i>Feedback</li>
        </ul>
        </div>
                     </div>                
                    </main><!-- #main -->
                </div>
            </div>
        </div>
    </section>
   
    <div class="clearfix"></div>
    <div class="unitslip_service">
    <section>
    <div class="container">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div class="wpb_wrapper">
			
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img src="http://softgentech.com/greengas/wp-content/uploads/2016/11/Free-shiping.png" class="vc_single_image-img attachment-full" alt="Free-shiping" width="359" height="47"></div>
		</div> 
	</div> 
		</div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div class="wpb_wrapper">
			
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img src="http://softgentech.com/greengas/wp-content/uploads/2016/11/customer.png" class="vc_single_image-img attachment-full" alt="customer" width="350" height="44"></div>
		</div> 
	</div> 
		</div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div class="wpb_wrapper">
			
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img src="http://softgentech.com/greengas/wp-content/uploads/2016/11/payment.png" class="vc_single_image-img attachment-full" alt="payment" width="370" height="47"></div>
		</div> 
	</div> 
		</div>
            </div>
        
        </div>
        </section>
    </div>
    <div class="clearfix"></div>
    <section class="logocase">
    <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <main id="main" class="site-main" role="main">
                    
                     <div class="container">
                     <br />
                     <h3 class="headline text-center underline  " style="color: #666666">Our Clients</h3>
                     <br />
                     <?php  echo do_shortcode('[wpaft_logo_slider]'); ?>
                     </div>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php get_template_part( 'framework/templates/blog/content', 'page' ); ?>

                            <?php
                                // If comments are open or we have at least one comment, load up the comment template
                                if (cshero_show_comments()) :
                                    comments_template();
                                endif;
                            ?>

                        <?php endwhile; // end of the loop. ?>
                       

                    </main><!-- #main -->
                </div>
            </div>
        </div>
    </section>
        
	</div><!-- #primary -->

<?php get_footer(); ?>
