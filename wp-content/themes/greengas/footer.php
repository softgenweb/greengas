<!--<?php global $smof_data; ?>
<?php if ($smof_data['footer_top_widgets']): ?>
    <footer id="footer-top">
        <div class="container">
            <div class="row">
                <div class="footer-top">
                    <?php cshero_sidebar_footer_top(); ?>
                </div>
            </div>
        </div>
    </footer>
<?php endif; ?>
<?php if ($smof_data['footer_bottom_widgets']): ?>
<footer id="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="footer-bottom">
                <?php cshero_sidebar_footer_bottom(); ?>
            </div>
        </div>
    </div>
</footer>
<?php endif; ?>
</div>
<?php echo $smof_data["space_body"]; ?>-->
<div class="container-fluid footer">
	<div class="col-md-3 pull-left">
    	<!--<div class="col-md-3 pull-left">-->
        	<span style="color:white"> Copyright © 2017 <a style="color: #f5f00a" href="#">Green Gas Limited </a></span>
        </div> 
        <div class="col-md-5 pull-left">
          
               <a style="color: #f5f00a" href="https://gglonline.net/terms-conditions/">Terms & Conditions </a>
           
           
                <a style="color: #f5f00a" href="https://gglonline.net/refund-policy/">Refund Policy</a>
        
          
               <a style="color: #f5f00a" href="https://gglonline.net/privacy-policy/">Privacy policy</a>
            
        </div>
        <div class="col-md-4 pull-right">
        	<span style="color:white"> 
Designed and Developed by:<a style="color: #f5f00a" href="http://softgentechnologies.com/" target="_blank"> Softgen Technologies (P) Ltd </a></span>
        </div>
   
</div>

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121808176-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());


  gtag('config', 'UA-121808176-1');

</script>


<?php wp_footer(); ?>

</body>
</html>