<?php
/*
 * Template Name: Page No Title Bar
 * Description: A Page Template page no title bar.
 *
 * @package cshero
 */

get_header(); ?>
<?php global $post; ?>

<div id="primary" class="content-area">
  <section class="slider">
    <div class="banner_fluid">
      <div class="sidebar_bg">
        <?php putRevSlider( "slider-2") ?>
      </div>
      <div class="slider_right">
        <ul class="nav navbar-nav">
          <li><a href="new-connection"><i class="fa fa-link" aria-hidden="true"></i> New Connection</a></li>
          <li><a href="customer-login"><i class="fa fa-user-o" aria-hidden="true"></i> Existing User </a></li>
          <li><a href="active-tenders"><i class="fa fa-id-card-o" aria-hidden="true"></i> Tender</a></li>
          <li><a href="pay-bill"><i class="fa fa-credit-card" aria-hidden="true"></i> Pay Bill</a></li>
          <li><a href="http://webmail.mailhostbox.com/appsuite/"><i class="fa fa-envelope-open-o" aria-hidden="true"></i> Web Mail</a></li>
          <li><a href="png-coverage"><i class="fa fa-bullhorn" aria-hidden="true"></i> Coverage</a></li>
          <li><a href="feedback"><i class="fa fa-comment" aria-hidden="true"></i> Feedback</a></li>
          <li><a href="emergency"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Emergency</a></li>
          <li><a href="contact-us"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us</a></li>
        </ul>
      </div>
    </div>
  </section>
  <div class="clearfix"></div>
  <div class="greenslip"> 
    <!--<section>
      <p style="text-align: center; line-height: 32px;"><span style="font-size: 32px;"><span style="font-family: arial black,avant garde;">Green Gas Ltd.</span> is looking for <span style="font-family: arial black,avant garde;">distributors</span>! Join our team! <a class="wpb_button wpb_btn-large" href="#">Become A Distributor</a></span></p>
    </section>--> 
  </div>
  <div class="greyslip">
    <section>
      <p class="welcome_slip">Welcome to Green Gas Ltd.</p>
    </section>
  </div>
  <div class="clearfix"></div>
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="col-md-7 left_grid">
        <div class="col-md-4">
          <div class="content_box">
            <div class="content_head">Green Gas Limited (GGL) </div>
            <div class="content_text">
              <p>Green Gas Limited (GGL) is a Joint Venture of GAIL (India) Limited [GAIL] and Indian Oil Corporation Limited [IOCL].</p>
              <p>It has been incorporated for the implementation of City Gas Projects for supply of Piped Natural Gas (PNG) to domestic, commercial and industrial</p>
              <div class="read_btn"><a href="User_Page/About_Us.aspx">Read More..</a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="content_box">
            <div class="content_head">News Update </div>
            <div class="content_text">
              <marquee behavior="scroll" direction="up" scrollamount="3" height="230px">
              <p><a href="SubLink/News.aspx" style="text-decoration:none; color:black; text-align:justify;"> <span id="repeaternews_lblnotification_0" style="padding-top:10px">Now PNG consumers can register to GGL website to view &amp; pay gas bills and update contact details.</span><br>
                </a></p>
              <p><a href="SubLink/News.aspx" style="text-decoration:none; color:black"> <span id="repeaternews_lblnotification_3" style="padding-top:10px">Upcoming CNG Stations:
                Lucknow: M/s Anurag FS- Raibarelly Road, Mother Station- Vrindavan Yojna -6,  Mother Station- Vrindavan Yojna-18, M/s SS Filling Stn- Kanpur Road, M/s Buddeshwar FS- Mohan Road, M/s Kalawati FS-Sultanpur Road</span><br>
                </a></p>
              <p><a href="SubLink/News.aspx" style="text-decoration:none; color:black"> <span id="repeaternews_lblnotification_4" style="padding-top:10px">Upcoming CNG Stations:Lucknow:Mother Station- Vrindavan Yojna -6,  Mother Station- Vrindavan Yojna-18, M/s SS Filling Stn- Kanpur Road, M/s Buddeshwar FS- Mohan Road, M/s Kalawati FS-Sultanpur Road</span><br>
                </a></p>
              </marquee>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="content_box">
            <div class="content_head">Customer Zone </div>
            <div class="content_text">
              <div class="btn btn-info cust_zone"> <a href="customer-login"><i class="fa fa-user"></i> Sign In</a> </div>
              <div class="btn btn-info cust_zone"> <a href="ggluser/user_applynewconnection.php"><i class="fa fa-edit"></i> Apply New Connection</a> </div>
              <div class="btn btn-info cust_zone"> <a href="png-coverage"><i class="fa fa-user"></i> PNG Coverage</a> </div>
              <div class="btn btn-info cust_zone"> <a href="ggluser/quickpay.php"><i class="fa fa-rupee"></i> Quick Pay</a> </div>
              <div class="btn btn-info cust_zone"> <a href="ggluser/user_feedback.php"><i class="fa fa-edit"></i> Feedback/Complain</a> </div>
              <div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-5 right_grid">
        <div class="col-md-6">
          <div class="content_box">
            <div class="content_head">Vendor Zone </div>
            <div class="content_text">
              <div class="btn btn-info cust_zone"> <a href="vendor-login"><i class="fa fa-user"></i> Sign In</a> </div>
              <div class="btn btn-info cust_zone"> <a href="vendor-registration"><i class="fa fa-edit"></i> Register</a> </div>
              <div class="btn btn-info cust_zone"> <a href="active-tenders"><i class="fa fa-folder-o"></i>Tender</a> </div>
              <div class="btn btn-info cust_zone"> <a href="corrigendum"><i class="fa fa-folder-o"></i> Corrigendum</a> </div>
              <div class="btn btn-info cust_zone"> <a href="archive"><i class="fa fa-folder-o"></i> Archieve</a> </div>
              <div class="btn btn-info cust_zone"> <a href="feedback"><i class="fa fa-edit"></i> Feedback </a> </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="content_box">
            <div class="content_head">Attention </div>
            <a href="http://gglonline.net/SubLink/Attention1.aspx" style="text-decoration:none; text-align:center">
            <h4>Attention All CNG Car Owners</h4>
            </a>
            <div style="width: 100%;"> <img src="http://gglonline.net/IMG/red-banner-new.png" alt="red_img"/> <img src="http://gglonline.net/IMG/hgc.png" alt="red_img"/> </div>
            <div style="width:100%; text-align:center;">
              <h5 style="font-size:14px;">Name and Address of PESO Authorised Hydro Testing Center</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="container-fluid">
    <div class="col-md-12 info_box">
      <div class="bottom_border"> CIN : U23201UP2005PLC030834  PHONE : 0522-4088530, 4088526 FAX : 0522-4088529 www.gglonline.net. </div>
    </div>
  </div>
  
  <!--<div class="container info_box">
  	<div class="footer_info"> CIN : U23201UP2005PLC030834  PHONE : 0522-4088530, 4088526 FAX : 0522-4088529 www.gglonline.net.</div>
    </div> --> 
</div>
<div class="container-fluid ggl_map_wrapper">
  <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 ggl_map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56952.73417708458!2d80.9533630436726!3d26.85439235296032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399be2b8c2614365%3A0x55cca8504c90f9e9!2sGreen+Gas+Limited!5e0!3m2!1sen!2sin!4v1497677621516" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 safety_tips">
  <?php echo do_shortcode('[supsystic-slider id=1 position="center"]');  ?>
  
  </div>
</div>

<!-- #primary --> 
<?php echo do_shortcode('[sg_popup id=1]');  ?>
<?php get_footer(); ?>
