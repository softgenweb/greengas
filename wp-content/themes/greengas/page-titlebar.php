<?php /*
 * Template Name: Page No Title Bar
 * Description: A Page Template page no title bar.
 *
 * @package cshero */
get_header(); ?>
<?php global $post; ?>
<div id="primary" class="content-area">
  <section class="slider">
    <div class="banner_fluid">
      <div class="sidebar_bg">
<?php putRevSlider( "slider-2") ?>
      </div>
      <div class="slider_right">
<ul class="nav navbar-nav">
  <li><a href="ggluser/user_applynewconnection.php"><i class="fa fa-link" aria-hidden="true"></i> New Connection</a></li>
  <!--<li><a href="customer-login"><i class="fa fa-user-o" aria-hidden="true"></i> Existing User </a></li>-->
  <li><a href="ggluser/update_contact_details.php"><i class="fa fa-user-o" aria-hidden="true"></i> Update Contact Details </a></li>
  <li><a href="https://gglonline.net/wp-content/uploads/2017/10/Whistle-blower-policy_Green-Gas-Limited.pdf" target="_blank"><i class="fa fa-id-card-o" aria-hidden="true"></i> Whistle Blower Policy</a></li>
  <li><a href="ggluser/quickpay.php"><i class="fa fa-credit-card" aria-hidden="true"></i> Pay Bill</a></li>
  <li><a href="https://gglonline.net/gallery"><i class="fa fa-camera-retro" aria-hidden="true"></i> Gallery</a></li>
  <li><a href="png-coverage"><i class="fa fa-bullhorn" aria-hidden="true"></i> Coverage</a></li>
  <li><a href="ggluser/user_feedback.php"><i class="fa fa-comment" aria-hidden="true"></i> Feedback/ Complain</a></li>
  <li><a href="emergency"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Emergency</a></li>
  <li><a href="contact-us"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us</a></li>

        
</ul>



      </div>



    </div>



  </section>



  <div class="clearfix"></div>



  <div class="greenslip"> 



    



    <!--<section>







      <p style="text-align: center; line-height: 32px;"><span style="font-size: 32px;"><span style="font-family: arial black,avant garde;">Green Gas Ltd.</span> is looking for <span style="font-family: arial black,avant garde;">distributors</span>! Join our team! <a class="wpb_button wpb_btn-large" href="#">Become A Distributor</a></span></p>







    </section>--> 



    



  </div>



  <div class="greyslip">



    <section>



      <p class="welcome_slip">Welcome to Green Gas Ltd.</p>



    </section>



  </div>



  <div class="clearfix"></div>



  <div class="container-fluid">



    <div class="col-md-12">



      <div class="col-md-9 left_grid">
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
  <div class="content_box">
    <div class="content_head">Green Gas Limited (GGL) </div>
    <div class="content_text">
      <p>Green Gas Limited (GGL) is a Joint Venture of GAIL (India) Limited [GAIL] and Indian Oil Corporation Limited [IOCL].</p>
      <p>It has been incorporated for the implementation of City Gas Projects for supply of Piped Natural Gas (PNG) to domestic, commercial and industrial</p>
      <div class="read_btn"><a href="User_Page/About_Us.aspx">Read More..</a></div>
    </div>
  </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
  <div class="content_box">
    <div class="content_head">News Update </div>
    <div class="content_text">
      <marquee behavior="scroll" direction="up" scrollamount="3" height="230px">
      <p><span id="repeaternews_lblnotification_0" style="padding-top:10px">1. Now You Can Register Online For Domestic PNG Connection.</span><br>
      </p>
      <p> <span id="repeaternews_lblnotification_3" style="padding-top:10px">2. Commercial CNG Sale has been commenced at Prakash Auto Faizabad Road.<br> 

              •	M/s Buddeshwar FS- Mohan Road, Lucknow  (OLS)<br> 

              •	M/s SS Filling Stn- Kanpur Road, Lucknow  (DBS)<br> 

              •	M/s Kalawati FS-Sultanpur Road, Lucnow  (DBS)<br>

              •	Prakash Auto Faizabad Road<br>

</span>
      </p>

              

              <p> <span id="repeaternews_lblnotification_3" style="padding-top:10px">3. Upcoming CNG Stations:<br>

                <b>Lucknow:</b>

                •	Hamara Pump, Mohanlal Ganj, (DBS)

                •	Dubagga CNG Station , UPSRTC (Mother Station)

                

                <b>Agra:</b>

                •	Brij Auto NH-2 Sikandra, 

                •	Kant Auto Tundla, 

                •	Sudhir Filling Station-Runukta, 

                •	Jhalak Kishori Shamshabad Road, 

                •	Kalindi Vihar,
      </span><br>
      </p>
     <!-- <p> <span id="repeaternews_lblnotification_4" style="padding-top:10px">4. Upcoming CNG Stations:<br>



<b>Lucknow:</b> M/s SS Filling Stn- Kanpur Road, M/s Buddeshwar FS- Mohan Road, M/s Kalawati FS-Sultanpur Road,<br>



<b>Agra:</b>  Brij Auto NH-2 Sikandra, Kant Auto Tundla, Sudhir Filling Station-Runukta, Jhalak Kishori Shamshabad Road, Kalindi Vihar,



</span><br>
      </p>-->
      </marquee>
    </div>
  </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
  <div class="content_box">
    <div class="content_head">Customer Zone </div>
    <div class="col-md-6 col-sm-6" style="padding:0px;">
      <div class="content_text"> 
        
        <!--<div class="btn btn-info cust_zone"> <a href="customer-login"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/pencil.png" alt="A/c"/></i> <br />Sign In</a> </div>--> 
        
        <!--<div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>-->
        
        <div class="btn btn-info cust_zone"><a href="ggluser/quickpay.php"> <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/wallet.png" alt="A/c"/><br />
          Quick Pay</a></div>
        
        <!-- <div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>-->
        
        <div class="btn btn-info cust_zone"><a href="ggluser/user_feedback.php"> <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/chat-1.png" alt="A/c"/> <br />
          Feedback/ Complain </a></div>
        
        <!--<div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>--> 
        
      </div>
    </div>
    <div class="col-md-6 col-sm-6"  style="padding:0px;">
      <div class="content_text"> 
        
        <!--<div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>-->
        
        <div class="btn btn-info cust_zone"> <a href="ggluser/user_applynewconnection.php"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/user-1.png" alt="A/c"/> <br />
          Apply New Connection </a></div>
        
        <!--<div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>-->
     <!--   
        <div class="btn btn-info cust_zone"> <a href="nearest-cng-station"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/map-marker.png" alt="A/c"/> <br />
          Locate CNG Outlet</a> </div>-->

                  

                     
        <div class="btn btn-info cust_zone"> <a href="ggluser/user_meter_reding.php"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/map-marker.png" alt="A/c"/> <br />
          Meter Reading</a> </div>
        
        <!--  <div class="btn btn-info cust_zone"> <a href="#"><i class="fa"></i> </a> </div>--> 
        
        <!--<div class="btn btn-info cust_zone"> <a href="archive"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/06/question-mark-1.png" alt="A/c"/> <br />Faqs</a> </div>--> 
        
        <!-- <div class="btn btn-info cust_zone"> <a href="feedback"><i class="fa fa-edit"></i> Feedback </a> </div>--> 
        
      </div>
    </div>
  </div>
</div>

<!-- <div class="col-md-3">




  <div class="content_box">




    




    




  </div>




</div>--> 




      </div>



      <div class="col-md-3 right_grid">
<div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">
  <div class="content_box">
    <div class="content_head">Attention </div>
    <a href="#" style="text-decoration:none; text-align:center">


    <h4>Attention All CNG Car Owners</h4>
    </a>
    <div style="width: 100%; text-align:center;"> <img src="http://www.gglonline.net/wp-content/uploads/2017/07/ribin.png" alt="red_img"/> </div>
    <div style="width: 100%; text-align:center;"> <img src="http://www.gglonline.net/wp-content/uploads/2017/07/car.png" alt="red_img"/> </div>
    <div style="width:100%; text-align:center;">
      <h5 style="font-size:14px;">Name and Address of PESO Authorised Hydro Testing Center</h5>

                <p style="font-size:16px;"><a href="nearest-cng-station"><i class="fa fa-map-marker" aria-hidden="true"></i> Locate CNG Outlet</a></p>
    </div>
  </div>
</div>



      </div>



    </div>



  </div>



  <div class="clearfix"></div>



  <div class="container-fluid">



    <div class="col-md-12 info_box">



      <div class="bottom_border"> CIN : U23201UP2005PLC030834  PHONE : 0522-4088530, 4088526 FAX : 0522-4088529 www.gglonline.net. </div>



    </div>



  </div>



  



  <!--<div class="container info_box">







  	<div class="footer_info"> CIN : U23201UP2005PLC030834  PHONE : 0522-4088530, 4088526 FAX : 0522-4088529 www.gglonline.net.</div>







    </div> --> 



  



</div>



<div class="container-fluid ggl_map_wrapper">



  <div class="col-xl-8 col-lg-7 col-md-7 col-sm-6 col-xs-12 ggl_map">



    <iframe class="map_mark" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56952.73417708458!2d80.9533630436726!3d26.85439235296032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399be2b8c2614365%3A0x55cca8504c90f9e9!2sGreen+Gas+Limited!5e0!3m2!1sen!2sin!4v1497677621516" width="100%"  frameborder="0" style="border:0" allowfullscreen></iframe>



  </div>



  <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6 col-xs-12 safety_tips"> 



<?php echo do_shortcode('[supsystic-slider id=1 position="center"]');  ?> 



</div>



</div>







<!-- #primary -->



<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content" style="height:600px;">

      <!--<div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Welcome to GGL</h4>

      </div>-->

      <div class="modal-body" style="overflow-x:auto; height:550px;">

        <img src="https://gglonline.net/wp-content/uploads/2017/11/paper-ad.png" alt=""/>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary btn-green" data-dismiss="modal" >Click to Enter the Website</button>

      </div>

    </div>



  </div>

</div>





<?php /*?><?php echo do_shortcode('[sg_popup id=1]');  ?><?php */?>



<?php echo do_shortcode('[sg_popup id=1]');  ?>

<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>

<script>

 $(window).load(function(){        

   $('#myModal').modal('show');

    }); 

</script>

<?php get_footer(); ?>



