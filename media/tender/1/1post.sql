-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2017 at 08:37 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_lender`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
`id` int(100) NOT NULL,
  `department_id` varchar(255) NOT NULL,
  `post_name` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `area_level` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `datetime` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `department_id`, `post_name`, `area`, `area_level`, `level`, `status`, `datetime`) VALUES
(1, '1', 'CEO', 'Country', '1', '1', '1', '2016-08-11 15:49:10'),
(2, '1', 'Vice President ', 'Country', '1', '1', '1', '2016-08-11 15:49:30'),
(3, '1', 'NSM', 'Country', '1', '1', '1', '2016-08-11 15:52:45'),
(4, '1', 'HR/Admin', 'State', '2', '1', '1', '2016-08-11 15:53:28'),
(5, '2', 'Operations', 'State', '2', '2', '1', '2016-08-11 15:53:58'),
(6, '3', 'Regional Sales Manager', 'Country', '1', '3', '1', '2016-11-18 17:10:58'),
(7, '3', 'Area Sales Manager', 'State', '2', '4', '1', '2016-11-19 13:01:53'),
(8, '3', 'Retail Personnel (Retail)', 'City', '3', '5', '1', '2017-01-27 17:21:35'),
(9, '4', 'Team Lead (Outbound)', 'State', '1', '4', '1', '2017-01-27 18:02:46'),
(10, '4', 'Outbound Caller', 'City', '3', '5', '1', '2017-02-28 17:33:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
