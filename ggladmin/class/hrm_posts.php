<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class hrm_postClass extends DbAccess {
		public $view='';
		public $name='hrm_post';

		
		/***************************************************** POST START **********************************************************/
		
		function show(){	
		$uquery ="select * from post where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
	

		
		
		
		function status(){
		$query="update post set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		
			
			$_SESSION['error'] = STATUS;	
            $_SESSION['errorclass'] = ERRORCLASS;
		//$this->show();	
		header("location:index.php?control=hrm_post&task=show");
		}
		
		function save(){
			$post_name=$_POST['post_name'];
			$area=$_POST['area'];
			$area_level=$_REQUEST['area_level'];
			$level=$_REQUEST['level'];
			$department_id=$_REQUEST['department_id'];
			if(!$_REQUEST['id']){
		
		  $query="insert into post (department_id,post_name,area,area_level,level,status,datetime) value('".$department_id."','".$post_name."','".$area."','".$area_level."','".$level."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
		$this->Execute();
			
			$_SESSION['error'] = ADDNEWRECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;
		header("location:index.php?control=hrm_post");
		}
		else
		{
			$update="update post set department_id='".$department_id."', post_name='".$post_name."',area='".$area."',area_level='".$area_level."',level='".$level."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
			$this->Execute();
				
			$_SESSION['error'] = UPDATERECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;
			header("location:index.php?control=hrm_post");
		}
		
		}
		
		function delete(){
		
		$query="DELETE FROM post WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';	
			
			$_SESSION['error'] = DELETE;	
            $_SESSION['errorclass'] = ERRORCLASS;
		//$this->show();
		header("location:index.php?control=hrm_post&task=show");
		
		}
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  post WHERE id=".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		/***************************************************** POST END **********************************************************/

	}
