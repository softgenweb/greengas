<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class contractor_masterClass extends DbAccess {
		public $view='';
		public $name='contractor_master';

		
		function show(){	
		
		$uquery ="select * from contractor_master where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query ="select * from contractor_master where 1 LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  contractor_master WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		
		
		function save(){
			
			
			$contact_person_name=$_POST['contact_person_name'];     $company_name=$_POST['company_name'];
			$address=$_POST['address'];								$mobile=$_POST['mobile'];
			$email_id=$_POST['email_id'];							$type=$_POST['type'];
			$city_id=$_POST['city_id'];
		
			
			if(!$_REQUEST['id']){
		
		 $new_contractor="INSERT INTO `contractor_master`(`contact_person_name`, `company_name`, `address`, `mobile`, `email_id`,`type`,`city_id`, `created_by`, `created_date_time`) VALUES ('".$contact_person_name."','".$company_name."','".$address."','".$mobile."','".$email_id."','".$type."','".$city_id."','".$_SESSION['adminid']."','".date('Y-m-d h:i:s')."')";
			$exe_new_contractor = mysql_query($new_contractor);
			$id = mysql_insert_id();
			if($this->Execute()) {	
			$_SESSION['error'] = ADDNEWRECORD;	
			$_SESSION['errorclass'] = ERRORCLASS;
}
			$activity = "New Contractor Add Id = ".$id.'-'.$contact_person_name; 
     		$this->log_report($activity);
        
		header("location:index.php?control=contractor_master&task=show");
		
		}
		else
		{
			  $update="UPDATE `contractor_master` SET `contact_person_name`='".$contact_person_name."',`company_name`='".$company_name."',`address`='".$address."',`mobile`='".$mobile."',`email_id`='".$email_id."',`type`='".$type."',`city_id`='".$city_id."',`modified_by`='".$_SESSION['adminid']."',`modified_date_time`='".date('Y-m-d h:i:s')."' WHERE id='".$_REQUEST['id']."'";
			
			$this->Query($update);
			$this->Execute();
			if($this->Execute()) {	
			$_SESSION['error'] = UPDATERECORD;	
			$_SESSION['errorclass'] = ERRORCLASS;
			
			
}

			$activity = "Edit Contractor Id = ".$_REQUEST['id'].'-'.$contact_person_name; 			 
     		$this->log_report($activity);
header("location:index.php?control=contractor_master&task=show");

		
		}
		
		}
	
		
		
		function status(){
		 $query="update contractor_master set status=".$_REQUEST['status'].", modified_by = '".$_SESSION['adminid']."',modified_date_time = '".date('Y-m-d h:i:s')."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		if($_REQUEST['status']=='1') {
		$activity = "Active Contractor Table ID = ".$_REQUEST['id']; } else { 	$activity = "Inactive Contractor Table ID = ".$_REQUEST['id'];} 		 
     		$this->log_report($activity);	
		header("location:index.php?control=contractor_master");
		}
		
		
		function delete(){
		
		$query="DELETE FROM contractor_master WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';
		//$this->show();
		header("location:index.php?control=contractor_master");
		
		}
		
		
		
		
		
	
	}