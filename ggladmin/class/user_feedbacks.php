<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class user_feedbackClass extends DbAccess {
		public $view='';
		public $name='user_feedback';

		
		
		function show(){
			if($_SESSION['utype']=='Employee' && $_SESSION['department_id']!='9'){
		    $department_id = " and department_id='".$_SESSION['department_id']."'";//$_REQUEST['department_id']?" and department_id='".$_SESSION['department_id']."'":'';
			   if($_SESSION['city_id']=='1'){ 
			        $city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":" and (city='Lucknow (Gomtinagar)' || city='Lucknow (Amousi)')";
			      
			   } 
			    if($_SESSION['city_id']=='2'){ 
			      $city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":" and city='Agra'";
			   }
			}
			else{
			$department_id = $_REQUEST['department_id']?" and department_id='".$_REQUEST['department_id']."'":'';	
			$city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":'';
			}
			
			$complain_type_id = $_REQUEST['complain_type_id']?" and complain_type_id='".$_REQUEST['complain_type_id']."'":'';
			//$city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":'';
		    $area = $_REQUEST['area']?" and area='".$_REQUEST['area']."'":'';
		  
	$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
	$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
	$bydate  = $dateFrom?$dateFrom:$dateTo;
	$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;
				if($_SESSION['department_id']!='1') {
		$uquery ="select * from feedback where complain_forward_department_id='' and (complain_status='Under Process' OR complain_status='') $department_id $complain_type_id  $city $area $date order by id DESC";
				} else {
				$uquery ="select * from feedback where complain_forward_department_id='' and (complain_status='Under Process' OR complain_status='') $complain_type_id  $city $area $date order by id DESC";	
				}
		
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$_SESSION['query'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		
		function close(){
		 if($_SESSION['utype']=='Employee' && $_SESSION['department_id']!='9'){
		    $department_id = " and department_id='".$_SESSION['department_id']."'";//$_REQUEST['department_id']?" and department_id='".$_SESSION['department_id']."'":'';
			   if($_SESSION['city_id']=='1'){ 
			        $city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":" and (city='Lucknow (Gomtinagar)' || city='Lucknow (Amousi)')";
			      
			   } 
			    if($_SESSION['city_id']=='2'){ 
			      $city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":" and city='Agra'";
			   }
			}
			else{
			$department_id = $_REQUEST['department_id']?" and department_id='".$_REQUEST['department_id']."'":'';	
			$city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":'';
			}
			
			$complain_type_id = $_REQUEST['complain_type_id']?" and complain_type_id='".$_REQUEST['complain_type_id']."'":'';
			//$city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":'';
		    $area = $_REQUEST['area']?" and area='".$_REQUEST['area']."'":'';
		  
	$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
	$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
	$bydate  = $dateFrom?$dateFrom:$dateTo;
	$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;
				
		 $uquery ="select * from feedback where complain_status='Close' $department_id $complain_type_id  $city $area $date order by id DESC";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$_SESSION['query'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/close.php"); 
		}
		
		
		function forwardtome_complain(){
		 if($_SESSION['utype']=='Employee' && $_SESSION['department_id']!='9'){
			 $complain_forward_department_id = " and complain_forward_department_id='".$_SESSION['department_id']."'";
		    $department_id = " and department_id='".$_SESSION['department_id']."'";//$_REQUEST['department_id']?" and department_id='".$_SESSION['department_id']."'":'';
			   if($_SESSION['city_id']=='1'){ 
			        $city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":" and (city='Lucknow (Gomtinagar)' || city='Lucknow (Amousi)')";
			      
			   } 
			    if($_SESSION['city_id']=='2'){ 
			      $city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":" and city='Agra'";
			   }
			}
			else{
			$complain_forward_department_id = "";
			$department_id = $_REQUEST['department_id']?" and department_id='".$_REQUEST['department_id']."'":'';	
			$city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":'';
			}
			
			$complain_type_id = $_REQUEST['complain_type_id']?" and complain_type_id='".$_REQUEST['complain_type_id']."'":'';
			//$city = $_REQUEST['city']?" and city='".$_REQUEST['city']."'":'';
		    $area = $_REQUEST['area']?" and area='".$_REQUEST['area']."'":'';
		  
	$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
	$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
	$bydate  = $dateFrom?$dateFrom:$dateTo;
	$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;
				
		  $uquery ="select * from feedback where 1 $complain_forward_department_id $department_id $complain_type_id  $city $area $date order by id DESC";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$_SESSION['query'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/forwardtome_complain.php"); 
		}
		
		function show_chat(){
	
				
		$uquery ="select * from feedback_response where feedback_id = '".$_REQUEST['id']."' order by id ASC";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
	
		
		
		function save(){
			
					$name = $_POST['name'];
					$date = date('Y-m-d');
					$id   = $_REQUEST['id'];
					if(!$id){
					
					$query="insert into feedback (name,date,status) value('".$name."','".$date."','1')";	
					$this->Query($query);	
					$this->Execute();
					
					$_SESSION['error'] = ADDNEWRECORD;	
					$_SESSION['errorclass'] = ERRORCLASS;
					
				header("location:index.php?control=user_feedback");
				}
				else
				{
					$update="update feedback set name='".$name."' where id='".$_REQUEST['id']."'";
					$this->Query($update);
					$this->Execute();
					
					$_SESSION['error'] = UPDATERECORD;	
            		$_SESSION['errorclass'] = ERRORCLASS;
					header("location:index.php?control=user_feedback");
				}
		
		}
		
		function save_chat(){
			
			$com_no = mysql_fetch_array(mysql_query("select * from feedback where id='".$_REQUEST['id']."'"));
			
			$feedback_id   = $_REQUEST['id'];			         $id   = $_REQUEST['id'];
			$complain_no   = $com_no['complain_no'];	  $usr_message = $_REQUEST['usr_message'];			
			$admin_message = $_REQUEST['admin_message'];	
			$complain_forward = $_REQUEST['complain_forward'];
			$complain_forward_department_id = $_REQUEST['complain_forward_department_id'];
			$complain_status  = $_REQUEST['complain_status'];
					
			$date = date('d-m-Y h:i:s');
			
				if($id){
				
				 $query="INSERT INTO `feedback_response`(`feedback_id`, `complain_no`, `usr_message`, `admin_message`, `type`, `date`, complain_forward, complain_forward_department_id, complain_status) VALUES ('".$feedback_id."','".$complain_no."','".$usr_message."','".$admin_message."', '2','".$date."','".$complain_forward."','".$complain_forward_department_id."','".$complain_status."')";
				$this->Query($query);	
				$this->Execute();
				
				$update="update feedback set complain_forward='".$complain_forward."', complain_forward_department_id='".$complain_forward_department_id."', complain_status='".$complain_status."' where id='".$id."'";
				$this->Query($update);
				$this->Execute();
				
				$_SESSION['error'] = ADDNEWRECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				header("location:index.php?control=user_feedback");
				}
				/*else
				{
					$update="update feedback set name='".$name."' where id='".$_REQUEST['id']."'";
					$this->Query($update);
					$this->Execute();
					
			$_SESSION['error'] = UPDATERECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;
					header("location:index.php?control=user_feedback");
				}*/
		
		}
		
		
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  feedback WHERE id =".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
				else {
								
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
		}
		
		function add_chat() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  feedback WHERE id=".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
				else {
								
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
		}
		
		function status(){
		$query="update feedback set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		//$this->show();	
		header("location:index.php?control=user_feedback");
		}
		
		
		
		function delete(){
		
		$query="DELETE FROM feedback WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';
		//$this->show();
		header("location:index.php?control=user_feedback");
		
		}
		
		
	}
