<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class png_domClass extends DbAccess {
		public $view='';
		public $name='png_dom';

		/*--------------------------------------Lucknow PNG DOM-------------------------------------*/
		function show(){	
			
			$asset = $_REQUEST['asset']?$_REQUEST['asset']:'';
			$search_asset = $asset?" AND asset = '".$asset."'":'';
			
			$fydate = $_REQUEST['fydate']?$_REQUEST['fydate']:'';
			if($fydate=='blank') {
			$search_fydate = " AND ng_progress_fy1920 = ''";
			}
			else {
			$search_fydate = $fydate?" AND ng_progress_fy1920 = '".$fydate."'":'';
			}
			
			
			$ninety_days = $_REQUEST['ninety_days']?$_REQUEST['ninety_days']:'';
			if($ninety_days=='2') {
			$search_ninety_days = "AND ninety_days_pendency_sch != ''";	
			}
			elseif($ninety_days!='blank') {
			$search_ninety_days = $ninety_days?" AND ninety_days_pendency_sch = '".$ninety_days."'":'';
			}
			else
			{
			$search_ninety_days = "AND ninety_days_pendency_sch = ''";	
			}
			
			
			
			$contractor = $_REQUEST['contractor']?$_REQUEST['contractor']:'';
			if($contractor!='blank') {
			$search_contractor = $contractor?" AND proj_gi_contractor_alloted = '".$contractor."'":'';
			}
			else
			{
			$search_contractor = "AND proj_gi_contractor_alloted = ''";	
			}
			
			
			$progress = $_REQUEST['progress']?$_REQUEST['progress']:'';
			if($progress=='ng_blank'){
			$search_progress = $progress?" AND proj_ng_dt = ''":'';
			} elseif($progress=='rfa_blank'){
			$search_progress = $progress?" AND proj_rfc_dt = ''":'';	
			}elseif($progress=='ng_filled'){
			$search_progress = $progress?" AND proj_ng_dt != ''":'';	
			}elseif($progress=='rfa_filled'){
			$search_progress = $progress?" AND proj_rfc_dt != ''":'';	
			}
			
			$crn =$_REQUEST['crn']?$_REQUEST['crn']:'';
			$search_crn = $crn?" AND (name LIKE '%".$crn."%' || form_no='".$crn."' || present_crn='".$crn."' || mobile='".$crn."')":'';
			
				$dateFrom = $_REQUEST['from_date_ng']?" and proj_ng_dt ='".$_REQUEST['from_date_ng']."'":'';
				$dateTo = $_REQUEST['to_date_ng']?" and proj_ng_dt ='".$_REQUEST['to_date_ng']."'":'';
				$bydate  = $dateFrom?$dateFrom:$dateTo;		
				$date = ($dateFrom && $dateTo)?" and proj_ng_dt between '".$_REQUEST['from_date_ng']."' and '".$_REQUEST['to_date_ng']."'":$bydate;
			
				$dateFrom_rfa = $_REQUEST['from_date_rfa']?" and proj_rfc_dt ='".$_REQUEST['from_date_rfa']."'":'';
				$dateTo_rfa = $_REQUEST['to_date_rfa']?" and proj_rfc_dt ='".$_REQUEST['to_date_rfa']."'":'';
				$bydate_rfa  = $dateFrom_rfa?$dateFrom_rfa:$dateTo_rfa;		
				$date_rfa = ($dateFrom_rfa && $dateTo_rfa)?" and proj_rfc_dt between '".$_REQUEST['from_date_rfa']."' and '".$_REQUEST['to_date_rfa']."'":$bydate_rfa;
			
			
		
	 $uquery ="select * from master_png_dom where 1 $search_asset $search_fydate $search_ninety_days $search_contractor $search_progress $search_crn $date $date_rfa";	
			
		
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row   = count($uresults);
		$_SESSION['png_dom'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
	$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : $this->defaultPageData();
	$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $adjacents = 4;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
		/* Paging end here */
			
		$query = $uquery." order by present_crn ASC LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
	require_once("views/".$this->name."/show.php"); 
		}
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  master_png_dom WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function view_profile() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  master_png_dom WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save(){
			
		
			$form_no=$_POST['form_no'];									$present_crn=$_POST['present_crn'];
			$asset=$_POST['asset'];										$name=$_POST['name'];
			$address=$_POST['address'];									$block_sec_khand=$_POST['block_sec_khand'];
			
			$apartment_society=$_POST['apartment_society'];     		$mou_flats_individual = $_POST['mou_flats_individual'];
			$charge_uncharge_area=$_POST['charge_uncharge_area'];     	$area=$_POST['area'];
			$mobile=$_POST['mobile'];									$landline=$_POST['landline'];
			
			$email=$_POST['email'];										$ninety_days_pendency_sch=$_POST['ninety_days_pendency_sch'];
			$fnl_status_z=$_POST['fnl_status_z'];    					$rfc_fnl_z=$_POST['rfc_fnl_z'];
			$rfc_date=$_POST['rfc_date'];								$mktg_status=$_POST['mktg_status'];
			$mktg_remark=$_POST['mktg_remark'];							$proj_gi_contractor_alloted=$_POST['proj_gi_contractor_alloted'];
			$meter_no = $_POST['meter_no'];								$initial_reading=$_POST['initial_reading'];	
			$proj_ng_dt=$_POST['proj_ng_dt'];							$proj_rfc_dt=$_POST['proj_rfc_dt']; 
			$jmr_no=$_POST['jmr_no'];									$proj_remarks=$_POST['proj_remarks'];  
			$registration_index=$_POST['registration_index']; 			$overall_comments=$_POST['overall_comments'];
			
			$form_status='1';									
			$created_by=$_POST['employee_id'];							$created_date_time=date('Y-m-d h:i:s');  
			$modified_by=$_POST['employee_id'];							$modifed_date_time=date('Y-m-d h:i:s');
			
			if($_POST['proj_ng_dt']!='') {  $ng_progress_fy1920=date('M-y',strtotime($_POST['proj_ng_dt']));	} 
			if($_POST['proj_rfc_dt']!='') { $ng_progress_fy1920=date('M-y',strtotime($_POST['proj_rfc_dt']));  }
			if($_POST['proj_rfc_dt']!='' && $_POST['proj_ng_dt']!='') { $ng_progress_fy1920=date('M-y',strtotime($_POST['proj_ng_dt'])); }
			 
			/*$registration_date=$_POST['registration_date'];			$form_id=$_POST['form_id']; 
			$status=$_POST['status'];	    							$sn = $_POST['sn'];
			$interim_crn=$_POST['interim_crn']; 						$master_status=$_POST['master_status'];			
			$second_meter_no=$_POST['2nd_meter_no'];     				$sap_meter_no=$_POST['sap_meter_no'];
			$second_initial_reading=$_POST['2nd_initial_reading'];		$rfc_date_1st=$_POST['rfc_date_1st'];
			$rfc_date_2nd=$_POST['rfc_date_2nd'];						$rfc_date=$_POST['rfc_date'];
			$rfc_contractor=$_POST['rfc_contractor'];					$ng_date_1 = $_POST['ng_date_1'];
			$ng_contractor=$_POST['ng_contractor'];						$ng_date=$_POST['ng_date'];
			$s_last_bill_date=$_POST['s_last_bill_date'];				$s_bill_status=$_POST['s_bill_status'];
			$s_customer_status=$_POST['s_customer_status'];				$s_sd_balance=$_POST['s_sd_balance'];
			$s_outstanding=$_POST['s_outstanding'];   			 		$mktg_update = $_POST['mktg_update']; 			
			$gm_rfc_dt=$_POST['gm_rfc_dt'];								$gm_ng_dt=$_POST['gm_ng_dt'];
			$gm_proj_status=$_POST['gm_proj_status'];					$gm_jmr_serial=$_POST['gm_jmr_serial'];
			$gm_jmr_hover=$_POST['gm_jmr_hover'];     					$gm_gi_cont = $_POST['gm_gi_cont'];
			$am_rfc_dt=$_POST['am_rfc_dt'];    							$am_ng_dt=$_POST['am_ng_dt'];
			$am_proj_status=$_POST['am_proj_status'];					$am_jmr_status=$_POST['am_jmr_status'];
			$am_jmr_sub_dt=$_POST['am_jmr_sub_dt'];					$am_gi_contractor=$_POST['am_gi_contractor'];	$ng_date_2=$_POST['ng_date_2']; */
			
			
			if(!$_REQUEST['id']){
		
		 $query="INSERT INTO `master_png_dom`(`form_no`, `present_crn`, `asset`, `name`, `address`, `block_sec_khand`, `apartment_society`, `mou_flats_individual`, `charge_uncharge_area`, `area`, `mobile`, `landline`,`email`,`fnl_status_z`,`rfc_fnl_z`,`rfc_date`,`mktg_status`,`ninety_days_pendency_sch`,`mktg_remark`,`proj_gi_contractor_alloted`,`meter_no`,`initial_reading`,`proj_ng_dt`,`proj_rfc_dt`,`jmr_no`, `proj_remarks`, `ng_progress_fy1920`,`registration_index`,`overall_comments`,`form_status`, `created_by`, `created_date_time`) VALUES ('".$form_no."','".$present_crn."','".$asset."','".$name."','".$address."','".$block_sec_khand."','".$apartment_society."','".$mou_flats_individual."','".$charge_uncharge_area."','".$area."','".$mobile."','".$landline."','".$email."','".$fnl_status_z."','".$rfc_fnl_z."','".$rfc_date."','".$mktg_status."','".$ninety_days_pendency_sch."','".$mktg_remark."','".$proj_gi_contractor_alloted."','".$meter_no."','".$initial_reading."','".$proj_ng_dt."','".$proj_rfc_dt."','".$jmr_no."','".$proj_remarks."','".$ng_progress_fy1920."','".$registration_index."','".$overall_comments."','".$form_status."','".$created_by."','".$created_date_time."')";	
		
			$this->Query($query);	
			$id = mysql_insert_id();
			if($this->Execute()) {	
			$_SESSION['error'] = ADDNEWRECORD;	
			$_SESSION['errorclass'] = ERRORCLASS;
}
			$activity = "New PNG Add Id = ".$_REQUEST['id']; 
     		$this->log_report($activity);
        
		header("location:index.php?control=png_dom&task=show");
		
		}
		else
		{
			 $update="UPDATE `master_png_dom` SET `form_no`='".$form_no."',`present_crn`='".$present_crn."',`asset`='".$asset."',`name`='".$name."',`address`='".$address."',`block_sec_khand`='".$block_sec_khand."',`apartment_society`='".$apartment_society."',`mou_flats_individual`='".$mou_flats_individual."',`charge_uncharge_area`='".$charge_uncharge_area."',`area`='".$area."',`mobile`='".$mobile."',`landline`='".$landline."',`email`='".$email."',`fnl_status_z`='".$fnl_status_z."',`rfc_fnl_z`='".$rfc_fnl_z."',`rfc_date`='".$rfc_date."',`mktg_status`='".$mktg_status."',`ninety_days_pendency_sch`='".$ninety_days_pendency_sch."',`mktg_remark`='".$mktg_remark."',`proj_gi_contractor_alloted`='".$proj_gi_contractor_alloted."',`meter_no`='".$meter_no."',`initial_reading`='".$initial_reading."',`proj_ng_dt`='".$proj_ng_dt."',`proj_rfc_dt`='".$proj_rfc_dt."',`jmr_no`='".$jmr_no."',`proj_remarks`='".$proj_remarks."',`ng_progress_fy1920`='".$ng_progress_fy1920."',`registration_index`='".$registration_index."',`overall_comments`='".$overall_comments."',`modified_by`='".$modified_by."',`modifed_date_time`='".$modifed_date_time."' WHERE id='".$_REQUEST['id']."'";
			
			$this->Query($update);
			$this->Execute();
			if($this->Execute()) {	
			$_SESSION['error'] = UPDATERECORD;	
			$_SESSION['errorclass'] = ERRORCLASS;
			
			
}

			$activity = "Edit PNG Id = ".$_REQUEST['id']; 			 
     		$this->log_report($activity);
			header("location:index.php?control=png_dom&task=show");

		
		}
		
		}		
		
		function status(){
		 $query="update master_png_dom set form_status=".$_REQUEST['form_status'].", modified_by = '".$_SESSION['adminid']."',modifed_date_time = '".date('Y-m-d h:i:s')."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		if($_REQUEST['form_status']=='1') {
		$activity = "Active PNG Table ID = ".$_REQUEST['id']; } else { 	$activity = "Inactive PNG Table ID = ".$_REQUEST['id'];} 		 
     		$this->log_report($activity);	
		header("location:index.php?control=png_dom");
		}
		
		
		function delete(){
		
		$query="DELETE FROM master_png_dom WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';
		//$this->show();
		header("location:index.php?control=png_dom");
		
		}
		
	/*--------------------------------------Lucknow PNG DOM-------------------------------------*/	
		
		function show_agra(){				
			
			
			/*$asset = $_REQUEST['asset']?$_REQUEST['asset']:'';
			$search_asset = $asset?" AND asset = '".$asset."'":'';
			
			$contractor = $_REQUEST['contractor']?$_REQUEST['contractor']:'';
			if($contractor!='blank') {
			$search_contractor = $contractor?" AND proj_gi_contractor_alloted = '".$contractor."'":'';
			}
			else
			{
			$search_contractor = "AND proj_gi_contractor_alloted = ''";	
			}
			$progress = $_REQUEST['progress']?$_REQUEST['progress']:'';
			if($progress=='ng_blank'){
			$search_progress = $progress?" AND proj_ng_dt = ''":'';
			} elseif($progress=='rfa_blank'){
			$search_progress = $progress?" AND proj_rfc_dt = ''":'';	
			}elseif($progress=='ng_filled'){
			$search_progress = $progress?" AND proj_ng_dt != ''":'';	
			}elseif($progress=='rfa_filled'){
			$search_progress = $progress?" AND proj_rfc_dt != ''":'';	
			}
			
			$crn =$_REQUEST['crn']?$_REQUEST['crn']:'';
			$search_crn = $crn?" AND (name LIKE '%".$crn."%' || present_crn='".$crn."' || mobile='".$crn."')":'';*/
			
			$asset = $_REQUEST['asset']?$_REQUEST['asset']:'';
			$search_asset = $asset?" AND asset = '".$asset."'":'';
			
			$fydate = $_REQUEST['fydate']?$_REQUEST['fydate']:'';
			if($fydate=='blank') {
			$search_fydate = " AND ng_progress_fy1920 = ''";
			}
			else {
			$search_fydate = $fydate?" AND ng_progress_fy1920 = '".$fydate."'":'';
			}
			
			
			$ninety_days = $_REQUEST['ninety_days']?$_REQUEST['ninety_days']:'';
			if($ninety_days=='2') {
			$search_ninety_days = "AND ninety_days_pendency_sch != ''";	
			}
			elseif($ninety_days!='blank') {
			$search_ninety_days = $ninety_days?" AND ninety_days_pendency_sch = '".$ninety_days."'":'';
			}
			else
			{
			$search_ninety_days = "AND ninety_days_pendency_sch = ''";	
			}
						
			
			$contractor = $_REQUEST['contractor']?$_REQUEST['contractor']:'';
			if($contractor!='blank') {
			$search_contractor = $contractor?" AND proj_gi_contractor_alloted = '".$contractor."'":'';
			}
			else
			{
			$search_contractor = "AND proj_gi_contractor_alloted = ''";	
			}
			
			
			$progress = $_REQUEST['progress']?$_REQUEST['progress']:'';
			if($progress=='ng_blank'){
			$search_progress = $progress?" AND proj_ng_dt = ''":'';
			} elseif($progress=='rfa_blank'){
			$search_progress = $progress?" AND proj_rfc_dt = ''":'';	
			}elseif($progress=='ng_filled'){
			$search_progress = $progress?" AND proj_ng_dt != ''":'';	
			}elseif($progress=='rfa_filled'){
			$search_progress = $progress?" AND proj_rfc_dt != ''":'';	
			}
			
			$crn =$_REQUEST['crn']?$_REQUEST['crn']:'';
			$search_crn = $crn?" AND (name LIKE '%".$crn."%' || present_crn='".$crn."' || mobile='".$crn."')":'';
			
			$dateFrom = $_REQUEST['from_date_ng']?" and proj_ng_dt ='".$_REQUEST['from_date_ng']."'":'';
			$dateTo = $_REQUEST['to_date_ng']?" and proj_ng_dt ='".$_REQUEST['to_date_ng']."'":'';
			$bydate  = $dateFrom?$dateFrom:$dateTo;		
			$date = ($dateFrom && $dateTo)?" and proj_ng_dt between '".$_REQUEST['from_date_ng']."' and '".$_REQUEST['to_date_ng']."'":$bydate;
			
			$dateFrom_rfa = $_REQUEST['from_date_rfa']?" and proj_rfc_dt ='".$_REQUEST['from_date_rfa']."'":'';
			$dateTo_rfa = $_REQUEST['to_date_rfa']?" and proj_rfc_dt ='".$_REQUEST['to_date_rfa']."'":'';
			$bydate_rfa  = $dateFrom_rfa?$dateFrom_rfa:$dateTo_rfa;		
			$date_rfa = ($dateFrom_rfa && $dateTo_rfa)?" and proj_rfc_dt between '".$_REQUEST['from_date_rfa']."' and '".$_REQUEST['to_date_rfa']."'":$bydate_rfa;
			
			//$uquery ="select `id`, `present_crn`, `asset`, `name`,`address`,`apartment_society`,`mou_flats_individual`,`landline`,`email`,`rfc_fnl_z`,`rfc_date`,`mktg_status`,`mktg_remark`,`meter_no`,`initial_reading`,`jmr_no`,`proj_remarks`,`ng_progress_fy1920`,`registration_index`,`overall_comments`,`area`, `mobile`,proj_rfc_dt,proj_ng_dt,proj_gi_contractor_alloted,fnl_status_z,form_status,registration_index,ninety_days_pendency_sch  from master_png_dom_agra where 1 $search_asset $search_fydate $search_ninety_days $search_contractor $search_progress $search_crn $date $date_rfa";
	     $uquery ="select `id`, `present_crn`, `asset`, `name`,`address`,`apartment_society`,`mou_flats_individual`,`landline`,`email`,`rfc_fnl_z`,`rfc_date`,`mktg_status`,`mktg_remark`,`meter_no`,`initial_reading`,`jmr_no`,`proj_remarks`,`ng_progress_fy1920`,`registration_index`,`area`, `mobile`,`ninety_days_pendency_sch`,`proj_ng_dt`,`proj_gi_contractor_alloted`  from master_png_dom_agra where 1 $search_asset $search_fydate $search_ninety_days $search_contractor $search_progress $search_crn $date $date_rfa";
		
		
	
			
	
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row   = count($uresults);
		$_SESSION['png_dom'] = $uquery;
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
	$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : 100;//$this->defaultPageData();
	$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $adjacents = 4;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
		/* Paging end here */
			
		 $query = $uquery. " order by present_crn ASC LIMIT ".(($page-1)*$tpages).",".$tpages;	
	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_agra.php"); 
		}
		
		function addnew_agra() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  master_png_dom_agra WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function view_profile_agra() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  master_png_dom_agra WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_agra(){
			
		
			$form_no=$_POST['form_no'];									$present_crn=$_POST['present_crn'];
			$asset=$_POST['asset'];										$name=$_POST['name'];
			$address=$_POST['address'];								
			
			$apartment_society=$_POST['apartment_society'];     		$mou_flats_individual = $_POST['mou_flats_individual'];
		   	$area=$_POST['area'];
			$mobile=$_POST['mobile'];									$landline=$_POST['landline'];
			
			$email=$_POST['email'];										$ninety_days_pendency_sch=$_POST['ninety_days_pendency_sch'];
			$fnl_status_z=$_POST['fnl_status_z'];    					$rfc_fnl_z=$_POST['rfc_fnl_z'];
			$rfc_date=$_POST['rfc_date'];								$mktg_status=$_POST['mktg_status'];
			$mktg_remark=$_POST['mktg_remark'];							$proj_gi_contractor_alloted=$_POST['proj_gi_contractor_alloted'];
			$meter_no = $_POST['meter_no'];								$initial_reading=$_POST['initial_reading'];	
			$proj_ng_dt=$_POST['proj_ng_dt'];							$proj_rfc_dt=$_POST['proj_rfc_dt']; 
			$jmr_no=$_POST['jmr_no'];									$proj_remarks=$_POST['proj_remarks'];  
			$registration_index=$_POST['registration_index']; 			$overall_comments=$_POST['overall_comments'];
			
			$form_status='1';									
			$created_by=$_POST['employee_id'];							$created_date_time=date('Y-m-d h:i:s');  
			$modified_by=$_POST['employee_id'];							$modifed_date_time=date('Y-m-d h:i:s');
			
			if($_POST['proj_ng_dt']!='') {  $ng_progress_fy1920=date('M-y',strtotime($_POST['proj_ng_dt']));	} 
			if($_POST['proj_rfc_dt']!='') { $ng_progress_fy1920=date('M-y',strtotime($_POST['proj_rfc_dt']));  }
			if($_POST['proj_rfc_dt']!='' && $_POST['proj_ng_dt']!='') { $ng_progress_fy1920=date('M-y',strtotime($_POST['proj_ng_dt'])); }
			
			
			
			if(!$_REQUEST['id']){
		
		 $query="INSERT INTO `master_png_dom_agra`(`form_no`, `present_crn`, `asset`, `name`, `address`, `apartment_society`, `mou_flats_individual`,`area`, `mobile`, `landline`,`email`,`fnl_status_z`,`rfc_fnl_z`,`rfc_date`,`mktg_status`,`ninety_days_pendency_sch`,`mktg_remark`,`proj_gi_contractor_alloted`,`meter_no`,`initial_reading`,`proj_ng_dt`,`proj_rfc_dt`,`jmr_no`, `proj_remarks`, `ng_progress_fy1920`,`registration_index`,`overall_comments`,`form_status`, `created_by`, `created_date_time`) VALUES ('".$form_no."','".$present_crn."','".$asset."','".$name."','".$address."','".$apartment_society."','".$mou_flats_individual."','".$area."','".$mobile."','".$landline."','".$email."','".$fnl_status_z."','".$rfc_fnl_z."','".$rfc_date."','".$mktg_status."','".$ninety_days_pendency_sch."','".$mktg_remark."','".$proj_gi_contractor_alloted."','".$meter_no."','".$initial_reading."','".$proj_ng_dt."','".$proj_rfc_dt."','".$jmr_no."','".$proj_remarks."','".$ng_progress_fy1920."','".$registration_index."','".$overall_comments."','".$form_status."','".$created_by."','".$created_date_time."')";	
		
			$this->Query($query);	
			$id = mysql_insert_id();
			if($this->Execute()) {	
			$_SESSION['error'] = ADDNEWRECORD;	
			$_SESSION['errorclass'] = ERRORCLASS;
}
			$activity = "New Agra PNG Add Id = ".$_REQUEST['id']; 
     		$this->log_report($activity);
        
		header("location:index.php?control=png_dom&task=show_agra");
		
		}
		else
		{
			 $update="UPDATE `master_png_dom_agra` SET `form_no`='".$form_no."', `present_crn`='".$present_crn."',`asset`='".$asset."',`name`='".$name."',`address`='".$address."',`apartment_society`='".$apartment_society."',`mou_flats_individual`='".$mou_flats_individual."',`area`='".$area."',`mobile`='".$mobile."',`landline`='".$landline."',`email`='".$email."',`fnl_status_z`='".$fnl_status_z."',`rfc_fnl_z`='".$rfc_fnl_z."',`rfc_date`='".$rfc_date."',`mktg_status`='".$mktg_status."',`ninety_days_pendency_sch`='".$ninety_days_pendency_sch."',`mktg_remark`='".$mktg_remark."',`proj_gi_contractor_alloted`='".$proj_gi_contractor_alloted."',`meter_no`='".$meter_no."',`initial_reading`='".$initial_reading."',`proj_ng_dt`='".$proj_ng_dt."',`proj_rfc_dt`='".$proj_rfc_dt."',`jmr_no`='".$jmr_no."',`proj_remarks`='".$proj_remarks."',`ng_progress_fy1920`='".$ng_progress_fy1920."',`registration_index`='".$registration_index."',`overall_comments`='".$overall_comments."',`modified_by`='".$modified_by."',`modifed_date_time`='".$modifed_date_time."' WHERE id='".$_REQUEST['id']."'";
			
			
			$this->Query($update);
			$this->Execute();
			if($this->Execute()) {	
			$_SESSION['error'] = UPDATERECORD;	
			$_SESSION['errorclass'] = ERRORCLASS;
			
			
}

			$activity = "Edit Agra PNG Id = ".$_REQUEST['id']; 			 
     		$this->log_report($activity);
			header("location:index.php?control=png_dom&task=show_agra");

		
		}
		
		}		
		
		function status_agra(){
		 $query="update master_png_dom_agra set form_status=".$_REQUEST['form_status'].", modified_by = '".$_SESSION['adminid']."',modifed_date_time = '".date('Y-m-d h:i:s')."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		
		if($_REQUEST['form_status']=='1') {
		$activity = "Active PNG Table ID = ".$_REQUEST['id']; } else { 	$activity = "Inactive PNG Table ID = ".$_REQUEST['id'];} 		 
     		$this->log_report($activity);	
		header("location:index.php?control=png_dom&task=show_agra");
		}
		
		
		function delete_agra(){
		
		$query="DELETE FROM master_png_dom_agra WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';
		//$this->show();
		header("location:index.php?control=png_dom&task=show_agra");
		
		}
		
	
	}