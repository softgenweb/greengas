<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class company_masterClass extends DbAccess {
		public $view='';
		public $name='company_master';

		
		
		function show(){	
		$uquery ="SELECT * FROM `company` WHERE 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
	
		
		

		
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM `company` WHERE id =".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
				else {
								
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
		}

		function save(){

			$name = mysql_real_escape_string($_REQUEST['name']);
			$email = mysql_real_escape_string($_REQUEST['email']);
			$helpline_no = mysql_real_escape_string($_REQUEST['helpline_no']);
			$complain_no = mysql_real_escape_string($_REQUEST['complain_no']);
			$branch_city = mysql_real_escape_string($_REQUEST['branch_city']);
			$address1 = mysql_real_escape_string($_REQUEST['address1']);
			$address2 = mysql_real_escape_string($_REQUEST['address2']);
			$gst_no = mysql_real_escape_string($_REQUEST['gst_no']);
			$gstin_type = mysql_real_escape_string($_REQUEST['gstin_type']);
			$tin_no = mysql_real_escape_string($_REQUEST['tin_no']);
			$website = mysql_real_escape_string($_REQUEST['website']);
			$pin_no = mysql_real_escape_string($_REQUEST['pin_no']);
			$state = mysql_real_escape_string($_REQUEST['state']);
			$country = mysql_real_escape_string($_REQUEST['country']);
			$branch_code = strtoupper($_REQUEST['branch_code']);
			$network_tarrif = mysql_real_escape_string($_REQUEST['network_tarrif']);
			$png_rate1 = mysql_real_escape_string($_REQUEST['png_rate1']);
			$png_rate2 = mysql_real_escape_string($_REQUEST['png_rate2']);
			$terms_condition = mysql_real_escape_string($_REQUEST['terms_condition']);
			
			$signature_old = mysql_real_escape_string($_REQUEST['signature_old']);

			$id = $_REQUEST['id'];
			if(!$id){
			 $query = "INSERT INTO `company`(`name`, `email`, `helpline_no`, `complain_no`, `address1`, `address2`, `pin_no`, `branch_city`, `state`, `country`, `gst_no`, `gstin_type`, `tin_no`, `website`, `branch_code`, `network_tarrif`, `png_rate1`, `png_rate2`, `terms_condition`, `created_date`) VALUES ('".$name."', '".$email."', '".$helpline_no."', '".$complain_no."', '".$address1."', '".$address2."', '".$pin_no."', '".$branch_city."', '".$state."', '".$country."', '".$gst_no."', '".$gstin_type."', '".$tin_no."', '".$website."', '".$branch_code."', '".$network_tarrif."', '".$png_rate1."', '".$png_rate2."', '".$terms_condition."', '".date('Y-m-d H:i:s')."')";
			
			// exit;
			mysql_query($query);
			$lst_id = mysql_insert_id();

				if (!is_dir('media/company/'.$lst_id)) {			
					mkdir('media/company/'.$lst_id,0777,true);
				}
						$tmpFilePath = $_FILES['signature']['tmp_name'];
					if($tmpFilePath != ""){
						$shortname = $_FILES['signature']['name'];
						$folder = "media/company/".$lst_id.'/';
						move_uploaded_file($tmpFilePath , $folder.$lst_id.$shortname);

						$query1="UPDATE `company` SET signature = '".($shortname?$lst_id.'/'.$lst_id.$shortname:'')."' WHERE `id` = '".$lst_id."'";
					// exit;
						$ex_query = mysql_query($query1);
					}
				
				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=company_master");

				}else{

					$query = "UPDATE `company` SET `name`='".$name."',`email`='".$email."',`helpline_no`='".$helpline_no."',`complain_no`='".$complain_no."',`branch_city`='".$branch_city."',`address1`='".$address1."',`address2`='".$address2."',`pin_no`='".$pin_no."',`state`='".$state."',`country`='".$country."',`gst_no`='".$gst_no."',`gstin_type`='".$gstin_type."',`tin_no`='".$tin_no."',`website`='".$website."',`branch_code`='".$branch_code."',`network_tarrif`='".$network_tarrif."',`png_rate1`='".$png_rate1."',`png_rate2`='".$png_rate2."',`terms_condition`='".$terms_condition."' WHERE `id`='".$id."'";

					mysql_query($query);


				if (!is_dir('media/company/'.$id)) {			
					mkdir('media/company/'.$id,0777,true);
				}
				$tmpFilePath = $_FILES['signature']['tmp_name'];
				
				if($tmpFilePath != ""){
						$shortname = $_FILES['signature']['name'];
						$folder = "media/company/".$id.'/';
						move_uploaded_file($tmpFilePath , $folder.$id.$shortname);

						$query1="UPDATE `company` SET signature = '".($shortname?$id.'/'.$id.$shortname:'')."' WHERE `id` = '".$id."'";
					// exit;
						$ex_query = mysql_query($query1);
					}
				
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=company_master");
				}

		}
				
		function status(){
			$query="UPDATE `company` SET `status`=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$this->task="show";
			$this->view ='show';
			//$this->show();	
			header("location:index.php?control=company_master");
		}
		
		
		
		function delete(){
		
		$query="DELETE FROM `company` WHERE `id` in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';
		//$this->show();
		header("location:index.php?control=company_master");
		
		}
		
		
	}
