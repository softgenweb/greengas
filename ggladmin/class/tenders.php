<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class tenderClass extends DbAccess {
		public $view='';
		public $name='tender';

		
		/***************************************************** POST START **********************************************************/
		
		function show()
		{
		$search = $_REQUEST['search_name']?" project='".$_REQUEST['search_name']."'":' 1 ';
			
				$dateFrom = $_REQUEST['from_date']?" and tender_date ='".$_REQUEST['from_date']."'":'';
				$dateTo = $_REQUEST['to_date']?" and tender_date ='".$_REQUEST['to_date']."'":'';
				$bydate  = $dateFrom?$dateFrom:$dateTo;

			$date = ($dateFrom && $dateTo)?" and tender_date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;
		  $uquery ="select * from tender where $search $date order by tender_date DESC ";	
				
		//$uquery ="select * from tender where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show.php"); 
		}
	

		
		
		
		function status(){
		$query="update tender set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$this->task="show";
		$this->view ='show';
		
			
			$_SESSION['error'] = STATUS;	
            $_SESSION['errorclass'] = ERRORCLASS;
		//$this->show();	
		header("location:index.php?control=tender&task=show");
		}
		
		function save(){
			$emp_id=$_SESSION['adminid'];
			$tender_no=$_POST['tender_no'];
			$tender_date=$_POST['tender_date'];
			$tender_closing_date=$_REQUEST['tender_closing_date'];
			$client=$_REQUEST['client'];
			$project=$_REQUEST['project'];
			$item=$_POST['item'];
			$coordinating_office=$_POST['coordinating_office'];
			$eligibility_criteria=$_REQUEST['eligibility_criteria'];
			$last_date=$_REQUEST['last_date'];
			$sale_start_date=$_REQUEST['sale_start_date'];
			$prebid_summary=$_POST['prebid_summary'];
			$sale_end_date=$_REQUEST['sale_end_date'];
			$document_cost=$_REQUEST['document_cost'];
			$submission_place=$_REQUEST['submission_place'];
			$execution_location=$_REQUEST['execution_location'];
			$tender_opening=$_REQUEST['tender_opening'];
			
			if(!$_REQUEST['id'])
			{
				
		 $query="INSERT INTO `tender`(`emp_id`, `tender_no`, `tender_date`, `tender_closing_date`, `client`, `project`, `item`, `coordinating_office`, `eligibility_criteria`, `last_date`, `sale_start_date`, `prebid_summary`, `sale_end_date`, `document_cost`, `submission_place`, `execution_location`, `tender_opening`, `status`, `time`, `date`) VALUES ('".$emp_id."','".$tender_no."','".$tender_date."','".$tender_closing_date."','".$client."','".$project."','".$item."','".$coordinating_office."','".$eligibility_criteria."','".$last_date."','".$sale_start_date."','".$prebid_summary."','".$sale_end_date."','".$document_cost."','".$submission_place."','".$execution_location."','".$tender_opening."','1','".date("Y-m-d")."','".date("H:i:s")."')";
		 
		$ece = mysql_query($query);
			 $id = mysql_insert_id();
			
			if (!is_dir('./../media/tender/'.$id)) {
			// dir doesn't exist, make it
			mkdir('./../media/tender/'.$id);
			}
			
			if(count($_FILES['tender_file']['name']) > 0){
				for($i=0; $i<count($_FILES['tender_file']['name']); $i++) {
					
					 $tmpFilePath = $_FILES['tender_file']['tmp_name'][$i];
					 
					 if($tmpFilePath != ""){
						 $shortname = $_FILES['tender_file']['name'][$i];
						$folder = "./../media/tender/".$id.'/';
						move_uploaded_file($tmpFilePath , $folder.$id.$shortname);
						
					 $query1="INSERT INTO `tender_attachment` (`tender_id`,`tender_file`,`date`) VALUES('".$id."', '".($shortname?$id.'/'.$id.$shortname:'')."','".date('Y-m-d')."')";
					$ex_query = mysql_query($query1);
                
					 }
				}
			}
			
		/*$this->Query($query);	
		$this->Execute();
			
			$_SESSION['error'] = ADDNEWRECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;*/
		header("location:index.php?control=tender");
		}
		else
		{ 
		$id = $_REQUEST['id'];
			$update="update tender set emp_id='".$emp_id."', tender_no='".$tender_no."',tender_date='".$tender_date."',tender_closing_date='".$tender_closing_date."',client='".$client."', project='".$project."',item='".$item."',coordinating_office='".$coordinating_office."',eligibility_criteria='".$eligibility_criteria."', last_date='".$last_date."',sale_start_date='".$sale_start_date."',prebid_summary='".$prebid_summary."',sale_end_date='".$sale_end_date."', document_cost='".$document_cost."',submission_place='".$submission_place."',execution_location='".$execution_location."',tender_opening='".$tender_opening."',update_datetime = '".date('Y-m-d H:i:s')."' where id='".$id."'";
			
			$exe_update = mysql_query($update);
			
			if (!is_dir('./../media/tender/'.$id)) {
				// dir doesn't exist, make it
				mkdir('./../media/tender/'.$id);
				}
				$folder = "./../media/tender/".$id.'/';
				$unlink = "./../media/tender/";
				
				if(count($_FILES['tender_file']['name']) > 0)
			{
				for($i=0; $i<count($_FILES['tender_file']['name']); $i++) {
					
					 $tmpFilePath = $_FILES['tender_file']['tmp_name'][$i];
					 
					 if($tmpFilePath != ""){
						 $shortname = $_FILES['tender_file']['name'][$i];
						$folder = "./../media/tender/".$id.'/';
						move_uploaded_file($tmpFilePath , $folder.$id.$shortname);
						
					 $query1="INSERT INTO `tender_attachment` (`tender_id`,`tender_file`,`date`) VALUES('".$id."', '".($shortname?$id.'/'.$id.$shortname:'')."','".date('Y-m-d')."')";
					
					$ex_query = mysql_query($query1);
                
					 }
				}
			}
			/*$this->Query($update);
			$this->Execute();*/
			
			
			
			$sqlemail = mysql_query("select email from tender_download_user where tender_id='".$id."' order by id ASC");
				while($tender_email = mysql_fetch_array($sqlemail))	{	
				  
							if($tender_email['email']) {
					
					      $email = "support@gglonline.net";
											
						  $to    =    $tender_email['email'];// "support@gglonline.net";
						$subject = 'Green Gas Tender Corrigendum';
                        $message .= " Please find attached corrigendum against the  tender –".$item;
						$message .= "<br>";
						$message .= "<a href='www.gglonline.net/ggluser/tender_download.php?id=".$id."'>Click Here To Download</a>";
						
						
						$headers = 'MIME-Version: 1.0' . "\r\n";
						
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$headers .= 'From: Green Gas <'.$email.'>'.  "\r\n";
						$headers .= 'Reply-To: '.$email.'' . "\r\n";
						mail($to, $subject, $message, $headers);
						
					}
			
				}
			
				
			
			
				
			$_SESSION['error'] = UPDATERECORD;	
            $_SESSION['errorclass'] = ERRORCLASS;
			header("location:index.php?control=tender");
		}
		
		}
		
	
		
		
		function delete(){
		
		$query="DELETE FROM tender WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		$this->Execute();	
		$this->task="show";
		$this->view ='show';	
			
			$_SESSION['error'] = DELETE;	
            $_SESSION['errorclass'] = ERRORCLASS;
		//$this->show();
		header("location:index.php?control=tender&task=show");
		
		}
		
		function addnew() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  tender WHERE id=".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		function addnew_view() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  tender WHERE id=".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		
		
			
		function download_tender()
		{
		
			    $tender_id = $_REQUEST['tender_id']?" and tender_id ='".$_REQUEST['tender_id']."'":'';
				$dateFrom = $_REQUEST['from_date']?" and tender_date ='".$_REQUEST['from_date']."'":'';
				$dateTo = $_REQUEST['to_date']?" and tender_date ='".$_REQUEST['to_date']."'":'';
				$bydate  = $dateFrom?$dateFrom:$dateTo;

			$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;
		  $uquery ="select * from tender_download_user where 1 $tender_id $date order by date DESC ";	
				
		//$uquery ="select * from tender where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/download_tender.php"); 
		}
		

	}
