<?php
require_once('dbaccess.php');
require_once('textconfig/config.php');		

if(file_exists('configuration.php')){

	require_once('configuration.php');
}

class user_applynewconnectionClass extends DbAccess {
	public $view='';
	public $name='user_applynewconnection';



	function show(){
		
		$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
		$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
		$bydate  = $dateFrom?$dateFrom:$dateTo;
		
		$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;

		$uquery ="select * from new_connection where 1 and payment_status='SUCCESS' $date";
		$_SESSION['qry'] = $uquery;
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		
		/* Paging start here */
		$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/show.php"); 
		}
		
		function show_admin(){
			if($_POST['search']) {

				$city_id = $_REQUEST['city_id']?$_REQUEST['city_id']:'';			
				$search_city_id = $city_id?" AND city_id = '".$city_id."'":'';

				$asset_id = $_REQUEST['asset_id']?$_REQUEST['asset_id']:'';			
				$search_asset_id = $asset_id?" AND asset_id = '".$asset_id."'":'';
				
				$area_id = $_REQUEST['area_id']?$_REQUEST['area_id']:'';			
				$search_area_id = $area_id?" AND area_id = '".$area_id."'":'';

				$contractor = $_REQUEST['contractor']?$_REQUEST['contractor']:'';
				if($contractor!='blank') {
					$search_contractor = $contractor?" AND proj_gi_contractor_alloted = '".$contractor."'":'';
				}
				else
				{
					$search_contractor = "AND proj_gi_contractor_alloted = ''";	
				}
				$progress = $_REQUEST['progress']?$_REQUEST['progress']:'';
				if($progress=='ng_blank'){
					$search_progress = $progress?" AND proj_ng_dt = ''":'';
				} elseif($progress=='rfa_blank'){
					$search_progress = $progress?" AND mktg_rfa_dt = ''":'';	
				}elseif($progress=='ng_filled'){
					$search_progress = $progress?" AND proj_ng_dt != ''":'';	
				}elseif($progress=='rfa_filled'){
					$search_progress = $progress?" AND mktg_rfa_dt != ''":'';	
				}

				$crn =$_REQUEST['crn']?$_REQUEST['crn']:'';
				$search_crn = $crn?" AND (cust_name LIKE '%".$crn."%' || application_no='".$crn."' || sap_crn='".$crn."' ||  mobile_no='".$crn."')":'';

				$dateFrom = $_REQUEST['from_date']?" and txnDateTime LIKE '%".$_REQUEST['from_date']."%'":'';
				$dateTo = $_REQUEST['to_date']?" and txnDateTime LIKE '%".$_REQUEST['to_date']."%'":'';
				$bydate  = $dateFrom?$dateFrom:$dateTo;

				$paymentdate = ($dateFrom && $dateTo)?" and txnDateTime between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."%'":$bydate;

				$uquery ="select * from admin_new_connection where 1 and verify_status ='2' $search_city_id $search_asset_id $search_area_id $search_contractor $search_progress $search_crn $paymentdate";

			} else {
				
				if($_SESSION['department_id']=='1' || $_SESSION['department_id']=='' || $_SESSION['department_id']=='4' || $_SESSION['department_id']=='6')
				{
					$uquery ="select * from admin_new_connection where 1 and verify_status ='2'";	
				}
				else
				{
					$uquery ="select * from admin_new_connection where 1 and verify_status ='2' and city_id = '".$_SESSION['city_id']."'";
				}
				

			}
			$_SESSION['qry'] = $uquery;
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);

			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/show_admin.php"); 
		}
		
		function show_png_admin(){
			
			if($_POST['search']) {

				$city_id = $_REQUEST['city_id']?$_REQUEST['city_id']:'';			
				$search_city_id = $city_id?" AND city_id = '".$city_id."'":'';

				$asset_id = $_REQUEST['asset_id']?$_REQUEST['asset_id']:'';			
				$search_asset_id = $asset_id?" AND asset_id = '".$asset_id."'":'';

				$area_id = $_REQUEST['area_id']?$_REQUEST['area_id']:'';			
				$search_area_id = $area_id?" AND area_id = '".$area_id."'":'';

				$fydate = $_REQUEST['fydate']?$_REQUEST['fydate']:'';
				if($fydate=='blank') {
					$search_fydate = " AND ng_progress_fy1920 = ''";
				}
				else {
					$search_fydate = $fydate?" AND ng_progress_fy1920 = '".$fydate."'":'';
				}


				$ninety_days = $_REQUEST['ninety_days']?$_REQUEST['ninety_days']:'';
				if($ninety_days=='2') {
					$search_ninety_days = "AND ninety_days_pendency_sch IN ('2','')";	
				}
				else
				{
					$search_ninety_days = $ninety_days?" AND ninety_days_pendency_sch = '".$ninety_days."'":'';
				}


				$contractor = $_REQUEST['contractor']?$_REQUEST['contractor']:'';
				if($contractor!='blank') {
					$search_contractor = $contractor?" AND proj_gi_contractor_alloted = '".$contractor."'":'';
				}
				else
				{
					$search_contractor = "AND proj_gi_contractor_alloted = ''";	
				}
				$progress = $_REQUEST['progress']?$_REQUEST['progress']:'';
				if($progress=='ng_blank'){
					$search_progress = $progress?" AND proj_ng_dt = ''":'';
				} elseif($progress=='rfa_blank'){
					$search_progress = $progress?" AND mktg_rfa_dt = ''":'';	
				}elseif($progress=='ng_filled'){
					$search_progress = $progress?" AND proj_ng_dt != ''":'';	
				}elseif($progress=='rfa_filled'){
					$search_progress = $progress?" AND mktg_rfa_dt != ''":'';	
				}

				$crn =$_REQUEST['crn']?$_REQUEST['crn']:'';
				$search_crn = $crn?" AND (cust_name LIKE '%".$crn."%' || application_no='".$crn."'  || sap_crn='".$crn."'||   mobile_no='".$crn."')":'';

				$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
				$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
				$bydate  = $dateFrom?$dateFrom:$dateTo;

				$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;

				$dateFrom_ng = $_REQUEST['from_date_ng']?" and proj_ng_dt ='".$_REQUEST['from_date_ng']."'":'';
				$dateTo_ng = $_REQUEST['to_date_ng']?" and proj_ng_dt ='".$_REQUEST['to_date_ng']."'":'';
				$bydate_ng  = $dateFrom_ng?$dateFrom_ng:$dateTo_ng;		
				$date_ng = ($dateFrom_ng && $dateTo_ng)?" and proj_ng_dt between '".$_REQUEST['from_date_ng']."' and '".$_REQUEST['to_date_ng']."'":$bydate_ng;

				$dateFrom_rfa = $_REQUEST['from_date_rfa']?" and mktg_rfa_dt ='".$_REQUEST['from_date_rfa']."'":'';
				$dateTo_rfa = $_REQUEST['to_date_rfa']?" and mktg_rfa_dt ='".$_REQUEST['to_date_rfa']."'":'';
				$bydate_rfa  = $dateFrom_rfa?$dateFrom_rfa:$dateTo_rfa;		
				$date_rfa = ($dateFrom_rfa && $dateTo_rfa)?" and mktg_rfa_dt between '".$_REQUEST['from_date_rfa']."' and '".$_REQUEST['to_date_rfa']."'":$bydate_rfa;

				$uquery ="select * from admin_new_connection where 1 and verify_status ='2' $search_city_id $search_asset_id  $search_area_id$search_contractor $search_progress $search_crn $date $date_ng $date_rfa";

			} 
			else {
				if($_SESSION['department_id']=='1' || $_SESSION['department_id']=='' || $_SESSION['department_id']=='6') 
				{
					$uquery ="select * from admin_new_connection where 1 and verify_status ='2'";	
				}
				else
				{
					$uquery ="select * from admin_new_connection where 1 and verify_status ='2' and city_id = '".$_SESSION['city_id']."'";
				}
			}
			$_SESSION['qry'] = $uquery;
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);

			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/show_png_admin.php"); 
		}
		
		function total_connection(){

			$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
			$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
			$bydate  = $dateFrom?$dateFrom:$dateTo;

			$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;

			$uquery ="select * from new_connection where 1 $date";
			$_SESSION['qry'] = $uquery;
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);

			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/total_connection.php"); 
		}
		
		function pending_connection(){

			$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
			$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
			$bydate  = $dateFrom?$dateFrom:$dateTo;

			$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;

			$uquery ="select * from new_connection where 1 and payment_status!='SUCCESS' $date";
			$_SESSION['qry'] = $uquery;
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);

			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/pending_connection.php"); 
		}
		
		function pending_connection_admin(){
			if($_POST['search']) {

				$city_id = $_REQUEST['city_id']?$_REQUEST['city_id']:'';			
				$search_city_id = $city_id?" AND city_id = '".$city_id."'":'';

				$asset_id = $_REQUEST['asset_id']?$_REQUEST['asset_id']:'';			
				$search_asset_id = $asset_id?" AND asset_id = '".$asset_id."'":'';

				$area_id = $_REQUEST['area_id']?$_REQUEST['area_id']:'';			
				$search_area_id = $area_id?" AND area_id = '".$area_id."'":'';

				$contractor = $_REQUEST['contractor']?$_REQUEST['contractor']:'';
				if($contractor!='blank') {
					$search_contractor = $contractor?" AND proj_gi_contractor_alloted = '".$contractor."'":'';
				}
				else
				{
					$search_contractor = "AND proj_gi_contractor_alloted = ''";	
				}
				$progress = $_REQUEST['progress']?$_REQUEST['progress']:'';
				if($progress=='ng_blank'){
					$search_progress = $progress?" AND proj_ng_dt = ''":'';
				} elseif($progress=='rfa_blank'){
					$search_progress = $progress?" AND mktg_rfa_dt = ''":'';	
				}elseif($progress=='ng_filled'){
					$search_progress = $progress?" AND proj_ng_dt != ''":'';	
				}elseif($progress=='rfa_filled'){
					$search_progress = $progress?" AND mktg_rfa_dt != ''":'';	
				}

				$crn =$_REQUEST['crn']?$_REQUEST['crn']:'';
				$search_crn = $crn?" AND (cust_name LIKE '%".$crn."%' || application_no='".$crn."' || crn='".$crn."' ||  mobile_no='".$crn."')":'';

				$dateFrom = $_REQUEST['from_date']?" and date ='".$_REQUEST['from_date']."'":'';
				$dateTo = $_REQUEST['to_date']?" and date ='".$_REQUEST['to_date']."'":'';
				$bydate  = $dateFrom?$dateFrom:$dateTo;

				$date = ($dateFrom && $dateTo)?" and date between '".$_REQUEST['from_date']."' and '".$_REQUEST['to_date']."'":$bydate;

				$uquery ="select * from admin_new_connection where 1 and verify_status !='2' $search_city_id $search_asset_id $search_area_id $search_contractor $search_progress $search_crn $date";

			} else {
				if($_SESSION['department_id']=='1' || $_SESSION['department_id']=='' || $_SESSION['department_id']=='6') 
				{
					$uquery ="select * from admin_new_connection where 1 and verify_status !='2'";	
				}
				else
				{
					$uquery ="select * from admin_new_connection where 1 and verify_status  !='2' and city_id = '".$_SESSION['city_id']."'";
				}	

			}
			$_SESSION['qry'] = $uquery;
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);

			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/pending_connection_admin.php"); 
		}
		
		function show_detail(){	
			$uquery ="select * from new_connection where id = '".$_REQUEST['id']."'";
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function show_detail_admin(){	
			$uquery ="select * from admin_new_connection where id = '".$_REQUEST['id']."'";
			$this->Query($uquery);
			$uresults = $this->fetchArray();	
			$tdata=count($uresults);
			/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/
			$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			/* Paging end here */	
			$query = $uquery. " LIMIT ".(($page-1)*$tpages).",".$tpages;
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
		function save(){
			
			if( $_SESSION['adminid']=='') {  
				header('location:login.php');	
			}

			if($_SESSION['department_id']=='11' || $_SESSION['department_id']=='3'){

				$connection_type = $_REQUEST['connection_type'];      $city_id = $_REQUEST['city_id'];
				$area_id = $_REQUEST['area_id'];                      $society_id = $_REQUEST['society_id'];
				$scheme_id = $_REQUEST['scheme_id']; 				  $state_id = $_REQUEST['state_id'];				
				$lpg_connection = $_REQUEST['lpg_connection'];
				$lpg_comp = $_REQUEST['lpg_comp'];                    $lpg_comp_other = $_REQUEST['lpg_comp_other'];
				$lpg_consumer_no = $_REQUEST['lpg_consumer_no'];      $lpg_delar_name = $_REQUEST['lpg_delar_name'];				
				$adhaar = $_REQUEST['adhaar'];
				$cust_name = $_REQUEST['cust_name'];                  $father_name = $_REQUEST['father_name'];
				$house_type = $_REQUEST['house_type'];                $address = $_REQUEST['address']; 
				$flat_no = $_REQUEST['flat_no'];
				$land_mark = $_REQUEST['land_mark'];                  $pincode = $_REQUEST['pincode'];
				$telephone_no1 = $_REQUEST['telephone_no1'];          $mobile_no = $_REQUEST['mobile_no'];
				$telephone_no2 = $_REQUEST['telephone_no2']; 
				$telephone_no3 = $_REQUEST['telephone_no3']; 
				$email_id = $_REQUEST['email_id']; 
				$owner_type = $_REQUEST['owner_type'];                $id_proof = $_REQUEST['id_proof'];
				$address_proof = $_REQUEST['address_proof'];          $address_proof_attechment = $_REQUEST['address_proof_attechment'];
				$ggl_email = $_REQUEST['ggl_email'];                  $term_condition = $_REQUEST['term_condition'];
				$status = $_REQUEST['status'];                        $amount = $_REQUEST['amount'];
				$time = $_REQUEST['time'];							  $employee_id = $_REQUEST['employee_id'];	
				$date = $_REQUEST['date'];							  $form_no = $_REQUEST['form_no'];
				if($_REQUEST['form_date']!='')
					{ $form_date = $_REQUEST['form_date']; } else { $form_date = date('d/m/Y');  } 
				
				
				$asset_detail = mysql_fetch_array(mysql_query("select * from area where id = '".$_REQUEST['area_id']."'"));
				$asset_id = $asset_detail['asset_id'];
				
				
				
				$id   = $_REQUEST['id'];

				$userID = $id?' and id!="'.$id.'"':'';


				/**************************** Start Check for duplicate CRN value ************************/	
			//echo "select crn_main_value,id,crn from admin_new_connection where city_id = '".$city_id."' and area_id = '".$area_id."' and connection_type='".$connection_type."' and society_id = '".$society_id."' ORDER BY crn_main_value DESC";	

				$fetch_crn = mysql_fetch_array(mysql_query("select crn_main_value,id,crn from admin_new_connection where city_id = '".$city_id."' and area_id = '".$area_id."' and connection_type='".$connection_type."' and society_id = '".$society_id."' ORDER BY crn_main_value DESC"));

				if($fetch_crn['crn_main_value']!=''){ $rand_no    = $fetch_crn['crn_main_value']+1;	} 
				else{			$rand_no    = '1';  	}

				$count_id_length = strlen($rand_no);

				if($count_id_length==1){ $rand_new    = '000'.$rand_no;	} 
				if($count_id_length==2){ $rand_new    = '00'.$rand_no;  } 
				if($count_id_length==3){ $rand_new    = '0'.$rand_no;   }  



				//$fecth_connection_status = mysql_fetch_array(mysql_query("select * from customer_connection_status where connection_status = 'Unchecked Registration'"));
				

				$sel_city = mysql_fetch_array(mysql_query("select * from city where id ='".$city_id."'"));
				$sel_area = mysql_fetch_array(mysql_query("select * from area where id ='".$area_id."'"));
				$sel_society = mysql_fetch_array(mysql_query("select * from society where id ='".$society_id."'"));
				
				$new_crn = $sel_city['city_code'].$_SESSION['customer_type'].$sel_area['area_code'].$sel_society['society_code'].$rand_new;
				
				$crn = $sel_city['city_code'].$_SESSION['customer_type'].$sel_area['area_code'].$sel_society['society_code'].$rand_new.'0';

				/****************************END Check for duplicate CRN value ************************/	
		// echo "select * from admin_new_connection where crn = '".$new_crn."' $userID ";

				$check_crn = mysql_fetch_array(mysql_query("select * from admin_new_connection where crn = '".$new_crn."' $userID "));
				if($check_crn['id']!='') {
					$_SESSION['error'] = DUPLICATE;	
					$_SESSION['errorclass'] = ERRORCLASS;
					header("location:index.php?control=user_applynewconnection&task=pending_connection_admin");	
				} 
				else{		

					if(!$id){


						$chk = mysql_fetch_array(mysql_query("select id from admin_new_connection where form_no = '".$form_no."'"));
						if(!$chk){
							$new_connection = "INSERT INTO `admin_new_connection`(`form_no`,`form_date`,`connection_type`,`state_id`, `city_id`,`asset_id`, `area_id`, `society_id`, `scheme_id`,`lpg_connection`, `lpg_comp`, `lpg_comp_other`, `lpg_consumer_no`, `lpg_delar_name`, `cust_name`, `father_name`, `house_type`, `flat_no`, `address`, `land_mark`, `pincode`, `telephone_no1`,`telephone_no2`, `telephone_no3`,`mobile_no`, `email_id`, `owner_type`, `id_proof`, `address_proof`, `ggl_email`, `term_condition`, `amount`, `status`,`employee_id`,`time`, `date`) VALUES ('".$form_no."','".$form_date."','".$connection_type."','".$state_id."','".$city_id."','".$asset_id."','".$area_id."','".$society_id."', '".$scheme_id."','".$lpg_connection."', '".$lpg_comp."', '".$lpg_comp_other."', '".$lpg_consumer_no."', '".$lpg_delar_name."', '".$cust_name."', '".$father_name."','".$house_type."','".$flat_no."','".$address."','".$land_mark."','".$pincode."','".$telephone_no1."','".$telephone_no2."','".$telephone_no3."','".$mobile_no."','".$email_id."','".$owner_type."','".$id_proof."','".$address_proof."','".$ggl_email."','".$term_condition."','".$amount."','1','".$employee_id."','".date('h:i:s')."','".date('Y-m-d')."')";


							$exe_new_connection = mysql_query($new_connection);
							$id = mysql_insert_id();
							$count_id_length = strlen($id);



							$res = mysql_fetch_array(mysql_query("SELECT * FROM  scheme WHERE id = '".$scheme_id."'"));
							$emi  = $res['emi'];
							$emi_amount  = $res['emi_amount'];
							$emi_installment  = $res['emi_installment'];	
							$zero_amount = $res['security_deposite']+$res['gas_supply']+$res['non_refund'];
							$total_amount  = ($emi=='Yes')?($emi_installment*$emi_amount):($res['registration_fee']=='0')?$zero_amount:$res['registration_fee'];


							$update_crn = "update admin_new_connection set application_no='".$crn."',`city_code`='".$sel_city['city_code']."',`customer_type`='".$_SESSION['customer_type']."',`area_code`='".$sel_area['area_code']."',`society_code`='".$sel_society['society_code']."',crn_value = '".$rand_new."',crn_main_value = '".$rand_no."',crn = '".$new_crn."', emi='".$emi."', emi_amount='".$emi_amount."', emi_installment='".$emi_installment."', total_amount='".$total_amount."' where id='".$id."'";
							$exe_crn_query = mysql_query($update_crn);




							if($res['mou']==1){
								$mou_sql = "update admin_new_connection set mou='".$res['mou']."', mou_value_deducted_against_registration='".$res['mou_value_deducted_against_registration']."', mou_customer_paid='".$res['mou_customer_paid']."', mou_total_amount='".$res['registration_fee']."'  where id='".$id."'";
								$mou_update = mysql_query($mou_sql);

								$mou_remaining_amount = $res['mou_remaining_amount']-$res['mou_value_deducted_against_registration'];
								$total_registration = $res['total_registration']+1;

								$scheme_sql = "update scheme set  mou_remaining_amount='".$mou_remaining_amount."', total_registration='".$total_registration."' where id='".$scheme_id."'";
								$scheme_update = mysql_query($scheme_sql);

								$activity = "Added by ".($this->userName($_SESSION['adminid']))." to MOU Scheme Amount:- (".$mou_remaining_amount." Form No ".$form_no.")"; 
								$this->log_report($activity);
							}


							$activity = "Added by ".($this->userName($_SESSION['adminid']))." to New Connection Form : (".$form_no.")"; 
							$this->log_report($activity);

							if (!is_dir('media/admin_new_connection/'.$id)) {
				// dir doesn't exist, make it
								mkdir('media/admin_new_connection/'.$id,0777,true);
							}

							$address_proof_attechment = $_FILES['address_proof_attechment']['tmp_name'];
							if($address_proof_attechment != "" ){
								$short_address_proof_attechment = $_FILES['address_proof_attechment']['name'];
								$folder = "media/admin_new_connection/".$id.'/';
								move_uploaded_file($address_proof_attechment , $folder.$id.$short_address_proof_attechment);

								$update_thumb="update admin_new_connection set address_proof_attechment='".($short_address_proof_attechment?$id.'/'.$id.$short_address_proof_attechment:'')."' where id='".$id."'";
								$exe_thumb = mysql_query($update_thumb);

							}

							$id_proof_attechment = $_FILES['id_proof_attechment']['tmp_name'];
							if($id_proof_attechment != "" ){
								$short_id_proof_attechment = $_FILES['id_proof_attechment']['name'];
								$folder = "media/admin_new_connection/".$id.'/';
								move_uploaded_file($id_proof_attechment , $folder.$id.$short_id_proof_attechment);

								$update_thumb="update admin_new_connection set id_proof_attechment='".($short_id_proof_attechment?$id.'/'.$id.$short_id_proof_attechment:'')."' where id='".$id."'";
								$exe_thumb = mysql_query($update_thumb);

							}



							$noc_owner_proof = $_FILES['noc_owner_proof']['tmp_name'];
							if($noc_owner_proof != "" ){
								$short_noc_owner_proof = $_FILES['noc_owner_proof']['name'];
								$folder = "media/admin_new_connection/".$id.'/';
								move_uploaded_file($noc_owner_proof , $folder.$id.$short_noc_owner_proof);

								$update_noc = "update admin_new_connection set noc_owner_proof='".($short_noc_owner_proof?$id.'/'.$id.$short_noc_owner_proof:'')."' where id='".$id."'";
								$exe_noc = mysql_query($update_noc);

							}

							$registration_form_attechment = $_FILES['registration_form_attechment']['tmp_name'];
							if($registration_form_attechment != "" ){
								$short_registration_form_attechment = $_FILES['registration_form_attechment']['name'];
								$folder = "media/admin_new_connection/".$id.'/';
								move_uploaded_file($registration_form_attechment , $folder.$id.$short_registration_form_attechment);

								$update_reg = "update admin_new_connection set registration_form_attechment='".($short_registration_form_attechment?$id.'/'.$id.$short_registration_form_attechment:'')."' where id='".$id."'";
								$exe_reg = mysql_query($update_reg);

							}



							$_SESSION['error'] = ADDNEWRECORD;	
							$_SESSION['errorclass'] = ERRORCLASS;
							header("location:index.php?control=user_applynewconnection&task=pending_connection_admin");

						} 
					} 

					else{	
						$chk = mysql_fetch_array(mysql_query("SELECT `id` FROM `admin_new_connection` WHERE `form_no` = '".$form_no."' and id!= '".$id."'"));
						if(!$chk){
							$result = mysql_fetch_array(mysql_query("select * from admin_new_connection where id = '".$id."'"));					

							$res = mysql_fetch_array(mysql_query("SELECT * FROM  scheme WHERE id = '".$scheme_id."'"));	
							$emi  = $res['emi'];
							$emi_amount  = $res['emi_amount'];
							$emi_installment  = $res['emi_installment'];	
							$zero_amount = $res['security_deposite']+$res['gas_supply']+$res['non_refund'];
							$total_amount  = ($emi=='Yes')?($emi_installment*$emi_amount):($res['registration_fee']=='0')?$zero_amount:$res['registration_fee'];


							/************************Start For scheme check on Update if not match & mou amount value updated ******************/
							if($result['scheme_id']!=$scheme_id){		   		

								if($res['mou']==1){	
									$data = mysql_fetch_array(mysql_query("SELECT * FROM  scheme WHERE id = '".$result['scheme_id']."'"));		

									$mou_sql = "update admin_new_connection set mou='".$res['mou']."', mou_value_deducted_against_registration='".$res['mou_value_deducted_against_registration']."', mou_customer_paid='".$res['mou_customer_paid']."', mou_total_amount='".$res['registration_fee']."'  where id='".$id."'";
									$mou_update = mysql_query($mou_sql);


									$mou_remaining_amount_old = $data['mou_remaining_amount']+$data['mou_value_deducted_against_registration'];
									$total_registration_old = $data['total_registration']-1;

									$scheme_id.''.	$scheme_sql = "update scheme set  mou_remaining_amount='".$mou_remaining_amount_old."', total_registration='".$total_registration_old ."' where id='".$result['scheme_id']."' and mou=1";

									$scheme_update = mysql_query($scheme_sql);

									$mou_remaining_amount = $res['mou_remaining_amount']-$res['mou_value_deducted_against_registration'];
									$total_registration = $res['total_registration']+1;
									$scheme_sql = "update scheme set  mou_remaining_amount='".$mou_remaining_amount."', total_registration='".$total_registration."' where id='".$scheme_id."'  and mou=1";

									$scheme_update = mysql_query($scheme_sql);

									$activity = "Modify by ".($this->userName($_SESSION['adminid']))." to EDIT Application form MOU Scheme Amount:- (".$mou_remaining_amount_old." Form No ".$form_no.")"; 
									$this->log_report($activity);
								}else{	

									$data = mysql_fetch_array(mysql_query("SELECT * FROM  scheme WHERE id = '".$result['scheme_id']."'"));	

									$mou_sql = "update admin_new_connection set mou='".$res['mou']."', mou_value_deducted_against_registration='".$res['mou_value_deducted_against_registration']."', mou_customer_paid='".$res['mou_customer_paid']."', mou_total_amount='' where id='".$id."'";
									$mou_update = mysql_query($mou_sql);			


									$mou_remaining_amount = $data['mou_remaining_amount']+$data['mou_value_deducted_against_registration'];
									$total_registration = $data['total_registration']-1;

									$scheme_sql = "update scheme set  mou_remaining_amount='".$mou_remaining_amount."', total_registration='".$total_registration."' where id='".$result['scheme_id']."'  and mou=1";
									$scheme_update = mysql_query($scheme_sql);

									$activity = "Modify by ".($this->userName($_SESSION['adminid']))." to Add MOU Scheme Amount Restore:- (".$mou_remaining_amount." Form No ".$form_no.")"; 
									$this->log_report($activity);	

								}			
							}
							/************************End For scheme check on Update if not match & mou is******************/


							$update="UPDATE `admin_new_connection` SET `form_no`='".$form_no."',`form_date`='".$form_date."', `connection_type`='".$connection_type."',`state_id`='".$state_id."',`city_id`='".$city_id."',`asset_id`='".$asset_id."',`area_id`='".$area_id."',`society_id`='".$society_id."',`scheme_id`='".$scheme_id."',`lpg_connection`='".$lpg_connection."',`lpg_comp`='".$lpg_comp."',`lpg_comp_other`='".$lpg_comp_other."',`lpg_consumer_no`='".$lpg_consumer_no."',`lpg_delar_name`='".$lpg_delar_name."',`cust_name`='".$cust_name."',`father_name`='".$father_name."',`house_type`='".$house_type."',`flat_no`='".$flat_no."',`address`='".$address."',`land_mark`='".$land_mark."',`pincode`='".$pincode."',`telephone_no1`='".$telephone_no1."',`telephone_no2`='".$telephone_no2."',`telephone_no3`='".$telephone_no3."',`mobile_no`='".$mobile_no."',`email_id`='".$email_id."',`owner_type`='".$owner_type."',`id_proof`='".$id_proof."',`address_proof`='".$address_proof."',`ggl_email`='".$ggl_email."',`term_condition`='".$term_condition."',`amount`='".$amount."',`modified_by`='".$employee_id."',`modifed_date_time`='".date('Y-m-d H:i:s')."', `mou`='".$res['mou']."', emi='".$emi."', emi_amount='".$emi_amount."', emi_installment='".$emi_installment."', total_amount='".$total_amount."' WHERE id='".$_REQUEST['id']."'";

							$exe_query = mysql_query($update);





							/************************Start For Update Value check New CEN No. if details if not match******************/


							if($result['city_id']!=$city_id || $result['area_id']!=$area_id || $result['society_id']!=$society_id){
								$fetch_crn = mysql_fetch_array(mysql_query("select id, crn_main_value,crn from admin_new_connection where city_id = '".$city_id."' and area_id = '".$area_id."' and society_id = '".$society_id."' ORDER BY crn_main_value DESC LIMIT 0,1"));


								$fetch_connection = mysql_fetch_array(mysql_query("select id,city_id,area_id,society_id,crn from admin_new_connection where id = '".$_REQUEST['id']."'"));

								$sel_city = mysql_fetch_array(mysql_query("select * from city where id ='".$fetch_connection['city_id']."'"));
								$sel_area = mysql_fetch_array(mysql_query("select * from area where id ='".$fetch_connection['area_id']."'"));
								$sel_society = mysql_fetch_array(mysql_query("select * from society where id ='".$fetch_connection['society_id']."'"));

								if($fetch_crn['crn']!=$fetch_connection['crn']){

									$rand_no    = $fetch_crn['crn_main_value'] +1;
								} else{
									$rand_no    = $fetch_crn['crn_main_value'];
								}

								$count_id_length = strlen($rand_no);

								if($count_id_length==1){ $rand_new    = '000'.$rand_no;	} 
								if($count_id_length==2){ $rand_new    = '00'.$rand_no;  } 
								if($count_id_length==3){ $rand_new    = '0'.$rand_no;   }  


								$new_crn = $sel_city['city_code'].$_SESSION['customer_type'].$sel_area['area_code'].$sel_society['society_code'].$rand_new;

								$crn = $sel_city['city_code'].$_SESSION['customer_type'].$sel_area['area_code'].$sel_society['society_code'].$rand_new.'0';



								$update_crn = "update admin_new_connection set application_no='".$crn."',`city_code`='".$sel_city['city_code']."',`customer_type`='".$_SESSION['customer_type']."',`area_code`='".$sel_area['area_code']."',`society_code`='".$sel_society['society_code']."',crn_value = '".$rand_new."',crn_main_value = '".$rand_no."', crn = '".$new_crn."' where id='".$_REQUEST['id']."'";
								$exe_crn_query = mysql_query($update_crn);
							}
							/************************End For Update Value check New CEN No. if details if not match******************/



							$activity = "Modify by ".($this->userName($_SESSION['adminid']))." to New Connection Form : (".$form_no.")"; 
							$this->log_report($activity);

							if (!is_dir('media/admin_new_connection'.$id)) {
					// dir doesn't exist, make it
								mkdir('media/admin_new_connection/'.$id, 0777, true);
							}
							$folder = "media/admin_new_connection/".$id.'/';
							$unlink = "admin_new_connection/";

							if($_FILES['address_proof_attechment']['name']){	
								$address_proof_attechment = $result['address_proof_attechment'];
								unlink($unlink.$address_proof_attechment);
							}
							$address_proof_attechment = $_FILES['address_proof_attechment'];
							move_uploaded_file($_FILES['address_proof_attechment']['tmp_name'], $folder.$id.$address_proof_attechment['name']);

							if($_FILES['id_proof_attechment']['name']){	
								$id_proof_attechment = $result['id_proof_attechment'];
								unlink($unlink.$id_proof_attechment);
							}
							$id_proof_attechment = $_FILES['id_proof_attechment'];
							move_uploaded_file($_FILES['id_proof_attechment']['tmp_name'], $folder.$id.$id_proof_attechment['name']);	

							if($_FILES['noc_owner_proof']['name']){	
								$noc_owner_proof = $result['noc_owner_proof'];
								unlink($unlink.$noc_owner_proof);
							}
							$noc_owner_proof = $_FILES['noc_owner_proof'];
							move_uploaded_file($_FILES['noc_owner_proof']['tmp_name'], $folder.$id.$noc_owner_proof['name']);

							if($_FILES['registration_form_attechment']['name']){	
								$registration_form_attechment = $result['registration_form_attechment'];
								unlink($unlink.$registration_form_attechment);
							}
							$registration_form_attechment = $_FILES['registration_form_attechment'];
							move_uploaded_file($_FILES['registration_form_attechment']['tmp_name'], $folder.$id.$registration_form_attechment['name']);

							$update_thumb = "update admin_new_connection set address_proof_attechment= '".($address_proof_attechment['name'] ? $id.'/'.$id.$address_proof_attechment['name']:$result['address_proof_attechment'])."',id_proof_attechment= '".($id_proof_attechment['name'] ? $id.'/'.$id.$id_proof_attechment['name']:$result['id_proof_attechment'])."',noc_owner_proof= '".($noc_owner_proof['name'] ? $id.'/'.$id.$noc_owner_proof['name']:$result['noc_owner_proof'])."',registration_form_attechment= '".($registration_form_attechment['name'] ? $id.'/'.$id.$registration_form_attechment['name']:$result['registration_form_attechment'])."'where id='".$_REQUEST['id']."'";
							$exe_thumb    = mysql_query($update_thumb);

							if($_SESSION['department_id']=='3') { 
								$_SESSION['error'] = UPDATERECORD;	
								$_SESSION['errorclass'] = ERRORCLASS;
								header("location:index.php?control=user_applynewconnection&task=show_detail_admin&id=".$id);

							} else{

								$_SESSION['error'] = UPDATERECORD;	
								$_SESSION['errorclass'] = ERRORCLASS;
								header("location:index.php?control=user_applynewconnection&task=pending_connection_admin");
							}}


							else{
								$_SESSION['error'] = DUPLICATE;	
								$_SESSION['errorclass'] = ERRORCLASS;
								header("location:index.php?control=user_applynewconnection&task=pending_connection_admin");
							}
						}
					}

				}		
				else{
					header('location:index.php?msg=1');	
				}


			}

			function addnew() {
				if($_SESSION['department_id']=='11' || $_SESSION['department_id']=='2' || $_SESSION['department_id']=='3')
				{	
					if($_REQUEST['id']) {
						$query_com ="SELECT * FROM  admin_new_connection WHERE id =".$_REQUEST['id'];
						$this->Query($query_com);

						$results = $this->fetchArray();
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
					else {

						require_once("views/".$this->name."/".$this->task.".php"); 
					}
				}
				else
				{
					header('location:index.php?msg=1');	
				}	
			}

			function proj_addnew() {
				if($_SESSION['department_id']=='3' || $_SESSION['department_id']=='2' || $_SESSION['department_id']=='1')
				{	
					if($_REQUEST['id']) {
						$query_com ="SELECT * FROM  admin_new_connection WHERE id =".$_REQUEST['id'];
						$this->Query($query_com);

						$results = $this->fetchArray();
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
					else {

						require_once("views/".$this->name."/".$this->task.".php"); 
					}
				}
				else
				{
					header('location:index.php?msg=1');	
				}	
			}

			function save_proj_edit(){

				if($_REQUEST['proj_ng_dt']!='') {  $ng_progress_fy1920=date('M-y',strtotime($_REQUEST['proj_ng_dt']));	} 
				if($_REQUEST['mktg_rfa_dt']!='') { $ng_progress_fy1920=date('M-y',strtotime($_REQUEST['mktg_rfa_dt']));  }
				if($_REQUEST['mktg_rfa_dt']!='' && $_REQUEST['proj_ng_dt']!='') { $ng_progress_fy1920=date('M-y',strtotime($_REQUEST['proj_ng_dt'])); }

				if($_SESSION['department_id']==1){
					$qry_string  = "overall_comments='".$_REQUEST['overall_comments']."'";
				}
				if($_SESSION['department_id']==2){
					$qry_string  = "ninety_days_pendency_sch='".$_REQUEST['ninety_days_pendency_sch']."',ng_progress_fy1920='".$ng_progress_fy1920."',proj_remarks='".$_REQUEST['proj_remarks']."',jmr_no='".$_REQUEST['jmr_no']."',proj_ng_dt='".$_REQUEST['proj_ng_dt']."',initial_reading='".$_REQUEST['initial_reading']."',meter_no='".$_REQUEST['meter_no']."',proj_gi_contractor_alloted='".$_REQUEST['proj_gi_contractor_alloted']."'";
				}
				if($_SESSION['department_id']==3){
					$qry_string  = "mktg_rfa_dt='".$_REQUEST['mktg_rfa_dt']."',mktg_remarks='".$_REQUEST['mktg_remarks']."'";
				}

				$query="update admin_new_connection set $qry_string  WHERE id='".$_REQUEST['id']."'";	



		 	 	// $query="update admin_new_connection set overall_comments='".$_REQUEST['overall_comments']."',ninety_days_pendency_sch='".$_REQUEST['ninety_days_pendency_sch']."',ng_progress_fy1920='".$ng_progress_fy1920."',mktg_rfa_dt='".$_REQUEST['mktg_rfa_dt']."',mktg_remarks='".$_REQUEST['mktg_remarks']."',proj_remarks='".$_REQUEST['proj_remarks']."',jmr_no='".$_REQUEST['jmr_no']."',proj_ng_dt='".$_REQUEST['proj_ng_dt']."',initial_reading='".$_REQUEST['initial_reading']."',meter_no='".$_REQUEST['meter_no']."',proj_gi_contractor_alloted='".$_REQUEST['proj_gi_contractor_alloted']."' WHERE id='".$_REQUEST['id']."'";	


				$this->Query($query);	
				$this->Execute();
				if($_REQUEST['page']=='show_png_admin') { 	
					header("location:index.php?control=user_applynewconnection&task=show_png_admin");
				} else
				{
					header("location:index.php?control=user_applynewconnection&task=show_admin");
				}}

				function save_proj_contractor() {

					$id = $_POST['id'];
					$proj_gi_contractor_alloted = $_POST['proj_gi_contractor_alloted'];
					$query="update admin_new_connection set proj_gi_contractor_alloted='".$proj_gi_contractor_alloted."'  WHERE id='".$id."'";	
					$this->Query($query);	
					$this->Execute();
			//$this->task="show_admin";
			//$this->view ='show_admin';
		//$this->show();	
					header("location:index.php?control=user_applynewconnection&task=show_admin");
				}



				function update_status() {
					if($_REQUEST['id']) {
						$query_com ="SELECT * FROM  new_connection WHERE id =".$_REQUEST['id'];
						$this->Query($query_com);

						$results = $this->fetchArray();
						require_once("views/".$this->name."/".$this->task.".php"); 
					}
					else {

						require_once("views/".$this->name."/".$this->task.".php"); 
					}
				}

				function save_status(){	
					$id = $_REQUEST['id'];	
					$com_no = mysql_fetch_array(mysql_query("select * from new_connection where id='".$id."'"));

					$new_connection_id   = $com_no['id'];			
					$application_no      = $com_no['application_no'];			 			
					$usr_message         = $_REQUEST['usr_message'];			
					$admin_message       = $_REQUEST['admin_message'];			
					$date = date('d-m-Y h:i:s');

					if($com_no){				
						$query="INSERT INTO `new_connection_status`(`new_connection_id`, `application_no`, `usr_message`, `admin_message`, `type`, `date`) VALUES ('".$new_connection_id."','".$application_no."','".$usr_message."','".$admin_message."', '2','".$date."')";
						$this->Query($query);	
						$this->Execute();				
						$_SESSION['error'] = ADDNEWRECORD;	
						$_SESSION['errorclass'] = ERRORCLASS;
						header("location:index.php?control=user_applynewconnection&task=update_status&id=".$id);
					}
					else{
						$_SESSION['error'] = ID;	
						$_SESSION['errorclass'] = ERRORCLASS;
						header("location:index.php?control=user_applynewconnection&task=update_status&id=".$id);		
					}
				}

				function status(){
					$query="update new_connection set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
					$this->Query($query);	
					$this->Execute();
					$this->task="show";
					$this->view ='show';
		//$this->show();	
					header("location:index.php?control=user_applynewconnection");
				}

				function verify_status(){	
					if($_SESSION['department_id']=='3' || $_SESSION['department_id']==''){

						$status =$_REQUEST['status'];	
						$remark =$_REQUEST['remark'];

						$query=mysql_query("update admin_new_connection set verify_status='".$_REQUEST['status']."',`modified_by`='".$_SESSION['adminid']."',`modifed_date_time`='".date('Y-m-d H:i:s')."'  WHERE id='".$_REQUEST['id']."'");	

						$status =$_REQUEST['status'];

						if($status=='1' || $status=='3') {
				//if($status=='1') {
							$fetch_record = mysql_fetch_array(mysql_query("select mobile_no,email_id,application_no,id,crn,remark from admin_new_connection where id='".$_REQUEST['id']."'"));

							$mobile = $fetch_record['mobile_no'];
							$email_id = $fetch_record['email_id'];

							$string = $fetch_record['application_no'];
							if($remark!='') {
								$remark = $remark;
							} else { 
								$remark = $fetch_record['remark'];
							}

							$new_str = substr($string, 0, -1);								


					//$fecth_connection_status = mysql_fetch_array(mysql_query("select * from customer_connection_status where connection_status = 'Temporary Registration'"));

					$application_no = $new_str.$status;   //$fecth_connection_status['connection_status_code'];
					
					$crn_no = $fetch_record['crn'];
					
					$update_crn = "update admin_new_connection set application_no='".$application_no."',remark='".$remark."',verify_status='".$status."'   where id='".$_REQUEST['id']."'";
					$exe_crn_query = mysql_query($update_crn);

					$activity = "Update Application(CRN) No. for = ".$_REQUEST['id']; 		 
					$this->log_report($activity);	

					/*-------------------------------------------------------------SMS and Email------------------------------------------------------*/	

					if($status=='1') { 
			//$link = '<a href="https://gglonline.net/ggluser/newconnection_quickpay.php?crn='.$crn.'">Click Here For Payment</a>';
						$link = 'https://gglonline.net/ggluser/newconnection_quickpay.php?crn='.$crn_no;

						$sms=urlencode("Dear Customer.\nThank you for applying for PNG connection.Kindly click below link and make the payment to process your application.\n$link\nTeam Green Gas Ltd.");

						file(("http://vas.sgtpl.in/sendsms/sendsms.php?username=STgreen&password=Lucknow1&type=TEXT&mobile=$mobile&sender=GGLGAS&&message=$sms")) or die("msg send failed");

						$mailLink = 'https://gglonline.net/ggluser/newconnection_quickpay.php?crn='.$crn_no;

						$d= 'Connection Payment Mail From GGL';
						$to = $email_id;
						$subject = 'Connection Payment';
						$message = "Dear Customer,";
						$message .= "\n\nThank you for applying for PNG connection.Kindly click below link and make the payment to process your application.\n\n".$mailLink."";

						$message .= "\n\n\nThis is an auto generated mail.Please DO NOT Reply.";
						$message .= "\n\nThanks & Regards";
						$message .= "\nGreen Gas Ltd.";
						$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
						$headers .= 'From: '.$d.'<customercare.lko@gglonline.net>'.  "\r\n";
			//$headers .= 'Reply-To: '.$to.'' . "\r\n";
						mail($to, $subject, $message, $headers);
						/*-------------------------------------------------------------SMS and Email------------------------------------------------------*/			
					}
				}

				if($status=='1'){		$activity = "Verify CRN NO = ".$application_no; } 		
				if($status=='3') {  	$activity = "Not-Verify CRN NO = ".$application_no;} 		 
				$this->log_report($activity);	
				header("location:index.php?control=user_applynewconnection&task=pending_connection_admin");
			}
			else
			{
				header('location:index.php?msg=1');	
			}	
		}
		
		function payment_verify(){	

			$id = $_REQUEST['id'];
			
			if($_SESSION['department_id']=='3'){	

				$query=mysql_query("update admin_new_connection set verify_status=".$_REQUEST['status'].",`modified_by`='".$_SESSION['adminid']."',`modifed_date_time`='".date('Y-m-d H:i:s')."'  WHERE id='".$id."'");	

				$status = $_REQUEST['status'];

				if($status=='2') {   


					$fetch_record = mysql_fetch_array(mysql_query("select mobile_no,email_id,application_no,id,crn,amount, city_id from admin_new_connection where id='".$id."'"));
					$sap_crn = mysql_fetch_array(mysql_query("select * from admin_new_connection where city_id='".$fetch_record['city_id']."' and verify_status='2' order by sap_crn DESC limit 0,1"));
					if($sap_crn['city_id']==1){   $cityName = 'LKO';   }
					if($sap_crn['city_id']==2){   $cityName = 'AGA';   }
					$max_sapNo =  (substr($sap_crn['sap_crn'],3)+1);
					$count_id_length = strlen($max_sapNo);

					if($count_id_length==5){ $rand_new    = '000';	} 
					if($count_id_length==6){ $rand_new    = '00';   } 
					if($count_id_length==7){ $rand_new    = '0';    }

					$create_sap_no = $cityName.$rand_new.$max_sapNo;



					if($fetch_record['amount']=='0'){
						$paymentUpdate = mysql_query("update admin_new_connection set sap_crn='".$create_sap_no."', payment_status='1', `transationid`='0-balance-success', `payment_amount`='0', `txnDateTime`='".date('Y-m-d H:i:s')."', payment_by='0'  WHERE id='".$id."'");	
					}


					$mobile = $fetch_record['mobile_no'];
					$email_id = $fetch_record['email_id'];
					$string = $fetch_record['application_no'];
					
					$new_str = substr($string, 0, -1);								

					
					//$fecth_connection_status = mysql_fetch_array(mysql_query("select * from customer_connection_status where connection_status = 'Temporary Registration'"));

					$application_no = $new_str.$status;  //$fecth_connection_status['connection_status_code'];
					
					$crn_no = $fetch_record['crn'];
					
					$update_crn = "update admin_new_connection set application_no='".$application_no."',  sap_crn='".$create_sap_no."', verify_status='".$status."'  where id='".$id."'"; 
					$exe_crn_query = mysql_query($update_crn);

					$activity = "Update Application(CRN) No. for = ".$_REQUEST['id']; 		 
					$this->log_report($activity);	

					/*-------------------------------------------------------------SMS and Email------------------------------------------------------*/	

					$sms=urlencode("Dear Customer.\n Thank you for Your Payment Verification Completed. Your CRN No. - ".$create_sap_no.".\nTeam Green Gas Ltd.");

					file(("http://vas.sgtpl.in/sendsms/sendsms.php?username=STgreen&password=Lucknow1&type=TEXT&mobile=$mobile&sender=GGLGAS&&message=$sms")) or die("msg send failed");

			//$mailLink = 'https://gglonline.net/ggluser/newconnection_quickpay.php?crn='.$crn_no;

					$d= 'New Connection Payment Mail From GGL';
					$to = $email_id;
					$subject = 'New Connection Payment';
					$message = "Dear Customer,";
					$message .= "\n\nThank you for Your Payment Verification Completed. Your CRN No. - ".$create_sap_no."";

					$message .= "\n\n\nThis is an auto generated mail.Please DO NOT Reply.";
					$message .= "\n\nThanks & Regards";
					$message .= "\nGreen Gas Ltd.";
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.$d.'<customercare.lko@gglonline.net>'.  "\r\n";
			//$headers .= 'Reply-To: '.$to.'' . "\r\n";
					mail($to, $subject, $message, $headers);
					/*-------------------------------------------------------------SMS and Email------------------------------------------------------*/			

					$activity = "Payment-Verify CRN No. - ".$create_sap_no;} 		 
					$this->log_report($activity);	
					header("location:index.php?control=user_applynewconnection&task=show_admin");
				}
				else
				{
					header('location:index.php?msg=1');	
				}	
			}

			function send_sms()
			{
				$cust_detail = mysql_fetch_array(mysql_query("select * from admin_new_connection where id = '".$_REQUEST['id']."' and verify_status ='1'"));
				$crn_no = $cust_detail['crn'];
				$mobile = $cust_detail['mobile_no'];
				$email_id = $cust_detail['email_id'];

				
				$link = 'https://gglonline.net/ggluser/newconnection_quickpay.php?crn='.$crn_no;

				$sms=urlencode("Dear Customer.\nThank you for applying for PNG connection.Kindly click below link and make the payment to process your application.\n$link\nTeam Green Gas Ltd.");

				file(("http://vas.sgtpl.in/sendsms/sendsms.php?username=STgreen&password=Lucknow1&type=TEXT&mobile=$mobile&sender=GGLGAS&&message=$sms")) or die("msg send failed");

				$mailLink = 'https://gglonline.net/ggluser/newconnection_quickpay.php?crn='.$crn_no;

				$d= 'Connection Payment Mail From GGL';
				$to = $email_id;
				$subject = 'Connection Payment';
				$message = "Dear Customer,";
				$message .= "\n\nThank you for applying for PNG connection.Kindly click below link and make the payment to process your application.\n\n".$mailLink."";

				$message .= "\n\n\nThis is an auto generated mail.Please DO NOT Reply.";
				$message .= "\n\nThanks & Regards";
				$message .= "\nGreen Gas Ltd.";
				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: '.$d.'<customercare.lko@gglonline.net>'.  "\r\n";
			//$headers .= 'Reply-To: '.$to.'' . "\r\n";
				mail($to, $subject, $message, $headers);

				$activity = "Send SMS To CRN NO = ".$crn_no; 		 
				$this->log_report($activity);	
				header("location:index.php?control=user_applynewconnection&task=pending_connection_admin");
			}

		}
