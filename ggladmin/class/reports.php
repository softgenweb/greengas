<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class reportClass extends DbAccess {
		public $view='';
		public $name='report';
	/*------------------------------------------PNG DOM Report---------------------------------------*/
		
		function show(){	
		
		/*$uquery ="SELECT * FROM `master_png_dom` WHERE proj_ng_dt !='' ";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row  = count($uresults);
		
		$page   = intval($_REQUEST['page']);
		$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
		$adjacents  = intval($_REQUEST['adjacents']);
		$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
		$tdata = floor($tdata);
		if($page<=0)  $page  = 1;
		if($adjacents<=0) $tdata?($adjacents = 4):0;
		$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
			
		$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();	*/	
		
		require_once("views/".$this->name."/show.php"); 
		}
		
		function show_ninety(){	
		
		$uquery ="select * from master_png_dom where 1 and ninety_days_pendency_sch !=''";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row  = count($uresults);
		/* Paging start here */
		$page   = intval($_REQUEST['page']);
		$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
		$adjacents  = intval($_REQUEST['adjacents']);
		$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
		$tdata = floor($tdata);
		if($page<=0)  $page  = 1;
		if($adjacents<=0) $tdata?($adjacents = 4):0;
		$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_ninety.php"); 
		}
		
		function show_rfc(){	
		
		/*$uquery ="select * from master_png_dom where 1 and proj_rfc_dt !=''";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row  = count($uresults);
		
		$page   = intval($_REQUEST['page']);
		$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
		$adjacents  = intval($_REQUEST['adjacents']);
		$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
		$tdata = floor($tdata);
		if($page<=0)  $page  = 1;
		if($adjacents<=0) $tdata?($adjacents = 4):0;
		$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		
		$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();*/		
		
		require_once("views/".$this->name."/show_rfc.php"); 
		}
		
			
		function view_profile() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  master_png_dom WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
	/*------------------------------------------PNG DOM Report---------------------------------------*/
	
	/*------------------------------------------Admin New Connection Report---------------------------------------*/
		
		function show_admin(){	
		
		$uquery ="select * from admin_new_connection where 1 and proj_ng_dt !=''";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row  = count($uresults);
		/* Paging start here */
		$page   = intval($_REQUEST['page']);
		$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
		$adjacents  = intval($_REQUEST['adjacents']);
		$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
		$tdata = floor($tdata);
		if($page<=0)  $page  = 1;
		if($adjacents<=0) $tdata?($adjacents = 4):0;
		$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_admin.php"); 
		}
		
		function show_ninety_admin(){	
		
		$uquery ="select * from admin_new_connection where 1 and ninety_days_pendency_sch !=''";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row  = count($uresults);
		/* Paging start here */
		$page   = intval($_REQUEST['page']);
		$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
		$adjacents  = intval($_REQUEST['adjacents']);
		$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
		$tdata = floor($tdata);
		if($page<=0)  $page  = 1;
		if($adjacents<=0) $tdata?($adjacents = 4):0;
		$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_ninety_admin.php"); 
		}
		
		function show_rfc_admin(){	
		
		$uquery ="select * from admin_new_connection where 1 and mktg_rfa_dt !=''";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		$no_of_row  = count($uresults);
		/* Paging start here */
		$page   = intval($_REQUEST['page']);
		$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
		$adjacents  = intval($_REQUEST['adjacents']);
		$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
		$tdata = floor($tdata);
		if($page<=0)  $page  = 1;
		if($adjacents<=0) $tdata?($adjacents = 4):0;
		$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	
		$query = $uquery. " order by id DESC LIMIT ".(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_rfc_admin.php"); 
		}
		
			
		function view_profile_admin() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  admin_new_connection WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}


		function show_weight_measure(){
				$query_com = "SELECT * FROM `weight_measurement` WHERE 1 ORDER BY STR_TO_DATE(`w_m_due_date`,'%d-%m-%Y') ASC";
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 			
		}

		function addnew_weight_measure(){
			$id = $_REQUEST['id'];
			if($id){
				$query_com = "SELECT * FROM `weight_measurement` WHERE `id`='".$id."'";
				$this->Query($query_com);

				$results = $this->fetchArray();
			}
			    require_once("views/".$this->name."/".$this->task.".php"); 			
		}

		function save_weight_measure(){
			$id = $_REQUEST['id'];

			$city_id = $_REQUEST['city_id'];			$station_id = $_REQUEST['station_id'];
			$equipment = $_REQUEST['equipment'];			$serial_no = $_REQUEST['serial_no'];
			$vendor = $_REQUEST['vendor'];			$side = $_REQUEST['side'];
			$w_m_done_date = $_REQUEST['w_m_done_date'];			$w_m_due_date = $_REQUEST['w_m_due_date'];
			$verification_fee = $_REQUEST['verification_fee'];			
			$stamped_verified_certificate = $_REQUEST['stamped_verified_certificate'];

			if(!$id){
				$insert = "INSERT INTO `weight_measurement`(`city_id`, `station_id`, `equipment`, `serial_no`, `vendor`, `side`, `w_m_done_date`, `w_m_due_date`, `date`) VALUES ('".$city_id."', '".$station_id."', '".$equipment."', '".$serial_no."', '".$vendor."', '".$side."', '".$w_m_done_date."', '".$w_m_due_date."', '".date('Y-m-d H:i:s')."')";
				$query_insert = mysql_query($insert);
				$insert_id = mysql_insert_id();				
				
						if (!is_dir('media/wm/'.$insert_id)) {
						// dir doesn't exist, make it
						mkdir('media/wm/'.$insert_id,0777,true);
						}
						
						$verification_fee = $_FILES['verification_fee']['tmp_name'];
						if($verification_fee != "" ){
						$short_verification_fee = $_FILES['verification_fee']['name'];
						$folder = "media/wm/".$insert_id.'/';
						move_uploaded_file($verification_fee , $folder.$insert_id.$short_verification_fee);
						
						$update_thumb="update weight_measurement set verification_fee='".($short_verification_fee?$insert_id.'/'.$insert_id.$short_verification_fee:'')."' where id='".$insert_id."'";
						$exe_thumb = mysql_query($update_thumb);
						
						}
						
						$stamped_verified_certificate = $_FILES['stamped_verified_certificate']['tmp_name'];
						if($stamped_verified_certificate != "" ){
						$short_stamped_verified_certificate = $_FILES['stamped_verified_certificate']['name'];
						$folder = "media/wm/".$insert_id.'/';
						move_uploaded_file($stamped_verified_certificate , $folder.$insert_id.$short_stamped_verified_certificate);
						
						$update_thumb="update weight_measurement set stamped_verified_certificate='".($short_stamped_verified_certificate?$insert_id.'/'.$insert_id.$short_stamped_verified_certificate:'')."' where id='".$insert_id."'";
						$exe_thumb = mysql_query($update_thumb);
						
						}

				
				$_SESSION['error'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				$activity = "W&M Entry Added BY ".($this->userName($_SESSION['adminid']))." ID or S.No. : (".$insert_id.")"; 
							$this->log_report($activity);
				header("location:index.php?control=report&task=show_weight_measure");

			}else{
						
						$result = mysql_fetch_array(mysql_query("select * from weight_measurement where id = '".$id."'"));
						
				$update = "UPDATE `weight_measurement` SET `city_id`='".$city_id."', `station_id`='".$station_id."', `equipment`='".$equipment."', `serial_no`='".$serial_no."', `vendor`='".$vendor."', `side`='".$side."', `w_m_done_date`='".$w_m_done_date."', `w_m_due_date`='".$w_m_due_date."' WHERE `id`='".$id."'";
				$query_update = mysql_query($update);
				
							if (!is_dir('media/wm/'.$id)) {
								// dir doesn't exist, make it
								mkdir('media/wm/'.$id, 0777, true);
							}
							$folder = "media/wm/".$id.'/';
							$unlink = "wm/";

							if($_FILES['verification_fee']['name']){	
								$verification_fee = $result['verification_fee'];
								unlink($unlink.$verification_fee);
							}
							$verification_fee = $_FILES['verification_fee'];
							move_uploaded_file($_FILES['verification_fee']['tmp_name'], $folder.$id.$verification_fee['name']);

							if($_FILES['stamped_verified_certificate']['name']){	
								$stamped_verified_certificate = $result['stamped_verified_certificate'];
								unlink($unlink.$stamped_verified_certificate);
							}
							$stamped_verified_certificate = $_FILES['stamped_verified_certificate'];
							move_uploaded_file($_FILES['stamped_verified_certificate']['tmp_name'], $folder.$id.$stamped_verified_certificate['name']);								

							$update_thumb = "update weight_measurement set verification_fee= '".($verification_fee['name'] ? $id.'/'.$id.$verification_fee['name']:$result['verification_fee'])."',stamped_verified_certificate= '".($stamped_verified_certificate['name'] ? $id.'/'.$id.$stamped_verified_certificate['name']:$result['stamped_verified_certificate'])."', modified_by = '".$_SESSION['adminid']."',modifed_date_time = '".date('d-m-Y h:i:s')."' where id='".$_REQUEST['id']."'";
							$exe_thumb    = mysql_query($update_thumb);
							
							$_SESSION['error'] = UPDATERECORD; 
							$_SESSION['errorclass'] = SUCCESSCLASS;
							$activity = "W&M Entry Edited BY ".($this->userName($_SESSION['adminid']))." ID or S.No. : (".$id.")"; 
							$this->log_report($activity);
				header("location:index.php?control=report&task=show_weight_measure");		
			}
		}

		function weight_measure_status(){
			$query="UPDATE weight_measurement SET status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();
			$_SESSION['error'] = STATUS; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
					
			header("location:index.php?control=report&task=show_weight_measure");			
		}
		
	/*------------------------------------------PNG DOM Report---------------------------------------*/
	}