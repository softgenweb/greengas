<?php
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');

	}
	
	class regional_hierarchyClass extends DbAccess {
		public $view='';
		public $name='regional_hierarchy';
		
		
		/***************************************************** ZONE START **********************************************************/
		
		function show_country(){
			if($_REQUEST['search'])
			
			 {
		    $country_name=$_REQUEST['search'];
			
				$uquery ="select * from country WHERE country_name like '%".$country_name."%'";
			}else {		
		  $uquery ="select * from country where 1"; 
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_country.php"); 
		}

		function addnew_country() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  country WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_country(){
			$country_name=$_POST['country_name'];
			if(!$_REQUEST['id']){
		
		$query="insert into country(country_name,status,datetime) value('".$country_name."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
		header("location:index.php?control=regional_hierarchy&task=show_country");
		}
		else
		{
			$update="update country set country_name='".$country_name."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
			if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_country();
			//header("location:index.php?control=regional_hierarchy&task=show_country");
		}

		
		}
		
		function country_status(){
		$query = "update country set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
           $this->show_country();
		}
		
		function country_delete(){
		
		$query="DELETE FROM country WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
} 
 $this->show_country();
		}
		
		/****************************************** ZONE END **********************************************************/
		
		
		/***************************************** STATE START **********************************************************/
		
		function show_state(){
			if($_REQUEST['search'])
			
			 {
		    $state_name=$_REQUEST['search'];
			
				$uquery ="select * from state WHERE state_name like '%".$state_name."%'";
			}else	{
		 $uquery ="select * from state where 1";
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_state.php"); 
		}
		
		function addnew_state() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  state WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_state(){
			$name=$_POST['state_name'];
			$country_id=$_POST['country_id'];
			if(!$_REQUEST['id']){
		
		$query="insert into state(state_name,country_id,status,datetime) value('".$name."','".$country_id."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
		header("location:index.php?control=regional_hierarchy&task=show_state");
		}
		else
		{
			$update="update state set state_name='".$name."',country_id='".$country_id."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
				if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_state();
		}
		
		}
		
		function state_delete(){
		
		$query="DELETE FROM state WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_state();
		}
		
		function state_status(){
		$query = "update state set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_state();
		}
		
		/******************************************* STATE END **********************************************************/
		
		
		
		/*************************************** CITY START **********************************************************/
		
		function show_city(){
			if($_REQUEST['search'])
			
			 {
		    $city_name=$_REQUEST['search'];
			
				$uquery ="select * from city WHERE city_name like '%".$city_name."%'";
			}else	{	
		 $uquery ="select * from city where 1";
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_city.php"); 
		}
		
		function addnew_city() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  city WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_city(){
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_name=$_POST['city_name'];
			$city_code=$_POST['city_code'];
			if(!$_REQUEST['id']){
		
		 $query="insert into city(city_name,city_code,country_id,state_id,status,created_by,datetime) value('".$city_name."','".$city_code."','".$country_id."','".$state_id."','1','".$_SESSION['adminid']."','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
					$activity = "Add City - ".$city_name; 		 
					$this->log_report($activity);
		header("location:index.php?control=regional_hierarchy&task=show_city");
		}
		else
		{
			$update="update city set city_name='".$city_name."',city_code='".$city_code."',country_id='".$country_id."',state_id='".$state_id."',`modified_by`='".$employee_id."',`modifed_date_time`='".date('Y-m-d H:i:s')."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
				if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $activity = "Update City - ".$city_name; 		 
		 $this->log_report($activity);
		 $this->show_city();
		
		}
		
		}
		
		function city_status(){
		$query = "update city set status='".$_REQUEST['status']."',`modified_by`='".$employee_id."',`modifed_date_time`='".date('Y-m-d H:i:s')."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_city();
		}
		
		function city_delete(){
		
		$query="DELETE FROM city WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_city();
		}
		
		/***************************************** CITY END **********************************************************/
		
		
		
		
		/*************************************** AREA START **********************************************************/
		
		function show_area(){	
		 $uquery ="select * from area where status=1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_area.php"); 
		}
		
		function addnew_area() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  area WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_area(){
			$area_name=$_POST['area_name'];     $amount = $_POST['amount'];
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_id=$_POST['city_id'];
			$asset_id=$_POST['asset_id'];
			
			$fecth_asset = mysql_fetch_array(mysql_query("select * from asset where id = '".$_POST['asset_id']."'"));
						
			if(!$_REQUEST['id']){
				
				$fetch_area = mysql_fetch_array(mysql_query("select area_val from area where country_id = '".$country_id."' and state_id = '".$state_id."' and city_id = '".$city_id."' ORDER BY `area`.`area_val` DESC LIMIT 0,1"));
				
				if($fetch_area['area_val']!='') {
					$area_val = $fetch_area['area_val'] +1;
				} else {
					$area_val = '1';
				}
			 $count_id_length = strlen($area_val);
			
			if($count_id_length==1){ 	 
			$area_new    = '00'.$area_val;
			} 
			if($count_id_length==2){ 	
			$area_new    = '0'.$area_val;
			 }  
			if($count_id_length==3){ 	
			$area_new    = $area_val;
			 } 
			
		
		   $query="insert into area(area_name,area_code,area_val,asset_id,asset_name,city_id,country_id,state_id,status,amount,created_by,datetime) value('".$area_name."','".$area_new."','".$area_val."','".$asset_id."','".$fecth_asset['asset_name']."','".$city_id."','".$country_id."','".$state_id."','1','".$amount."','".$_SESSION['adminid']."','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $activity = "Add Area - ".$area_name; 		 
		 $this->log_report($activity);
		header("location:index.php?control=regional_hierarchy&task=show_area");
		}
		else
		{
			
			
			$update="update area set area_name='".$area_name."', amount='".$amount."', asset_id='".$asset_id."',asset_name='".$fecth_asset['asset_name']."', city_id='".$city_id."',country_id='".$country_id."',state_id='".$state_id."',modified_by = '".$_SESSION['adminid']."',modifed_date_time ='".date("Y-m-d H:i:s")."'  where id='".$_REQUEST['id']."'";
			$this->Query($update);
			$this->Execute();
					if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
 $activity = "Update Area - ".$area_name; 		 
		 $this->log_report($activity);
$this->show_area();
		
		}
		
		}
		
		function area_status(){
		$query = "update area set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_area();
		}
		
		function area_delete(){
		
		$query="DELETE FROM area WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_area();
		}
		
		/****************************************** AREA END **********************************************************/
	
/***************************************** Asset START **********************************************************/
		
		function show_asset(){
			if($_REQUEST['search'])
			
			 {
		    $asset_name=$_REQUEST['search'];
			
				$uquery ="select * from asset WHERE asset_name like '%".$asset_name."%'";
			}else	{
		 $uquery ="select * from asset where 1";
			}
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_asset.php"); 
		}
		
		function addnew_asset() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  asset WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_asset(){
			$name=$_POST['asset_name'];
			$city_id=$_POST['city_id'];
			if(!$_REQUEST['id']){
		
		$query="insert into asset(asset_name,city_id,created_by,created_date_time) value('".$name."','".$city_id."','".$_SESSION['adminid']."','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        $activity = "Add Asset - ".$name; 		 
		 $this->log_report($activity);
		header("location:index.php?control=regional_hierarchy&task=show_asset");
		}
		else
		{
			$update="update asset set asset_name='".$name."',city_id='".$city_id."',modified_by= '".$_SESSION['adminid']."',modifed_date_time='".date("Y-m-d H:i:s")."' where id='".$_REQUEST['id']."'";
	$this->Query($update);
	if($this->Execute()) {	
	$_SESSION['error'] = UPDATERECORD;	
	$_SESSION['errorclass'] = ERRORCLASS;
}
 $activity = "Update Asset - ".$name; 		 
		 $this->log_report($activity);
         $this->show_asset();
		}
		
		}
		
		function asset_delete(){
		
		$query="DELETE FROM asset WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_asset();
		}
		
		function asset_status(){
		$query = "update asset set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
         $this->show_asset();
		}
		
		/******************************************* Asset END **********************************************************/
		
		
		
			/******************************************Start Society  **********************************************************/
		function show_society(){	
		 $uquery ="select * from society where status=1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		 require_once("views/".$this->name."/show_society.php"); 
		}
		
		function addnew_society() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  society WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_society(){
			$society_name=$_POST['society_name'];
			$area_id=$_POST['area_id'];
			$country_id=$_POST['country_id'];
			$state_id=$_POST['state_id'];
			$city_id=$_POST['city_id'];
			$owner_type=$_POST['owner_type'];
			$start_date =$_POST['start_date'];
			$end_date =$_POST['end_date'];
			
			$marketing_end_date =$_POST['marketing_end_date'];
			
			if(!$_REQUEST['id']){
		
		$fetch_society = mysql_fetch_array(mysql_query("select society_val from society where country_id = '".$country_id."' and state_id = '".$state_id."' and city_id = '".$city_id."' and area_id='".$area_id."'  ORDER BY `society`.`society_val` DESC LIMIT 0,1"));
				
				if($fetch_society['society_val']!='') {
					$society_val = $fetch_society['society_val'] +1;
				} else {
					$society_val = '1';
				}
			$count_id_length = strlen($society_val);
			
			if($count_id_length==1){ 	 
			$society_no    = '00'.$society_val;
			} 
			if($count_id_length==2){ 	
			$society_no    = '0'.$society_val;
			 } 
			if($count_id_length==3){ 	
			$society_no    = $society_val;
			 } 
		
		 $query="insert into society(society_name,society_code,society_val,area_id,city_id,country_id,state_id,owner_type,start_date,end_date,status,created_by,datetime) value('".$society_name."','".$society_no."','".$society_val."','".$area_id."','".$city_id."','".$country_id."','".$state_id."','".$owner_type."','".$start_date."','".$end_date."','1','".$_SESSION['adminid']."','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
		
		     $activity = "Added by ".($this->userName($_SESSION['adminid']))." to Society name:- (".$society_name.")"; 
     $this->log_report($activity);
		
		
		header("location:index.php?control=regional_hierarchy&task=show_society");
		}
		else
		{	
		if($_SESSION['department_id']!='3'){ 
		
				$update="update society set society_name='".$society_name."',area_id='".$area_id."',city_id='".$city_id."',country_id='".$country_id."',state_id='".$state_id."',owner_type='".$owner_type."', start_date='".$start_date."', end_date='".$end_date."',  modified_by='".$_SESSION['adminid']."',modify_date='".date("Y-m-d H:i:s")."' where id='".$_REQUEST['id']."'";
		
				$this->Query($update);
				$this->Execute();
				
				if($this->Execute()) {	
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				}
	$activity = "Modify by ".($this->userName($_SESSION['adminid']))." to Society name:- (".$society_name." - ID - ".$_REQUEST['id'].""; 
     $this->log_report($activity);
	 
				} else {
			$fecth_record = mysql_fetch_array(mysql_query("select previous_end_date,end_date from society where id='".$_REQUEST['id']."'"));	
				
			$previous_end_date = $fecth_record['previous_end_date'] ? $fecth_record['previous_end_date'] : $_POST['end_date'];
			
			$end_date = $_POST['marketing_end_date'];
			
			
			
		$update="update society set end_date='".$end_date."',previous_end_date = '".$previous_end_date."', modified_by='".$_SESSION['adminid']."',modify_date='".date("Y-m-d H:i:s")."' where id='".$_REQUEST['id']."'";	
		$this->Query($update);
				$this->Execute();
				
				if($this->Execute()) {	
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				}
	$activity = "Modify by ".($this->userName($_SESSION['adminid']))." to End Date:- (".$fecth_record['end_date']." To ".$end_date.")"; 
     $this->log_report($activity);
		}
				header("location:index.php?control=regional_hierarchy&task=show_society");
				
				}
		
		}
		
		function society_status(){
		$query = "update society set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	   
		
		
		
		if($this->Execute()) {	
		$_SESSION['error'] = STATUS;	
		$_SESSION['errorclass'] = ERRORCLASS;
		}
       
	    $activity = "Modify by ".($this->userName($_SESSION['adminid']))." to Society ID status :- (".$_REQUEST['status']. ',ID:- '.$_REQUEST['id'].")"; 
		$this->log_report($activity);
    $this->show_society();
		}
		
		function society_delete(){
		
		$query="DELETE FROM society WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
			if($this->Execute()) {	
			$_SESSION['error'] = DELETE;	
			$_SESSION['errorclass'] = ERRORCLASS;
			}
    $activity = "Delete by ".($this->userName($_SESSION['adminid']))." to Society ID :- (".$_REQUEST['id'].")"; 
    $this->log_report($activity);


$this->show_society();
		}
		
		
	
		/****************************************** Start Scheme END **********************************************************/	
		
		function show_scheme(){	
		 $uquery ="select * from scheme where status=1"; 
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		 require_once("views/".$this->name."/show_scheme.php"); 
		}
		
		function addnew_scheme() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  scheme WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_scheme(){
			$scheme_type = $_POST['scheme_type'];		
			$rental  = $_POST['rental'];
			 $remark  = $_POST['remark'];
			$security_deposite = $_POST['security_deposite'];
			$gas_supply = $_POST['gas_supply'];
			$non_refund = $_POST['non_refund'];
			$registration_fee = $_POST['registration_fee'];
			$mou = $_POST['mou'];
			$mou_value_deducted_against_registration = $_POST['mou_value_deducted_against_registration'];
			$mou_customer_paid = $_POST['mou_customer_paid'];
			$mou_total_amount = $_POST['mou_total_amount'];
			$mou_remaining_amount = $_POST['mou_remaining_amount'];
			
			$emi         = $_POST['emi'];
			$emi_amount         = $_POST['emi_amount'];
			$emi_installment         = $_POST['emi_installment'];
			 
			 
			if(!$_REQUEST['id']){
		
		 $query="insert into scheme(scheme_type, registration_fee, security_deposite, gas_supply, non_refund,mou, mou_value_deducted_against_registration, mou_customer_paid, mou_total_amount, mou_remaining_amount,emi,emi_amount,emi_installment,rental, remark, status, datetime) value('".$scheme_type."','".$registration_fee."', '".$security_deposite."', '".$gas_supply."', '".$non_refund."', '".$mou."', '".$mou_value_deducted_against_registration."', '".$mou_customer_paid."', '".$mou_total_amount."', '".$mou_remaining_amount."','".$emi."','".$emi_amount."', '".$emi_installment."','".$rental."','".$remark."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
			if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
        
			     $activity = "Added by ".($this->userName($_SESSION['adminid']))." to Scheme name:- (".$scheme_type.")"; 
    $this->log_report($activity);
	
	if($mou=='1') {
	$mou_value = mysql_query("INSERT INTO `mou_record`(`mou_value_deducted_against_registration`, `	mou_customer_paid`, `mou_total_amount`, `mou_remaining_amount`, `employee_id`, `status`, `date_time`) VALUES ('".$mou_value_deducted_against_registration."','".$mou_customer_paid."','".$mou_total_amount."','".$mou_remaining_amount."','".$_SESSION['adminid']."','1','".date("Y-m-d H:i:s")."')");
	
		$activity = "Added by ".($this->userName($_SESSION['adminid']))." to Scheme name:- (".$scheme_type.")"; 
    $this->log_report($activity);
	
			}
			header("location:index.php?control=regional_hierarchy&task=show_scheme");
		}
		else
		{
				$update="update scheme set scheme_type='".$scheme_type."', registration_fee='".$registration_fee."', security_deposite='".$security_deposite."',  mou='".$mou."', mou_value_deducted_against_registration='".$mou_value_deducted_against_registration."', mou_customer_paid='".$mou_customer_paid."', mou_total_amount='".$mou_total_amount."', mou_remaining_amount='".$mou_remaining_amount."', gas_supply='".$gas_supply."', non_refund='".$non_refund."', emi_installment='".$emi_installment."',emi='".$emi."', emi_amount='".$emi_amount."',rental='".$rental."', remark='".$remark."' where id='".$_REQUEST['id']."'";
		
				$this->Query($update);
				$this->Execute();
				
				if($this->Execute()) {	
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				}
				
				$data = mysql_fetch_array(mysql_query("SELECT * FROM  scheme WHERE id = '".$_REQUEST['id']."'"));
				
		if($mou=='1' && ($data['mou_value_deducted_against_registration']!=$mou_value_deducted_against_registration || $data['mou_customer_paid']!=$mou_customer_paid || $data['mou_total_amount']!=$mou_total_amount || $data['mou_remaining_amount']!=$mou_remaining_amount)) {
	$mou_value = mysql_query("INSERT INTO `mou_record`(`mou_value_deducted_against_registration`, `	mou_customer_paid`, `mou_total_amount`, `mou_remaining_amount`, `employee_id`, `status`, `date_time`) VALUES ('".$mou_value_deducted_against_registration."','".$mou_customer_paid."','".$mou_total_amount."','".$mou_remaining_amount."','".$_SESSION['adminid']."','1','".date("Y-m-d H:i:s")."')");
	
		$activity = "Added by ".($this->userName($_SESSION['adminid']))." to Scheme name:- (".$scheme_type.")"; 
    $this->log_report($activity);
	
			}
				
				
			     $activity = "Modify by ".($this->userName($_SESSION['adminid']))." to Scheme name:- (".$scheme_type.', id : '.$_REQUEST['id'].")"; 
    
		$this->log_report($activity);
		
		
				
				header("location:index.php?control=regional_hierarchy&task=show_scheme");
				
				}
		
		}
		
		function scheme_status(){
		$query = "update scheme set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}

    $activity = "Modify by ".($this->userName($_SESSION['adminid']))." to Scheme ID status :- (".$_REQUEST['status']. ',ID:- '.$_REQUEST['id'].")"; 
	$this->log_report($activity);
$this->show_scheme();
		}
		
		function scheme_delete(){
		
		$query="DELETE FROM scheme WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
  $activity = "Delete by ".($this->userName($_SESSION['adminid']))." to Scheme ID :- (".$_REQUEST['id'].")"; 
$this->log_report($activity);

$this->show_scheme();
		}
		
		
		/******************************************End Scheme  **********************************************************/		
		
		
		
		
		/****************************************** Start New Scheme  **********************************************************/	
		
		function show_new_connection_scheme(){	
		 $uquery ="select * from new_connection_scheme where status=1 order by id DESC"; 
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		 require_once("views/".$this->name."/show_new_connection_scheme.php");  
		}
		
		function addnew_new_connection_scheme() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  new_connection_scheme WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_new_connection_scheme(){
			$society_id = $_POST['society_id'];			$area_id = $_POST['area_id'];
			$city_id    = $_POST['city_id'];       $rental  = $_POST['rental'];
			
			$city = mysql_fetch_array(mysql_query("select * from city where id='".$city_id."'"));
			
			
			$boxes = $_POST['chkbox'];
			
/*$scheme_id = array();
$p = array_key_exists('scheme_id',$_POST) ? $_POST['scheme_id'] : array();
for($v = 0; $v <= $boxes; $v++){
    if(array_key_exists($v,$p)){
        $scheme_id[$v] = trim(stripslashes($p[$v]));
    }else{
        $scheme_id[$v] = 'No';
    }
}

print_r($scheme_id);
exit;*/
			
			
			if(!$_REQUEST['id']){
				
		    $scheme_id = array();
			$p = array_key_exists('scheme_id',$_POST) ? $_POST['scheme_id'] : array();
			for($v = 0; $v <= $boxes; $v++){
				if(array_key_exists($v,$p)){
					$scheme_id[$v] = trim(stripslashes($p[$v]));
					 $query="insert into new_connection_scheme(scheme_id,society_id,area_id,city_id,country_id,state_id,status,datetime) value('".$scheme_id[$v]."', '".$society_id."', '".$area_id."', '".$city_id."', '".$city['country_id']."', '".$city['state_id']."', '1', '".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
		$this->Execute();
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
					
				}else{
					$scheme_id[$v] = 'No';
				}
			}
		
		

        
		header("location:index.php?control=regional_hierarchy&task=show_new_connection_scheme");
		}
		else
		{
			 $scheme_id = array();
			$p = array_key_exists('scheme_id',$_POST) ? $_POST['scheme_id'] : array();
			for($v = 0; $v <= $boxes; $v++){
				if(array_key_exists($v,$p)){
					$scheme_id[$v] = trim(stripslashes($p[$v]));
			
			
				$update="update new_connection_scheme set scheme_id='".$scheme_id[$v]."', society_id='".$society_id."', area_id='".$area_id."', city_id='".$city_id."', country_id='".$city['country_id']."', state_id='".$city['state_id']."' where id='".$_REQUEST['id']."'";
				$this->Query($update);
				$this->Execute();
			/*	if(!$this->Execute()) {	
				 $query= mysql_query("insert into new_connection_scheme(scheme_id, society_id, area_id, city_id, country_id, state_id, status, datetime) value('".$scheme_id[$v]."', '".$society_id."', '".$area_id."', '".$city_id."', '".$city['country_id']."', '".$city['state_id']."', '1', '".date("Y-m-d H:i:s")."')");	
		    }*/
				
				
					}else{
					$scheme_id[$v] = 'No';
				}
			}
		
				
			
				$_SESSION['error'] = UPDATERECORD;	
				$_SESSION['errorclass'] = ERRORCLASS;
				
				
				
				
				header("location:index.php?control=regional_hierarchy&task=show_new_connection_scheme");
				
				}
		
		}
		
		function new_connection_scheme_status(){
		$query = "update new_connection_scheme set status='".$_REQUEST['status']."' WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = STATUS;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_new_connection_scheme();
		}
		
		function new_connection_scheme_delete(){
		
		$query="DELETE FROM new_connection_scheme WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
				if($this->Execute()) {	
$_SESSION['error'] = DELETE;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_new_connection_scheme();
		}
		
		
		/******************************************End New Scheme  **********************************************************/	
		
		
		
		/***************************************** POST START **********************************************************/
		
		function show_post(){	
		 $uquery ="select * from post where 1";
		$this->Query($uquery);
		$uresults = $this->fetchArray();	
		$tdata=count($uresults);
		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */
		$query = $uquery." LIMIT ".(($page-1)*$tpages).",".$tpages;	
		$this->Query($query);
		$results = $this->fetchArray();		
		
		require_once("views/".$this->name."/show_post.php"); 
		}
		
		function addnew_post() {
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM  post WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
                          else {
				
				    require_once("views/".$this->name."/".$this->task.".php"); 
			}
		}
		
		function save_post(){
			$post_name=$_POST['post_name'];
			if(!$_REQUEST['id']){
		
		$query="insert into post(post_name,status,datetime) value('".$post_name."','1','".date("Y-m-d H:i:s")."')";	
		$this->Query($query);	
				if($this->Execute()) {	
$_SESSION['error'] = ADDNEWRECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}

		header("location:index.php?control=regional_hierarchy&task=show_post");
		}
		else
		{
			$update="update post set post_name='".$post_name."' where id='".$_REQUEST['id']."'";
			$this->Query($update);
					if($this->Execute()) {	
$_SESSION['error'] = UPDATERECORD;	
$_SESSION['errorclass'] = ERRORCLASS;
}
$this->show_post();
			
		}

		
		}
		
		function post_delete(){
		
		$query="DELETE FROM post WHERE id in (".$_REQUEST['id'].")";	
		$this->Query($query);
		if($this->Execute()) {	
          $_SESSION['error'] = UPDATERECORD;	
         $_SESSION['errorclass'] = ERRORCLASS;
			}
			$this->show_post();
		}
		
		/***************************************************** POST END **********************************************************/
	
		

		
		function show_excel(){	
			 
		require_once("views/".$this->name."/".$this->task.".php"); 
		}
		
	
	}
