<?php session_start(); ?>
   <?php foreach($results as $result) { }  ?>
<div class="col-md-12">
        <div class="row">
			<div class="col-lg-12">            
			   <ol class="breadcrumb">    
				<li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=tender&task=show">Tender</a></li>
                <li class="active"> <?php if($result['id']!='') { ?> Edit <?php } else { ?>Add<?php } ?> Tender </li>
			</ol>
			</div>
		</div><!--/.row-->
      </div>  
<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                
                  <?php if($result['id']!='') { ?> <u><h3>Edit Tender </h3></u> <?php } else { ?><u><h3>Add New Tender</h3></u><?php } ?>
                    </div>
                    <br>
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
			
            <div class="col-md-8 col-md-offset-1">
            
            
			<div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Tender No :
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="tender_no" id="tender_no" value="<?php echo $result['tender_no']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			TENDER DATE:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="tender_date" id="tender_date" value="<?php echo $result['tender_date']; ?>" readonly="readonly"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			TENDER CLOSING DATE:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="tender_closing_date" id="tender_closing_date" value="<?php echo $result['tender_closing_date']; ?>" readonly="readonly"/>	
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
           <!-- <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			CLIENT:
			</div></div>
            
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="client" id="client" value="<?php echo $result['client']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            
            <div class="clearfix"></div>-->
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			PROJECT/WORK LOCATION:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="project" id="project" value="<?php echo $result['project']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			ITEM:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="item" id="item" value="<?php echo $result['item']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 COORDINATING OFFICE:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
            <textarea class="form-control" name="coordinating_office" id="coordinating_office"><?php echo $result['coordinating_office']; ?></textarea>
 	
			<span id="msgname" style="color:red;"></span>
			</div></div>
               <div class="clearfix"></div>        
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 ELIGIBILITY CRITERIA:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="eligibility_criteria" id="eligibility_criteria" value="<?php echo $result['eligibility_criteria']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
          <!--  <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			LAST SUBMISSION DATE:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="last_date" id="last_date" value="<?php echo $result['last_date']; ?>" readonly="readonly"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 SALE START DATE:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="sale_start_date" id="sale_start_date" value="<?php echo $result['sale_start_date']; ?>" readonly="readonly"/>			
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>-->
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 PREBID DATE & TIME:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="prebid_summary" id="prebid_summary" value="<?php echo $result['prebid_summary']; ?>" readonly="readonly"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
          <!--  <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 SALE END DATE:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="sale_end_date" id="sale_end_date" value="<?php echo $result['sale_end_date']; ?>" readonly="readonly"/>			
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div> -->
                
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			DOCUMENT COST:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
            <textarea class="form-control" name="document_cost" id="document_cost"><?php echo $result['document_cost']; ?></textarea>
	
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 PLACE OF SUBMISSION:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 
 <textarea class="form-control" name="submission_place" id="submission_place"><?php echo $result['submission_place']; ?></textarea>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
                        
           <!-- <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 EXECUTION LOCATION:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="execution_location" id="execution_location" value="<?php echo $result['execution_location']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>-->
            
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			TENDER OPENING ON:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="tender_opening" id="tender_opening" value="<?php echo $result['tender_opening']; ?>" readonly="readonly"/>			
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Upload File :
			</div></div>
		    <div class="col-md-4 col-md-offset-right-4"><div class="form-group" style="position:relative;">
				<?php 	
                $attch = mysql_query("select * from tender_attachment where tender_id = '".$result['id']."'");                $i=1;
                while($fetch = mysql_fetch_array($attch)){  ?>
              					<div id="filediv" class="col-md-12 multiupload">
                                       <div id="abcd<?php echo $i;?>" class="abcd">
                                       		<!--<img id="previewimg<?php echo $i;?>" src="<?php if($fetch['tender_file']) { ?>./../media/tender/<?php echo $fetch['tender_file'];  } else { echo "images/profile.jpg";}?>">-->
                                            
                                            <div class="filedis" >
                                            	<span>
                                               		 <?php
													 
														$fname = explode('/',$fetch['tender_file']);													 
														$prefix = $fetch['id'];
														$str = $fname[1];
														
														if (substr($str, 0, strlen($prefix)) == $prefix) {
															$str = substr($str, strlen($prefix));
														}
														 
														
														echo $str;
													  ?>
                                                </span>
                                            </div>
                                      		 <img id="img" onclick="delimage(<?php echo $fetch['id'];?>,<?php echo $i;?>);"  src="images/closehover.png" alt="delete" >  
                                       </div>
                                       <input name="tender_file[]" id="tender_file" value="<?php echo $fetch['tender_file']; ?>" style="display: none;" type="file"> 
                               	</div>
                                 
                                 <?php $i++;} ?>
                                 
                                    <div id="filediv" class="col-md-12 multiupload abcd">
                                       <input name="tender_file[]" type="file" id="tender_file"/> 
                               		</div>    
                                    <a class="add-image-box clearfix" id="add_more" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                     <span id="msgname" style="color:red;"></span>
			</div></div>               
                                 
                                 
                                 
                                 
                                 
                                 
                             
                              
                              </br>
               
                    
            <div class="col-md-12" align="center"><div class="form-group">
            <button type="submit" class="btn btn-primary"><?php if($result['id']) { echo "Update"; } else { echo "Submit";} ?></button>
            <!--<button type="submit" class="btn btn-primary">Send</button>-->
            </div></div>
            
            
            </div>      
                      <input type="hidden" name="control" value="tender"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="student_id" id="student_id" value="<?php echo $_SESSION['student_id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                      

		</form>

                        </div>
					    </div>			
					    </div>



<script>
  function isEmail(text)
{
	
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	//"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
	var regex = new RegExp( pattern );
	return regex.test(text);
	
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}

</script>


<script type="text/javascript">
function validation()
{
	/* var chk=1;
if(document.getElementById('email').value == '') { 
	document.getElementById('msgemail').innerHTML ="*Required field.";
	chk=0;
	}else if(!isEmail(document.getElementById('email').value)){ 
	document.getElementById('msgemail').innerHTML ="*Must be email-id only.";
	chk=0;	
    }else {
	document.getElementById('msgemail').innerHTML = "";
	}
	
	if(document.getElementById('name').value == '') { 
	document.getElementById('msgname').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgname').innerHTML = "";
	}
	
	if(document.getElementById('subject').value == '') { 
	document.getElementById('msgsubject').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgsubject').innerHTML = "";
	}
	
	if(document.getElementById('message').value == '') { 
	document.getElementById('msgmessage').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgmessage').innerHTML = "";
	}
	
	if(document.getElementById('mobile').value == '') { 
	document.getElementById('msgmobile').innerHTML ="*Required field.";
    chk=0;
	}else if(!numeric(document.getElementById('mobile').value)){ 
	document.getElementById('msgmobile').innerHTML ="*Must be numeric only.";
	chk=0;	
    }
	else {
	document.getElementById('msgmobile').innerHTML = "";
	}
	
	
	if(document.getElementById('filename').value == '') {
	chk = 0;
	document.getElementById('msgfilename').innerHTML ="This field is required";	
	}else {
	var image = document.getElementById('filename').value;
	var imagefzipLength = image.length;
	var imagefzipDot = image.lastIndexOf(".");
	var imagefzipType = image.substring(imagefzipDot,imagefzipLength);
	if(image) {
		
			if((imagefzipType==".jpg")||(imagefzipType==".jpeg")||(imagefzipType==".gif")||(imagefzipType==".png")||(imagefzipType==".doc")||(imagefzipType==".docx")) {
				   document.getElementById('msgfilename').innerHTML = "";
				   
				      var fsize = $('#filename')[0].files[0].size; //get file size
				if(fsize>102400) 
				{   alert("Upload Max 100Kb File");
					document.getElementById('msgfilename').innerHTML = "Please reduce the size of your file using an image editor."; 
					chk =0;
				}
			}
			else {	chk = 0;
				 document.getElementById('msgfilename').innerHTML = "Invalid file format only (jpg,gif,png,doc)"; 
			}
		 }
	} */
	var chk=1;
<!--Designation NAME Validation -->
if(document.getElementById("post_name").value == '') {
chk = 0;
document.getElementById('post_name').focus();
document.getElementById('post_name').style.borderColor="red";} 
else if (!isletter(document.getElementById("post_name").value)) {
chk = 0;
document.getElementById('post_name').focus();
document.getElementById('post_name').style.borderColor="red";}
else {
document.getElementById('post_name').style.border="1px solid #ccc";
}

<!-- Operation Area Validation -->
if(document.getElementById("area").value == '') {
chk = 0;
document.getElementById('area').focus();
document.getElementById('area').style.borderColor="red";
}
else {
document.getElementById('area').style.border="1px solid #ccc";
}
<!-- Management Lavel Validation -->
if(document.getElementById("level").value == '') {
chk = 0;
document.getElementById('level').focus();
document.getElementById('level').style.borderColor="red";
}
else {
document.getElementById('level').style.border="1px solid #ccc";
}
	if(chk) {	
	        alert("Doctor Registration Successful ..!!");
		return true;
		}
		else {
		return false;		
		}	
}

</script>


<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
/*$('#tender_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',

});*/

$('#tender_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y/m/d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#tender_closing_date').datetimepicker({value:'',step:30});
$('#tender_closing_date').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:true
});
$('#last_date').datetimepicker({value:'',step:30});
$('#last_date').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:true
});


$('#sale_start_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y/m/d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#sale_end_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y/m/d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

$('#tender_opening').datetimepicker({value:'',step:30});
$('#tender_opening').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'30:00',
	timepickerScrollbar:true
});


$('#prebid_summary').datetimepicker({value:'',step:30});
$('#prebid_summary').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'30:00',
	timepickerScrollbar:true
});

</script>
<script>
function delimage(obj,srno)
{
		 if(confirm('Are you sure you want to delete this file?')) {
		
	var image_id = obj;
	alert(image_id);
	
	
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		//document.getElementById("previewimg"+srno).innerHTML = xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		$('#abcd'+srno).hide();
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/tender_delimage.php?id="+image_id, true);
		
		xmlhttp.send();
	 }
	 
       
	
}
var abc = 0; //Declaring and defining global increement variable

$(document).ready(function() {

//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
    $('#add_more').click(function() {
		
        $(this).before($("<div class='col-md-12 multiupload abcd'/>", {id: 'filediv'}).fadeIn('slow').append(
                $("<input/>", {name: 'tender_file[]', type: 'file', id: 'tender_file'}),        
                $("")
				
                ));
				
		//$(this).before("<div class='clearfix'></div>");
		
    });

//following function will executes on change event of file input to select different file	
$('body').on('change', '#tender_file', function(){
            if (this.files && this.files[0]) {
                 abc =  $('.abcd').length + 1; //increementing global variable by 1
				 var filename  =this.files[0].name;
				//alert(abc);
				var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
               // $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src='' /></div>");
               $(this).before("<div class='filedis' id='abcd"+ abc +"' ><span>"+ filename +"</span></div>");
			   //alert(this.files[0].name);
			  // <div class='filedis' id='abcd"+ abc +"' ><span></span></div>
			    var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
               
			    $(this).hide();
                $("#abcd"+ abc).append($("<img/>", {id: 'img', src: 'images/closehover.png', alt: 'delete'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

//To preview image     
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    
});
</script>

<style>
.upload{
    background-color:#ff0000;
    border:1px solid #ff0000;
    color:#fff;
    border-radius:5px;
    padding:10px;
    text-shadow:1px 1px 0px green;
    box-shadow: 2px 2px 15px rgba(0,0,0, .75);
}
.upload:hover{
    cursor:pointer;
    background:#c20b0b;
    border:1px solid #c20b0b;
    box-shadow: 0px 0px 5px rgba(0,0,0, .75);
}
/*#file{
    color:green;
    padding:5px; border:1px dashed #123456;
    background-color: #f9ffe5;
}*/
#tender_files {
  margin: 0 auto;
  position: relative;
  top: 32px;
  white-space: normal;
  width: 80px;
}
#upload{
    margin-left: 45px;
}

#noerror{
    color:green;
    text-align: left;
}
#error{
    color:red;
    text-align: left;
}
#img{ 
    width: 17px;
    border: none; 
    height:17px;
    margin-left: -20px;
    margin-bottom: 91px;
}
 #imgdel{ 
    width: 17px;
    border: none; 
    height:17px;
    margin-left: -20px;
    margin-bottom: 91px;
}

.abcd img{
    height:100px;
    width:100px;
    padding: 5px;
    border: 1px solid rgb(232, 222, 189);
}
 
#formget{
    float:right; 

}
.add-image-box{
	font-size: 28px;
	
}
.multiupload{
	padding: 0px !important;
}
.add-image-box.clearfix {
 position:absolute;
 top:-9px;
 right:-40px;
}
#img {
  border: medium none;
  height: 26px;
  margin-bottom: 91px;
  margin-left: -20px;
  position: absolute;
  top: 6px;
  right:0;
  width: 26px;
}
#imgdel {
  border: medium none;
  height: 26px;
  margin-bottom: 91px;
  margin-left: -20px;
  position: absolute;
  top: -3px;
  width: 26px;
}
.abcd {
  position: relative;
}
.multiupload {
  margin-bottom: 15px !important;
}
.filedis {
    background: #f2f2f2 none repeat scroll 0 0;
    display: block;
    padding: 10px;
}
</style>