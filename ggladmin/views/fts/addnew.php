<?php session_start(); ?>
   <?php foreach($results as $result) { }  ?>
<div class="col-md-12">
        <div class="row">
			<div class="col-lg-12">            
			   <ol class="breadcrumb">    
				<li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=fts">FTS</a></li>
                <li class="active"> <?php if($result['id']!='') { ?> Edit <?php } else { ?>Add<?php } ?> Initiate New files </li>
			</ol>
			</div>
		</div><!--/.row-->
      </div>  
<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                
                  <u><h3><?php if($result['id']!='') { ?> Edit <?php } else { ?>Add New <?php } ?>Initiate New files</h3></u>
                    </div>
                    <br>
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
			
            <div class="col-md-8 col-md-offset-1">
            
            
			<div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			File Reference(Physical) :
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="file_ref" id="file_ref" value="<?php echo $result['file_ref']; ?>"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Subject:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <textarea class="form-control" name="subject" id="subject"><?php echo $result['subject']; ?></textarea>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Route:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
                       <select name="route" id="route" class="form-control" >
                                <option value="">Select</option>
                                <option value="Normal" <?php if($result['route']=='Normal'){echo "selected";} ?>>Normal</option>
                                <option value="Green line" <?php if($result['route']=='Green line'){echo "selected";} ?>>Green line</option>
                            </select>
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
         
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Initiator:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
 <input type="text" class="form-control" name="initiate_email" id="initiate_email" value="<?php echo $_SESSION['email']; ?>" readonly="readonly"/>	
 <input type="hidden" name="initiate_id" id="initiate_id" value="<?php echo $_SESSION['adminid']; ?>" readonly="readonly"/>		
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Send to:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
   <select class="form-control" type="text" name="emp_sendto" id="emp_sendto"  >
                    <option value="">---Select Officer / Department / Location---</option>
                    <?php $sqlten = mysql_query("select * from users where utype!='Admin' and status=1 and id!='".$_SESSION['adminid']."'"); 
					   while($user = mysql_fetch_array($sqlten)){
						$dept = mysql_fetch_array(mysql_query("select * from department where id='".$user['department_id']."' and status=1"));    		$city = mysql_fetch_array(mysql_query("select * from city where id='".$user['city_id']."' and status=1")); 
						   ?>
                    <option value="<?php echo $user['id']; ?>" <?php if($user['id']==$result['emp_sendto']){echo "selected";} ?> ><?php echo $user['name']. " (". $user['email']. ") / ". $dept['name']. " / ".$city['city_name']; ?></option>
                    <?php } ?>
                  
                  </select>	
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
            
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			 Action for:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
          <select name="actionfor" id="actionfor" class="form-control">
                            <option value="">Select 'Action For'</option>
                            <option value="Finance Concurrence" <?php if($result['actionfor']=='Finance Concurrence'){echo "selected";}?>>Finance Concurrence</option>
                            <option value="Approval" <?php if($result['actionfor']=='Approval'){echo "selected";}?>>Approval</option>
                            <option value="Indent Raising" <?php if($result['actionfor']=='Indent Raising'){echo "selected";}?>>Indent Raising</option>
                            <option value="Implementation" <?php if($result['actionfor']=='Implementation'){echo "selected";}?>>Implementation</option>
                            <option value="Tender Preparation" <?php if($result['actionfor']=='Tender Preparation'){echo "selected";}?>>Tender Preparation</option>
                            <option value="Tender Vetting" <?php if($result['actionfor']=='Tender Vetting'){echo "selected";}?>>Tender Vetting</option>
                            <option value="Tender Floating" <?php if($result['actionfor']=='Tender Floating'){echo "selected";}?>>Tender Floating</option>
                            <option value="TBA/CBA" <?php if($result['actionfor']=='TBA/CBA'){echo "selected";}?>>TBA/CBA</option>
                            <option value="TQ/CQ Forwarding" <?php if($result['actionfor']=='TQ/CQ Forwarding'){echo "selected";}?>>TQ/CQ Forwarding</option>
                            <option value="TCR For Price Bid Opening" <?php if($result['actionfor']=='TCR For Price Bid Opening'){echo "selected";}?>>TCR For Price Bid Opening</option>
                            <option value="Comparative statement Preparation" <?php if($result['actionfor']=='Comparative statement Preparation'){echo "selected";}?>>Comparative statement Preparation</option>
                            <option value="TCR For PO Placement" <?php if($result['actionfor']=='TCR For PO Placement'){echo "selected";}?>>TCR For PO Placement</option>
                            <option value="PO Placement" <?php if($result['actionfor']=='PO Placement'){echo "selected";}?>>PO Placement</option>
                            <option value="Reply To Querry" <?php if($result['actionfor']=='Reply To Querry'){echo "selected";}?>>Reply To Querry</option>
                            <option value="Forwarding" <?php if($result['actionfor']=='Forwarding'){echo "selected";}?>>Forwarding</option>
                            <option value="Recommendation" <?php if($result['actionfor']=='Recommendation'){echo "selected";}?>>Recommendation</option>
                            <option value="Examine And Put UP" <?php if($result['actionfor']=='Examine And Put UP'){echo "selected";}?>>Examine And Put UP</option>
                            <option value="File" <?php if($result['actionfor']=='File'){echo "selected";}?>>File</option>
                            <option value="Put Up Draft Reply" <?php if($result['actionfor']=='Put Up Draft Reply'){echo "selected";}?>>Put Up Draft Reply</option>
                            <option value="Reply" <?php if($result['actionfor']=='Reply'){echo "selected";}?>>Reply</option>
                            <option value="Release Of Payment" <?php if($result['actionfor']=='Release Of Payment'){echo "selected";}?>>Release Of Payment</option>
                            <option value="Recalled" <?php if($result['actionfor']=='Recalled'){echo "selected";}?>>Recalled</option>
                            <option value="Change Importance Of File" <?php if($result['actionfor']=='Change Importance Of File'){echo "selected";}?>>Change Importance Of File</option>
                        </select>
 	
			<span id="msgname" style="color:red;"></span>
			</div></div>
               <div class="clearfix"></div>        
            <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Action Taken:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
            <select name="actiontaken" id="actiontaken" class="form-control">  
                            <option value="">Action 'Taken'</option> 
                             <option value="Returned" <?php if($result['actiontaken']=='Returned'){echo "selected";}?>>Returned</option>
                            <option value="Physical file not received and returned" <?php if($result['actiontaken']=='Physical file not received and returned'){echo "selected";}?>>Physical file not received and returned</option>
                            <option value="Finance Concurred" <?php if($result['actiontaken']=='Finance Concurred'){echo "selected";}?>>Finance Concurred</option>
                            <option value="Approved" <?php if($result['actiontaken']=='Approved'){echo "selected";}?>>Approved</option>
                            <option value="Not-Approved" <?php if($result['actiontaken']=='Not-Approved'){echo "selected";}?>>Not-Approved</option>
                            <option value="Indent Raised" <?php if($result['actiontaken']=='Indent Raised'){echo "selected";}?>>Indent Raised</option>
                            <option value="Implemented" <?php if($result['actiontaken']=='Implemented'){echo "selected";}?>>Implemented</option>
                            <option value="Tender Prepared" <?php if($result['actiontaken']=='Tender Prepared'){echo "selected";}?>>Tender Prepared</option>
                            <option value="Tender Vetted" <?php if($result['actiontaken']=='Tender Vetted'){echo "selected";}?>>Tender Vetted</option>
                            <option value="Tender Floated" <?php if($result['actiontaken']=='Tender Floated'){echo "selected";}?>>Tender Floated</option>
                            <option value="TBA/CBA Done" <?php if($result['actiontaken']=='TBA/CBA Done'){echo "selected";}?>>TBA/CBA Done</option>
                            <option value="TQ/CQ  Forwarded" <?php if($result['actiontaken']=='TQ/CQ  Forwarded'){echo "selected";}?>>TQ/CQ  Forwarded</option>
                            <option value="TCR For Price Bid Opened" <?php if($result['actiontaken']=='TCR For Price Bid Opened'){echo "selected";}?>>TCR  For Price Bid Opened</option>
                            <option value="Comparative Statement Prepared" <?php if($result['actiontaken']=='Comparative Statement Prepared'){echo "selected";}?>>Comparative Statement Prepared</option>
                            <option value="TCR For PO Placement" <?php if($result['actiontaken']=='TCR For PO Placement'){echo "selected";}?>>TCR For PO Placement</option>
                            <option value="PO Placed" <?php if($result['actiontaken']=='PO Placed'){echo "selected";}?>>PO Placed</option>
                            <option value="Replied To  Query" <?php if($result['actiontaken']=='Replied To  Query'){echo "selected";}?>>Replied To  Query</option>
                            <option value="Forwarded" <?php if($result['actiontaken']=='Forwarded'){echo "selected";}?>>Forwarded</option>
                            <option value="Recommendation Given" <?php if($result['actiontaken']=='Recommendation Given'){echo "selected";}?>>Recommendation Given</option>
                            <option value="Examined And Re Put UP" <?php if($result['actiontaken']=='Examined And Re Put UP'){echo "selected";}?>>Examined And Re Put UP</option>
                            <option value="Filed" <?php if($result['actiontaken']=='Filed'){echo "selected";}?>>Filed</option>
                            <option value="Draft Reply Put Up" <?php if($result['actiontaken']=='Draft Reply Put Up'){echo "selected";}?>>Draft Reply Put Up</option>
                            <option value="Replied" <?php if($result['actiontaken']=='Replied'){echo "selected";}?>>Replied</option>
                            <option value="Payment Released" <?php if($result['actiontaken']=='Payment Released'){echo "selected";}?>>Payment Released</option>
                            <option value="Recalled" <?php if($result['actiontaken']=='Recalled'){echo "selected";}?>>Recalled</option>
                            <option value="Change Importance Of File" <?php if($result['actiontaken']=='Change Importance Of File'){echo "selected";}?>>Change Importance Of File</option>
                        </select>	
			<span id="msgactiontaken" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>
              
              
             <!-- <div class="col-md-3 col-md-offset-2">
			<div class="form-group" >
			Action:
			</div></div>
			<div class="col-md-4 col-md-offset-right-4"><div class="form-group">
                       <select name="initiate_by_action" id="initiate_by_action" class="form-control">
                                <option value="">Select</option>
                                <option value="1" <?php if($result['initiate_by_action']=='1'){echo "selected";}?>>Approved</option>
                                <option value="2" <?php if($result['initiate_by_action']=='2'){echo "selected";}?>>Not-Approved</option>
                                <option value="3" <?php if($result['initiate_by_action']=='3'){echo "selected";}?>>Pending</option>
                            </select>
			<span id="msgname" style="color:red;"></span>
			</div></div>
            <div class="clearfix"></div>  -->     
                              </br>
               
                    
            <div class="col-md-12" align="center"><div class="form-group">
            <button type="submit" class="btn btn-primary"><?php if($result['id']) { echo "Update"; } else { echo "Submit";} ?></button>
            <!--<button type="submit" class="btn btn-primary">Send</button>-->
            </div></div>
            
            
            </div>     
                     <input type="hidden" name="initiate_by_action" id="initiate_by_action" value="Pending" placeholder="Pending"/> 
                      <input type="hidden" name="control" value="fts"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                      

		</form>

                        </div>
					    </div>			
					    </div>



<script>
  function isEmail(text)
{
	
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	//"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
	var regex = new RegExp( pattern );
	return regex.test(text);
	
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}

</script>


<script type="text/javascript">
function validation()
{
	
	var chk=1;
<!--Designation NAME Validation -->
if(document.getElementById("post_name").value == '') {
chk = 0;
document.getElementById('post_name').focus();
document.getElementById('post_name').style.borderColor="red";} 
else if (!isletter(document.getElementById("post_name").value)) {
chk = 0;
document.getElementById('post_name').focus();
document.getElementById('post_name').style.borderColor="red";}
else {
document.getElementById('post_name').style.border="1px solid #ccc";
}

<!-- Operation Area Validation -->
if(document.getElementById("area").value == '') {
chk = 0;
document.getElementById('area').focus();
document.getElementById('area').style.borderColor="red";
}
else {
document.getElementById('area').style.border="1px solid #ccc";
}
<!-- Management Lavel Validation -->
if(document.getElementById("level").value == '') {
chk = 0;
document.getElementById('level').focus();
document.getElementById('level').style.borderColor="red";
}
else {
document.getElementById('level').style.border="1px solid #ccc";
}
	if(chk) {	
	        alert("Doctor Registration Successful ..!!");
		return true;
		}
		else {
		return false;		
		}	
}

</script>


<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
/*$('#tender_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',

});*/

$('#tender_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y/m/d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#tender_closing_date').datetimepicker({value:'',step:30});
$('#tender_closing_date').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:true
});
$('#last_date').datetimepicker({value:'',step:30});
$('#last_date').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:true
});


$('#sale_start_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y/m/d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#sale_end_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y/m/d',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

$('#tender_opening').datetimepicker({value:'',step:30});
$('#tender_opening').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'30:00',
	timepickerScrollbar:true
});


$('#prebid_summary').datetimepicker({value:'',step:30});
$('#prebid_summary').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'30:00',
	timepickerScrollbar:true
});

</script>
<script>
function delimage(obj,srno)
{
		 if(confirm('Are you sure you want to delete this file?')) {
		
	var image_id = obj;
	alert(image_id);
	
	
		var xmlhttp; 
		if (window.XMLHttpRequest)
		{ // code for IE7+, Firefox, Chrome, Opera, Safari 
		xmlhttp=new XMLHttpRequest();
		}
		else
		{ // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{ 
		if (xmlhttp.readyState==4)
		{
		//document.getElementById("previewimg"+srno).innerHTML = xmlhttp.responseText; 
		//alert(xmlhttp.responseText);
		$('#abcd'+srno).hide();
		}
		}
		xmlhttp.open("GET", "script/popup_scripts/tender_delimage.php?id="+image_id, true);
		
		xmlhttp.send();
	 }
	 
       
	
}
var abc = 0; //Declaring and defining global increement variable

$(document).ready(function() {

//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
    $('#add_more').click(function() {
		
        $(this).before($("<div class='col-md-12 multiupload abcd'/>", {id: 'filediv'}).fadeIn('slow').append(
                $("<input/>", {name: 'tender_file[]', type: 'file', id: 'tender_file'}),        
                $("")
				
                ));
				
		//$(this).before("<div class='clearfix'></div>");
		
    });

//following function will executes on change event of file input to select different file	
$('body').on('change', '#tender_file', function(){
            if (this.files && this.files[0]) {
                 abc =  $('.abcd').length + 1; //increementing global variable by 1
				 var filename  =this.files[0].name;
				//alert(abc);
				var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
               // $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src='' /></div>");
               $(this).before("<div class='filedis' id='abcd"+ abc +"' ><span>"+ filename +"</span></div>");
			   //alert(this.files[0].name);
			  // <div class='filedis' id='abcd"+ abc +"' ><span></span></div>
			    var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
               
			    $(this).hide();
                $("#abcd"+ abc).append($("<img/>", {id: 'img', src: 'images/closehover.png', alt: 'delete'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

//To preview image     
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    
});
</script>

<style>
.upload{
    background-color:#ff0000;
    border:1px solid #ff0000;
    color:#fff;
    border-radius:5px;
    padding:10px;
    text-shadow:1px 1px 0px green;
    box-shadow: 2px 2px 15px rgba(0,0,0, .75);
}
.upload:hover{
    cursor:pointer;
    background:#c20b0b;
    border:1px solid #c20b0b;
    box-shadow: 0px 0px 5px rgba(0,0,0, .75);
}
/*#file{
    color:green;
    padding:5px; border:1px dashed #123456;
    background-color: #f9ffe5;
}*/
#tender_files {
  margin: 0 auto;
  position: relative;
  top: 32px;
  white-space: normal;
  width: 80px;
}
#upload{
    margin-left: 45px;
}

#noerror{
    color:green;
    text-align: left;
}
#error{
    color:red;
    text-align: left;
}
#img{ 
    width: 17px;
    border: none; 
    height:17px;
    margin-left: -20px;
    margin-bottom: 91px;
}
 #imgdel{ 
    width: 17px;
    border: none; 
    height:17px;
    margin-left: -20px;
    margin-bottom: 91px;
}

.abcd img{
    height:100px;
    width:100px;
    padding: 5px;
    border: 1px solid rgb(232, 222, 189);
}
 
#formget{
    float:right; 

}
.add-image-box{
	font-size: 28px;
	
}
.multiupload{
	padding: 0px !important;
}
.add-image-box.clearfix {
 position:absolute;
 top:-9px;
 right:-40px;
}
#img {
  border: medium none;
  height: 26px;
  margin-bottom: 91px;
  margin-left: -20px;
  position: absolute;
  top: 6px;
  right:0;
  width: 26px;
}
#imgdel {
  border: medium none;
  height: 26px;
  margin-bottom: 91px;
  margin-left: -20px;
  position: absolute;
  top: -3px;
  width: 26px;
}
.abcd {
  position: relative;
}
.multiupload {
  margin-bottom: 15px !important;
}
.filedis {
    background: #f2f2f2 none repeat scroll 0 0;
    display: block;
    padding: 10px;
}
</style>