<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>

<!--<link rel="stylesheet" href="styles/alertmessage.css" />-->
<script type="text/javascript">
    setTimeout("closeMsg('closeid2')",5000);
    function closeMsg(clss) {
      document.getElementById(clss).className = "clspop";

  }
</script>

<!--for alert message start-->
<style type="text/css">
    .clspop {
       display:none;	
   }
   .darkbase_bg {
     display:block !important;

 }		
</style>
<!---------POPUP MESSAGE------->
<?php if(isset($_SESSION['error'])){?>
  <div id="flashMessage" class="message">
    <div  class='darkbase_bg' id='closeid2'>
      <div class='alert_pink' > <a class='pop_close'> <img src="images/close.png" onclick="closeMsg('closeid2')" title="close" /> </a>
        <div class='pop_text <?php echo $_SESSION['errorclass']; ?>'><!--warn_red-->
          <p style='color:#063;'><?php echo $_SESSION['error']; ?></p>
      </div>
  </div>
</div>
</div>
<?php  
unset($_SESSION['error']);
unset($_SESSION['errorclass']);

}?>
<!--POPUP MESSAGE CLOSE-->
<!-------------------------------Alert Message--------------------------------> 
<div class="col-sm-12">			

  <div class="row">
     <div class="col-lg-12">
       <ol class="breadcrumb">
        <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
        <!--<li class="active"><a href="index.php?control=user_applynewconnection&task=show_admin">New Registration</a></li>-->
        <li class="active">Pending Registration</li>
    </ol>
</div>
</div><!--/.row-->

<div class="row" >
    <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
             <div class="col-md-7 col-sm-12">
              <div class="panel-heading">
               <u>
                <h3>Pending Registration Report</h3>
            </u> 
        </div>
    </div>
    <div class="col-md-5 col-sm-12">
       <?php 
       $current_date = date('Y-m-d');
       $dateFrom = $_REQUEST['from_date']?$_REQUEST['from_date']:$current_date;
       $dateTo = $_REQUEST['to_date']?$_REQUEST['to_date']:$current_date;
       ?>
       <div class="col-md-10">
           <a href="javascript:void(0);" onclick="window.open('excel/export3excel_new_connection_report.php?datefrom=<?php echo $dateFrom;?>&dateto=<?php echo $dateTo;?>');" class="btn btn-primary btn-md" id="btn-chat">Excel Report</a>				

       </div>


       <div class="col-md-1 col-sm-2">
        <form action="index.php" method="post" name="filterForm" id="filterForm" style="width:77px;">
            <!--<span style="font-size:12px;">Show entries :</span>-->
            <select name="filter"  class="form-control"  id="filter" onchange="paging1(this.value)">
                <option value="1000"<?php if($_REQUEST['tpages']=="1000") {?> selected="selected"<?php }?>>All</option>
                <option value="2"<?php if($_REQUEST['tpages']=="2") {?> selected="selected"<?php }?> >2</option>
                <option value="5"<?php if($_REQUEST['tpages']=="5") {?> selected="selected"<?php }?> >5</option>
                <option value="10"<?php if($_REQUEST['tpages']=="10") {?> selected="selected"<?php }?>>10</option>
                <option value="20"<?php if($_REQUEST['tpages']=="20") {?> selected="selected"<?php }?>>20</option>
                <option value="50"<?php if($_REQUEST['tpages']=="50") {?> selected="selected"<?php }?>>50</option>
                <option value="100"<?php if($_REQUEST['tpages']=="100") {?> selected="selected"<?php }?>>100</option>
            </select>
            <input type="hidden" name="control" value="<?php echo $_REQUEST['control'];?>"  />
            <input type="hidden" name="view" value="<?php echo $_REQUEST['view'];?>"  />
            <input type="hidden" name="task" id="task" value="<?php echo $_REQUEST['task'];?>"  />
            <input type="hidden" name="tpages" id="tpages" value=""  />
            <input type="hidden" name="adjacents" value="<?php echo $_REQUEST['adjacents'];?>"  />
            <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
            <input type="hidden" name="tmpid" value="<?php echo $_REQUEST['tmpid'];?>"  />
            <input type="hidden" name="id" id="id"  />
            <input type="hidden" name="status" id="status"  />
            <input type="hidden" name="defftask" id="defftask" value="pending_connection_admin" />
            <input type="hidden" name="del" id="del" />
            <input type="hidden" name="edit" id="edit" /> 
            <input type="hidden" name="from_date" value="<?php echo $_REQUEST['from_date']?$_REQUEST['from_date']:'';?>" />     
            <input type="hidden" name="to_date" value="<?php echo $_REQUEST['to_date']?$_REQUEST['to_date']:'';?>" />                        
            <input type="hidden" name="search" value="<?php echo $_REQUEST['search']?$_REQUEST['search']:'';?>" />  
            <input type="hidden" name="city_id" value="<?php echo $_REQUEST['city_id']?$_REQUEST['city_id']:'';?>" /> 
            <input type="hidden" name="asset_id" value="<?php echo $_REQUEST['asset_id']?$_REQUEST['asset_id']:'';?>" />    
            <input type="hidden" name="area_id" value="<?php echo $_REQUEST['area_id']?$_REQUEST['area_id']:'';?>" /> 
            <input type="hidden" name="contractor" value="<?php echo $_REQUEST['contractor']?$_REQUEST['contractor']:'';?>" /> 
            <input type="hidden" name="progress" value="<?php echo $_REQUEST['progress']?$_REQUEST['progress']:'';?>" />    
            <input type="hidden" name="crn" value="<?php echo $_REQUEST['crn']?$_REQUEST['crn']:'';?>" /> 

        </form>                 
    </div>

</div>
</div>
<div class="col-md-12">

   <?php foreach($results as $result) { }  ?>

   <form name="customer_search" action="" method="post" class="double_search">
      <?php if($_SESSION['department_id']=='1' || $_SESSION['department_id']=='' || $_SESSION['department_id']=='6') {?>         
        <div class="col-md-2 col-sm-2">
            <div class="form-group">
                <select class="form-control" type="text" name="city_id" id="txtcity" onchange="asset_change(this.value);area_change(this.value);">                    
                    <option value="">City</option>
                    <?php  $sql_city = mysql_query("select * from city where status='1'"); 
                    while($exe_city = mysql_fetch_array($sql_city)){?>
                        <option value="<?php echo $exe_city['id']; ?>" <?php if($exe_city['id']==$_REQUEST['city_id']){echo "selected";} ?> ><?php echo $exe_city['city_name']; ?></option>
                    <?php } ?>
                </select>  

            </div>
        </div> 
    <?php }  if($_SESSION['department_id']=='1' || $_SESSION['department_id']=='' || $_SESSION['department_id']=='6') {?>    
        <div class="col-md-2 col-sm-2">
            <div class="form-group">
                <select class="form-control" name="asset_id" id="asset_name" >                    
                    <option value="">Asset</option>
                    <?php  $sql_asset = mysql_query("select * from asset where status='1' and city_id = '".$_REQUEST['city_id']."' "); 
                    while($exe_asset = mysql_fetch_array($sql_asset)){?>
                        <option value="<?php echo $exe_asset['id']; ?>" <?php if($exe_asset['id']==$_REQUEST['asset_id']){echo "selected";} ?> ><?php echo $exe_asset['asset_name']; ?></option>
                    <?php } ?>
                </select>  

            </div>
        </div> 

        <div class="col-md-2 col-sm-2">
            <div class="form-group">
                <select class="form-control" type="text" name="area_id" id="txtarea">                    
                    <option value="">Area</option>
                    <?php  $sql_area = mysql_query("select * from area where status='1'"); 
                    while($exe_area = mysql_fetch_array($sql_area)){?>
                        <option value="<?php echo $exe_area['id']; ?>" <?php if($exe_area['id']==$_REQUEST['area_id']){echo "selected";} ?> ><?php echo $exe_area['area_name']; ?></option>
                    <?php } ?>
                </select>  

            </div>
        </div> 
    <?php  } else { ?>
        <div class="col-md-2 col-sm-2">
            <div class="form-group">
                <select class="form-control" name="asset_id" id="asset_name" >                    
                    <option value="">Asset</option>
                    <?php  $sql_asset = mysql_query("select * from asset where status='1'  and city_id = '".$_SESSION['city_id']."'"); 
                    while($exe_asset = mysql_fetch_array($sql_asset)){?>
                        <option value="<?php echo $exe_asset['id']; ?>" <?php if($exe_asset['id']==$_REQUEST['asset_id']){echo "selected";} ?> ><?php echo $exe_asset['asset_name']; ?></option>
                    <?php } ?>
                </select>  

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="form-group">
                <select class="form-control" type="text" name="area_id" id="area_id" >                    
                    <option value="">Area</option>
                    <?php  $sql_area = mysql_query("select * from area where status='1' and city_id = '".$_SESSION['city_id']."'"); 
                    while($exe_area = mysql_fetch_array($sql_area)){?>
                        <option value="<?php echo $exe_area['id']; ?>" <?php if($exe_area['id']==$_REQUEST['area_id']){echo "selected";} ?> ><?php echo $exe_area['area_name']; ?></option>
                    <?php } ?>
                </select>  

            </div>
        </div> 
    <?php } ?>
    <div class="col-md-2 col-sm-2">
        <div class="form-group">
            <select class="form-control" type="text" name="contractor" id="contractor" >
                <option value="">Proj.Contractor</option>
                <?php $sql_cont = mysql_query("select * from contractor_master where status='1'"); 
                while($contract = mysql_fetch_array($sql_cont)){?>
                    <option value="<?php echo $contract['id']; ?>" <?php if($contract['id']==$_REQUEST['contractor']){echo "selected";} ?> ><?php echo $contract['company_name']; ?></option>
                <?php } ?>
                <option value="blank" <?php if($_REQUEST['contractor']=='blank') {?> selected="selected" <?php } ?>>Blank</option>
            </select>
        </div>
    </div>  


    <div class="col-md-2 col-sm-2">
        <div class="form-group">
            <select class="form-control" name="progress" id="progress" >
                <option value="">Progress</option>
                <option value="ng_blank" <?php if($_REQUEST['progress']=='ng_blank') { ?> selected="selected" <?php } ?>>Proj NG Date Blank</option>
                <option value="rfa_blank" <?php if($_REQUEST['progress']=='rfa_blank') { ?> selected="selected" <?php } ?>>Mktg RFA Date Blank</option>
                <option value="ng_filled" <?php if($_REQUEST['progress']=='ng_filled') { ?> selected="selected" <?php } ?>>Proj NG Date Filled</option>
                <option value="rfa_filled" <?php if($_REQUEST['progress']=='rfa_filled') { ?> selected="selected" <?php } ?>>Mktg RFA Date Filled</option>

            </select>
        </div>
    </div>

    <div class="col-md-2 col-sm-2">
        <div class="form-group">
           <input type="text" class="form-control" name="crn" id="crn" value="<?php echo $_REQUEST['crn'];?>" placeholder="CRN No,Name,Mob." />
       </div>
   </div>   

   <div class="col-md-2 col-sm-2">
    <div class="form-group">
        <input class="form-control" type="text" name="from_date" id="from_date" value="<?php echo $_REQUEST['from_date'];?>"  placeholder="From Date"  readonly>
    </div>
</div>

<div class="col-md-2 col-sm-2">
    <div class="form-group">
        <input class="form-control" type="text" name="to_date" id="to_date" value="<?php echo $_REQUEST['to_date'];?>"  placeholder="To Date"  readonly>
    </div>
</div>



<span class="input-group-btn">
    <button class="btn btn-primary btn-md" type="submit" name="search" value="Search" >Search</button>


</span>
<input type="hidden" name="tpages" value="<?php echo $_REQUEST['tpages'];?>"  >
<input type="hidden" name="control" value="user_applynewconnection"/>
<input type="hidden" name="view" value="pending_connection_admin"/>
<input type="hidden" name="task" value="pending_connection_admin"/>

</form>


</div>


<div class="container">
    <div class="col-md-12"></div>
</div>
<br>
<!-- <table style="width:100%" data-toggle="table" id="table-style"  data-row-style="rowStyle">-->
   <table  id="data_table"  class="table table-bordered" >  
    <thead>
       <tr>
        <th><div align="center">S.No.</div></th>
        <th><div align="center">Form No.<br />Identification No.</div></th>
        <?php if($_SESSION['utype']=='Admin') { ?>
           <th><div align="center">Identification CRN</div></th>
       <?php } ?>
       <th><div align="center">Name</div></th>
       <th><div align="center">Mobile<br />Email</div></th> 
                          <!--<th><div align="center">House Type</div></th>
                             <th><div align="center">Email</div></th>
                          <th><div align="center">Visit Date</div></th>
                          <th><div align="center">Visit Time</div></th>-->
                          <th><div align="center">Society</div></th>
                          <th><div align="center"><b>Asset</b><br />Area<br />House Type</div></th>
                          <th><div align="center">Amount</div></th>
                          <th><div align="center">Payment Status</div></th>
                          <th><div align="center">Date</div></th>
                          <th><div align="center">Submitted By </div></th>
                          <th><div align="center">Action<br />Remark</div></th>
                      </tr>
                  </thead>
                  <?php
                  if($results) {
                      $countno = ($page-1)*$tpages;
                      $i=0;
                      foreach($results as $result){ 
                          $i++;
                          $countno++;
                          ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";

                          $area = mysql_fetch_array(mysql_query("select * from area where id='".$result['area_id']."'"));
                          $asset=mysql_fetch_array(mysql_query("select * from asset where id ='".$result['asset_id']."'"));						 
                          $user=mysql_fetch_array(mysql_query("select * from users where id ='".$result['employee_id']."'"));


                          ?>

                          <tr>
                            <td class="<?php echo $class; ?>"><div align="center"><?php echo $countno; ?></div></td>
                            <td class="<?php echo $class; ?>"><div align="center"><?php echo $result['form_no']; ?><br /><?php echo $result['application_no']; ?></div></td>
                            <?php if($_SESSION['utype']=='Admin') { ?>
                                <td class="<?php echo $class; ?>"><div align="center"><?php echo $result['crn'];?></div></td> 
                            <?php } ?>
                            <td class="<?php echo $class; ?>"><div align="center"> <a href="index.php?control=user_applynewconnection&task=show_detail_admin&id=<?php echo $result['id']; ?>" ><?php echo $result['cust_name'] ; ?>
                        </a></div></td>
                        <td class="<?php echo $class; ?>"><div align="left"><?php echo $result['mobile_no'] ; ?><br /><?php echo $result['email_id'];?></div></td>
                        <td class="<?php echo $class; ?>"><div align="center"><?php echo $this->societyName($result['society_id']);?></div></td>
                    <!--<td class="<?php echo $class; ?>"><div align="center"><?php echo $result['house_type'];?></div></td>
                    <td class="<?php echo $class; ?>"><div align="center"><?php echo $result['email_id'];?></div></td>
                    <td class="<?php echo $class; ?>"><div align="center"><?php echo $date = date('d-m-Y',strtotime($result['date'])); ?></div></td>-->
                    <td class="<?php echo $class; ?>"><div align="center"><b><?php echo $asset['asset_name'];?></b><br /> <?php echo $area['area_name']; ?><br /><?php echo $result['house_type'];?> </div> </td>
                    <td class="<?php echo $class; ?>"><div align="center"> Rs.<?php echo $result['amount']; ?>/- </div> </td>
                    <td class="<?php echo $class; ?>"><div align="center"> 
                       <?php 
                       if($result['payment_status']==1){?> 
                           <a href="index.php?control=user_applynewconnection&task=payment_verify&status=2&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Verify" onclick="return confirm('Are you sure you want to Payment-Verify?')"><span class="btn btn-primary">Success</span></a><br />
                           <a href="../ggluser/newconnection_payment_confirm.php?crn=<?php echo $result['crn']; ?>" style="cursor:pointer;" target="_blank">Trans Details</a>                          <?php }else{
                               if($result['verify_status']=='1') { echo "Pending"; }
                           } 

                           ?> </div> </td>
                           <td class="<?php echo $class; ?>"><div align="center"> <?php echo date('d-m-Y', strtotime($result['date'])); ?> </div> </td>

                           <!--<td class="<?php echo $class; ?>"><div align="center"><a href="index.php?control=user_applynewconnection&task=update_status&id=<?php echo $result['id']; ?>" target="_blank" > Update Status</a></div> </td>-->   
                           <td class="<?php echo $class; ?>"><div align="center"><?php if($result['form_type']=='1') { ?> <b style="color:#F00">Online</b><?php } else { ?><b style="color:#175200"><?php echo $user['name']; ?></b><?php }?></div> </td>



                           <td class="<?php echo $class; ?>"><div align="center">

                              <?php if($_SESSION['department_id']=='3' && $_SESSION['utype']!='Admin') { ?> 
                              
                                <?php  if($result['verify_status']==0 && $_SESSION['utype']!='Admin'){?> <a href="index.php?control=user_applynewconnection&task=show_detail_admin&id=<?php echo $result['id']; ?>" ><b>Check Profile</b></a><br /><?php   }
                                  if($result['verify_status']=='1' && $_SESSION['utype']!='Admin' && $result['form_type']=='') { echo "<span style='color:green;'><b>Doc Verify</b></span>"; } ?>
                                <?php 
                                if($result['verify_status']=='1' && $_SESSION['utype']!='Admin' && $result['form_type']=='1') { ?> <a href="index.php?control=user_applynewconnection&task=show_detail_admin&id=<?php echo $result['id']; ?>" style="color: #F0F"><b>Online Doc Verify</b></a>  <?php } ?>

                                <?php if($result['verify_status']=='1' && $_SESSION['utype']!='Admin') { ?>  <br /><a href="index.php?control=user_applynewconnection&task=send_sms&id=<?php echo $result['id']; ?>" style="color: #F30;"><b>Send SMS</b></a><?php } 

                                if($result['verify_status']=='1' && $_SESSION['utype']=='Admin') { ?> <a href="index.php?control=user_applynewconnection&task=show_detail_admin&id=<?php echo $result['id']; ?>" style="color:green;"><b>Doc Verify</b></a>  <?php } ?>
                                <?php    if($result['verify_status']=='2') { echo "<span style='color:green;'><b>Payment Verify</b></span>"; } 
                                if($result['verify_status']=='3' && $_SESSION['utype']!='Admin') { ?> <a href="index.php?control=user_applynewconnection&task=show_detail_admin&id=<?php echo $result['id']; ?>" style="color:#F00;"><b>Not - Verify</b></a><br><b style='color:blue;'><?php echo $result['remark'];?></b> <?php } 
                                ?>
                            <?php } else  {
                                if($result['verify_status']=='1') { echo "<span style='color:blue;'><b>Check Profile</b><br /></span>"; } 
                                if($result['verify_status']=='1') { echo "<span style='color:green;'><b>Doc Verify</b><br /></span>"; } 
                                if($result['verify_status']=='2') { echo "<span style='color:green;'><b>Payment Verify</b><br /></span>"; } 
                                if($result['verify_status']=='3') { echo "<span style='color:red;'><b>Not-Verify</b></span><br><b style='color:blue;'>".$result['remark']."</b>"; } ?> 
                            <?php } 
                            if(($result['verify_status']=='0' || $result['verify_status']=='3') && $_SESSION['department_id']=='11') { ?>
                               <br><a href="index.php?control=user_applynewconnection&task=addnew&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a>
                               <?php } ?></div></td>
                           </tr>
                       <?php }  

                   }
                   else{ ?>
                    <div>
                      <!-- <strong>Data not found.</strong>-->
                  </div>
                  <!-- <tbody><tr class="no-records-found"><td colspan="11">No matching records found</td></tr></tbody>-->
              <?php } ?>

          </table>
      </div>
      <?php if(count($results)>0){ ?>
        <?php
        include("pagination.php");
        echo paginate_three($reload, $page, $tpages, $adjacents,$tdata); ?>
    <?php } ?>
</div>
</div>
</div>
</div>

<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>  
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
    $('#from_date').datetimepicker({
       yearOffset:0,
       lang:'ch',
       timepicker:false,
       format:'Y-m-d',
       formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
    $('#to_date').datetimepicker({
       yearOffset:0,
       lang:'ch',
       timepicker:false,
       format:'Y-m-d',
       formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});


    function customer_edit(id)
    {
		//alert(id);
		document.getElementById("edit").value = Number(id);
		document.getElementById("tpages").value = Number(document.getElementById("filter").value);
		document.getElementById("task").value = "edit";
		document.getElementById("id").value = id;
		//document.getElementById("pageNo").value = Number(document.getElementById("page").value)*Number(document.getElementById("filterForm").value);
		/*document.getElementById("searchForm").action="/betcha/index.php/promotion/edit/"+edit;*/
		document.filterForm.submit(); 
	}


</script> 			
