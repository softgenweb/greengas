<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:5;?>
<!--<link rel="stylesheet" href="styles/alertmessage.css" />-->
<script type="text/javascript">
   setTimeout("closeMsg('closeid2')",5000);
   function closeMsg(clss) {
   		document.getElementById(clss).className = "clspop";
   	}
       
</script>
<!--for alert message start-->
<style type="text/css">
   .clspop {
   display:none;	
   }
   .darkbase_bg {
   display:block !important;
   }
</style>
<!--POPUP MESSAGE-->
<?php if(isset($_SESSION['error'])){
   ?>
<div id="flashMessage" class="message">
   <div  class='darkbase_bg' id='closeid2'>
      <div class='alert_pink' >
         <a class='pop_close'> <img src="images/close.png" onclick="closeMsg('closeid2')" title="close" /> </a>
         <div class='pop_text <?php echo $_SESSION['errorclass']; ?>'>
            <!--warn_red-->
            <p style='color:#063;'><?php echo $_SESSION['error']; ?></p>
         </div>
      </div>
   </div>
</div>
<?php  
   unset($_SESSION['error']);
   unset($_SESSION['errorclass']);
   
   }?>
<!--POPUP MESSAGE CLOSE-->
<!-------------------------------Alert Message-------------------------------->  
<div class="col-sm-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=contractor_master">Contractor List</a></li>
            <li class="active">Contractor Detail</li>
         </ol>
      </div>
   </div>
   <!--/.row-->
   <div class="row" >
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="col-md-4" style="top: -15px;">
                  <div class="panel-heading">
                     <u>
                        <h3>Contractor List</h3>
                     </u>
                  </div>
               </div>
               <div class="col-md-8" style="margin-top:1%">
                  <div class="col-md-8">
                     <?php foreach($results as $result) { }  ?>
                     <span class="col-md-6" style="top: 8px; color:<?php if($result[0]['name']!=''){ echo 'green';} else { echo 'red';}?>">
                     <?php  
                        if($_REQUEST['search']!=''){
                        	if($result[0]['name']!=''){ 
                        		echo "Welcome! " .$result['name'];
                        	}
                        	else{ 
                        		echo "Sorry! This Does Not Exists.";
                        	}
                        }				
                        ?>
                     </span>
                     <form class="col-md-6" name="customer_search" action="index.php" method="post" onsubmit="return validation();">
                        <div class="input-group">
                           <!--<input type="text" id="search" class="form-control input-md" name="search" value="<?php echo $_REQUEST['search']?$_REQUEST['search']:$_REQUEST['search'];?>" placeholder="Type your name or userid search here..." />
                              <span class="input-group-btn">
                              	<button class="btn btn-primary btn-md" id="btn-chat">Search</button>
                              </span>-->
                           <input type="hidden" name="control" value="contractor_master"/>
                           <input type="hidden" name="view" value="show"/>
                           <input type="hidden" name="task" value="show"/>
                        </div>
                     </form>
                  </div>
                  	<?php if($_SESSION['department_id']!='1' && $_SESSION['department_id']!='6') {?>
                  <div class="col-md-3">				
                     <a href="index.php?control=contractor_master&task=addnew" class="btn btn-primary btn-md" id="btn-chat">Add New Contractor</a>
                  </div>
                  	<?php } ?>
                  <div class="col-md-1">
                     <form action="index.php" method="post" name="filterForm" id="filterForm" style="width:77px;">
                        <!--<span style="font-size:12px;">Show entries :</span>-->
                        <select name="filter"  class="form-control"  id="filter" onchange="paging1(this.value)">
                           <option value="1000"<?php if($_REQUEST['tpages']=="1000") {?> selected="selected"<?php }?>>All</option>
                           <option value="2"<?php if($_REQUEST['tpages']=="2") {?> selected="selected"<?php }?> >2</option>
                           <option value="5"<?php if($_REQUEST['tpages']=="5") {?> selected="selected"<?php }?> >5</option>
                           <option value="10"<?php if($_REQUEST['tpages']=="10") {?> selected="selected"<?php }?>>10</option>
                           <option value="20"<?php if($_REQUEST['tpages']=="20") {?> selected="selected"<?php }?>>20</option>
                           <option value="50"<?php if($_REQUEST['tpages']=="50") {?> selected="selected"<?php }?>>50</option>
                           <option value="100"<?php if($_REQUEST['tpages']=="100") {?> selected="selected"<?php }?>>100</option>
                        </select>
                        <input type="hidden" name="control" value="<?php echo $_REQUEST['control'];?>"  />
                        <input type="hidden" name="view" value="<?php echo $_REQUEST['view'];?>"  />
                        <input type="hidden" name="task" id="task" value="<?php echo $_REQUEST['task'];?>"  />
                        <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages'];?>"  />
                        <input type="hidden" name="adjacents" value="<?php echo $_REQUEST['adjacents'];?>"  />
                        <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                        <input type="hidden" name="tmpid" value="<?php echo $_REQUEST['tmpid'];?>"  />
                        <input type="hidden" name="id" id="id"  />
                        <input type="hidden" name="status" id="status"  />
                        <input type="hidden" name="defftask" id="defftask" value="show" />
                        <input type="hidden" name="del" id="del"  />
                        <input type="hidden" name="edit" id="edit"  />
                     </form>
                  </div>
               </div>
               <div class="container">
                  <div class="col-md-12"></div>
               </div>
               <br>
               <table style="width:100%" data-toggle="table" id="table-style"  data-row-style="rowStyle">
                  <thead>
                     <tr>
                        <th><div align="center">S.No.</div></th>                   
                        <th><div align="center">Company Name</div></th>
                        <th><div align="center">Contact Person Name</div></th>
                        <th><div align="center">Mobile</div></th>
                        <th><div align="center">Email-ID</div></th>
                        <th><div align="center">Address</div></th>
                        <th><div align="center">Type</div></th>
                         <th><div align="center">City</div></th>
                         	<?php if($_SESSION['department_id']!='1' && $_SESSION['department_id']!='6') {?>
                        <th width="9%"><div align="center">Action</div></th>
                        <?php }?>
                     </tr>
                  </thead>
                  <?php
                     if($results) {
                     	$countno = ($page-1)*$tpages;
                     	$i=0;
                     	foreach($results as $result){ 
                     	$i++;
                     	$countno++;
                     $sel_city = mysql_fetch_array(mysql_query("select * from city where id='".$result['city_id']."'"));
				
                     ?>
                  <tr>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $countno; ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $result['company_name'] ; ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $result['contact_person_name'] ; ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $result['mobile'] ; ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $result['email_id'] ; ?></div>
                     </td>
                     <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $result['address'] ; ?></div>
                     </td>
                    
                    <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $result['type'] ; ?></div>
                     </td>
                      <td class="<?php echo $class; ?>">
                        <div align="center"><?php echo $sel_city['city_name'] ; ?></div>
                     </td>
                     	<?php if($_SESSION['department_id']!='1' && $_SESSION['department_id']!='6') {?>
                     <td class="<?php echo $class; ?>">
                        <div align="center">
                           <a href="index.php?control=contractor_master&task=addnew&id=<?php echo $result['id']; ?>" style="cursor:pointer;" onclick="return confirm('Are you sure you want to Edit Record ?')" title="Edit"><img src="images/edit.png" alt="edit" title="Edit" /></a>
                           <?php if($result['status']=='1'){  ?>
                           <a href="index.php?control=contractor_master&task=status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')" title="Click to Inactive"><img src="images/backend/check.png" alt="check_de" /></a>
                           <?php } else { ?>
                           <a  href="index.php?control=contractor_master&task=status&status=1&id=<?php echo $result['id']; ?>" onclick="return confirm('Are you sure you want to Activate ?')" style="cursor:pointer;" title="Click to Active"><img src="images/backend/check_de.png" alt="check_de" /></a>
                           <?php } ?>
                        </div>
                     </td>
                     <?php } ?>
                  </tr>
                  <?php }  
                     } else{ ?>
                  <div>
                     <!-- <strong>Data not found.</strong>-->
                  </div>
                  <!-- <tbody><tr class="no-records-found"><td colspan="11">No matching records found</td></tr></tbody>-->
                  <?php } ?>
               </table>
            </div>
            <?php if(count($results)>0){ ?>
            <?php
               include("pagination.php");
               echo paginate_three($reload, $page, $tpages, $adjacents,$tdata); ?>
            <?php } ?>
         </div>
      </div>
   </div>
</div>
<div id="myModal" class="reveal-modal">
   <div id="view_popup"></div>
   <a class="close-reveal-modal">
      <!--<img src="assets/popup/images/close.png" width="23" height="23" />-->
   </a>
</div>

