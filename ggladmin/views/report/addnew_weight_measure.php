<?php foreach($results as $result) { }  ?>
<div class="col-md-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=report&task=show_weight_measure">New Contractor </a></li>
            <li class="active"> <?php if($result['id']!='') { ?> View <?php } else { ?>Add<?php } ?> New Weight Measurement </li>
         </ol>
      </div>
   </div>
   <!--/.row-->
</div>
<div class="col-md-12">
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="panel-heading">
            <?php if($result['id']!='') { ?> 
            <h3>Edit Weight Measurement </h3>
            <?php } else { ?>
            <u>
               <h3>Add New Weight Measurement </h3>
            </u>
            <?php }
               ?>  
         </div>
         <br>
         <form class="col-md-10 col-md-offset-1 form-horizontal" action="" method="post" role="form" enctype="multipart/form-data" >
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" >City</label>
                  <div class="col-sm-7">
                     <select name="city_id" id="city_id" class="form-control">
                        <option value="">Select</option>
                        <?php $this->Query("SELECT * FROM `city` WHERE `status`=1");
                        $cities = $this->fetchArray();
                        foreach ($cities as $city){ ?>
                        <option value="<?php echo $city['id']; ?>" <?php echo $result['city_id']==$city['id']?"selected":""; ?>><?php echo $city['city_name']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" for="new_crn">Station</label>
                  <div class="col-sm-7">
                     <select name="station_id" id="station_id" class="form-control">
                        <option value="">Select</option>
                        <?php $this->Query("SELECT * FROM `master_station` WHERE `status`=1");
                        $stations = $this->fetchArray();
                        foreach ($stations as $station){ ?>
                        <option value="<?php echo $station['id']; ?>" <?php echo $result['station_id']==$station['id']?"selected":""; ?>><?php echo $station['name']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" for="asset">Equipment</label>
                  <div class="col-sm-7">
                     <select name="equipment" id="equipment" class="form-control">
                        <option value="">Select</option>
                        <?php /*$this->Query("SELECT DISTINCT(`equipment`) FROM `weight_measurement` WHERE 1");
                        $equipment = $this->fetchArray();
                        foreach ($equipment as $equipment){ ?>
                        <option value="<?php echo $equipment['equipment']; ?>" <?php echo $result['equipment']==$equipment['equipment']?"selected":""; ?>><?php echo $equipment['equipment']; ?></option>
                        <?php }*/ ?>
                      <option value="Dispenser 1" <?php echo $result['equipment']=="Dispenser 1"?"selected":""; ?>>Dispenser 1</option>
                      <option value="Dispenser 2" <?php echo $result['equipment']=="Dispenser 2"?"selected":""; ?>>Dispenser 2</option>
                      <option value="Dispenser 3" <?php echo $result['equipment']=="Dispenser 3"?"selected":""; ?>>Dispenser 3</option>
                      <option value="Dispenser 4" <?php echo $result['equipment']=="Dispenser 4"?"selected":""; ?>>Dispenser 4</option>
                      <option value="Dispenser 5" <?php echo $result['equipment']=="Dispenser 5"?"selected":""; ?>>Dispenser 5</option>
                      <option value="Dispenser 6" <?php echo $result['equipment']=="Dispenser 6"?"selected":""; ?>>Dispenser 6</option>
                      <option value="Dispenser 7" <?php echo $result['equipment']=="Dispenser 7"?"selected":""; ?>>Dispenser 7</option>
                      <option value="Dispenser 8" <?php echo $result['equipment']=="Dispenser 8"?"selected":""; ?>>Dispenser 8</option>
                      <option value="Dispenser 9" <?php echo $result['equipment']=="Dispenser 9"?"selected":""; ?>>Dispenser 9</option>
                      <option value="Dispenser 10" <?php echo $result['equipment']=="Dispenser 10"?"selected":""; ?>>Dispenser 10</option>
                     </select>
                  </div>
               </div>
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand">Serial No</label>
                  <div class="col-sm-7">
                     <input type="text" id="serial_no" name="serial_no" value="<?php echo $result['serial_no']; ?>" class="form-control">
                  </div>
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" for="name">Vendor</label>
                  <div class="col-sm-7">
                     <input type="text" id="vendor" name="vendor" value="<?php echo $result['vendor']; ?>" class="form-control">
                  </div>
               </div>
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" for="address">Side</label>
                  <div class="col-sm-7">
                     <select name="side" id="side" class="form-control">
                        <option value="">Select</option>
                        <option value="A" <?php echo $result['side']=="A"?"selected":""; ?>>A</option>
                        <option value="B" <?php echo $result['side']=="B"?"selected":""; ?>>B</option>
                       
                     </select>              
                  </div>
               </div>
               <div class="form-group form-group-sm">
                  <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand"> W&M Done Date </label>
                  <div class="col-sm-7">
                    <input type="text" id="w_m_done_date" name="w_m_done_date" value="<?php echo $result['w_m_done_date']; ?>" class="form-control">
                  </div>
               </div>
               <div class="form-group form-group-sm">
                 <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand"> W&M Due Date </label>
                  <div class="col-sm-7">
                    <input type="text" id="w_m_due_date" name="w_m_due_date" value="<?php echo $result['w_m_due_date']; ?>" class="form-control">
                  </div>
               </div>
             </div>   
              <?php if($_SESSION['department_id']=='2' || $_SESSION['utype']=='Admin') { ?>
             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">             
               <div class="form-group form-group-sm">
                   <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand"> Verification fee details </label>
                  <div class="col-sm-7">
                    <input type="file" id="verification_fee" name="verification_fee" value="<?php echo $result['verification_fee']; ?>" > 
                  </div>
               </div>
               </div>  
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">   
               <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand"> Stamped and verified certificate </label>
                  <div class="col-sm-7">
                    <input type="file" id="stamped_verified_certificate" name="stamped_verified_certificate" value="<?php echo $result['stamped_verified_certificate']; ?>" >                 
                  </div>
               </div>
            </div>
			<?php } ?>
            <div class="clearfix"></div>
            <input type="hidden" name="employee_id" id="employee_id" value="<?php echo  $_SESSION['adminid'];?>" />
            <div class="col-md-12" align="center">
               <div class="form-group">
                  <button type="submit" class="btn btn-primary"><?php if($result[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
                        <input type="hidden" name="control" value="report"/>
      <input type="hidden" name="edit" value="1"/>
      <input type="hidden" name="task" value="save_weight_measure"/>
      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
               </div>
            </div>
      </div>

      </form>
   </div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>  
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
$('#w_m_due_date').datetimepicker({
   yearOffset:0,
   lang:'ch',
   timepicker:false,
   format:'d-m-Y',
   formatDate:'d-m-Y',
   //minDate:'-1970/01/02', // yesterday is minimum date
   //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#w_m_done_date').datetimepicker({
   yearOffset:0,
   lang:'ch',
   timepicker:false,
   format:'d-m-Y',
   formatDate:'d-m-Y',
   //minDate:'-1970/01/02', // yesterday is minimum date
   //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script> 
