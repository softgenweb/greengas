<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<!--<link rel="stylesheet" href="styles/alertmessage.css" />-->
<script type="text/javascript">
   setTimeout("closeMsg('closeid2')",5000);
   function closeMsg(clss) {
   		document.getElementById(clss).className = "clspop";
   	}
       
</script>
<script type="text/javascript" src="assets/editor/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script>
<!-- <script type="text/javascript" src="assets/multiselect/docs/js/prettify.js"></script>

<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                window.prettyPrint() && prettyPrint();
				$('#zone_id').multiselect({
            includeSelectAllOption: true
        });
            });
        </script> -->
<style type="text/css">
   .clspop {
   display:none;	
   }
   .darkbase_bg {
   display:block !important;
   }
</style>
<!--POPUP MESSAGE-->
<?php if(isset($_SESSION['error'])){?>
<div id="flashMessage" class="message">
   <div  class='darkbase_bg' id='closeid2'>
      <div class='alert_pink' >
         <a class='pop_close'> <img src="images/close.png" onclick="closeMsg('closeid2')" title="close" /> </a>
         <div class='pop_text <?php echo $_SESSION['errorclass']; ?>'>
            <!--warn_red-->
            <p style='color:#063;'><?php echo $_SESSION['error']; ?></p>
         </div>
      </div>
   </div>
</div>
<?php  
   unset($_SESSION['error']);
   unset($_SESSION['errorclass']);
   
   }?>
<!--POPUP MESSAGE CLOSE-->
<!-------------------------------Alert Message-------------------------------->    
<div class="col-sm-12">
   <div class="row">
      <div class="col-lg-12">
         <ol class="breadcrumb">
            <li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active"><a href="index.php?control=hrm_department">Masters</a></li>
            <li class="active">Company Master / <?php if($result['id']!='') { ?> Edit <?php } else { ?>Add<?php } ?> Company</li>
         </ol>
      </div>
   </div>
   <!--/.row-->
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="panel-heading">
                  <?php foreach($results as $result) { }  ?>
                  <?php if($result['id']!='') { ?> 
                  <u>
                     <h3>Edit Company</h3>
                  </u>
                  <?php } else { ?>
                  <u>
                     <h3>Add New Company</h3>
                  </u>
                  <?php } ?>
               </div>
               <br>
			<div class="panel-body">
			      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
			         <div class="row col-md-12">
			            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Name</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control"  required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Email</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="email" value="<?php echo $result['email']; ?>" id="email" name="email" class="form-control" >                        
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Helpline No.</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['helpline_no']; ?>" id="helpline_no" name="helpline_no" class="form-control"  required="">                         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Complain No.</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['complain_no']; ?>" id="complain_no" name="complain_no" class="form-control"  required="">                         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Country</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <select name="country" id="country" class="form-control">
			                     	<option value="">Select City</option>
			                     	<?php $sql= mysql_query("SELECT * FROM `country` WHERE 1");
			                     	while($country = mysql_fetch_array($sql)){ ?>
			                     	<option value="<?php echo $country['id']; ?>" <?php echo $result['country']==$country['id']?'selected':'' ?>><?php echo $country['country_name']; ?></option>
			                     	<?php } ?>
			                     </select>        
			                     <!-- <input type="text" value="<?php echo $result['branch']; ?>" id="branch" name="branch" class="form-control"  required="" >          -->
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>State</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <select name="state" id="state" class="form-control">
			                     	<option value="">Select City</option>
			                     	<?php $sql= mysql_query("SELECT * FROM `state` WHERE `country_id`='".$result['country']."'");
			                     	while($state = mysql_fetch_array($sql)){ ?>
			                     	<option value="<?php echo $state['id']; ?>" <?php echo $result['state']==$state['id']?'selected':'' ?>><?php echo $state['state_name']; ?></option>
			                     	<?php } ?>
			                     </select>        
			                     <!-- <input type="text" value="<?php echo $result['branch']; ?>" id="branch" name="branch" class="form-control"  required="" >          -->
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>City</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <select name="branch_city" id="branch_city" class="form-control">
			                     	<option value="">Select City</option>
			                     	<?php $sql= mysql_query("SELECT * FROM `city` WHERE `state_id`='".$result['state']."'");
			                     	while($city = mysql_fetch_array($sql)){ ?>
			                     	<option value="<?php echo $city['id']; ?>" <?php echo $result['branch_city']==$city['id']?'selected':'' ?>><?php echo $city['city_name']; ?></option>
			                     	<?php } ?>
			                     </select>        
			                     <!-- <input type="text" value="<?php echo $result['branch']; ?>" id="branch" name="branch" class="form-control"  required="" >          -->
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Address-1</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                  	<input type="text" value="<?php echo $result['address1']; ?>" id="address1" name="address1" class="form-control" required="">
			                           
			                     <!-- <textarea id="address" name="address" class="form-control"  required=""><?php echo $result['address']; ?></textarea>          -->
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Address-2</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                  	<input type="text" value="<?php echo $result['address2']; ?>" id="address2" name="address2" class="form-control" required="">
			                           
			                     <!-- <textarea id="address" name="address" class="form-control"  required=""><?php echo $result['address']; ?></textarea>          -->
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>PIN</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['pin_no']; ?>" id="pin_no" name="pin_no" class="form-control" maxlength="6" required="">
			                     
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>GST No.</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" placeholder="XXABCDEXXXXFXZX" value="<?php echo $result['gst_no']; ?>" id="gst_no" name="gst_no" class="form-control" pattern="[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[1-9a-zA-Z]{1}" maxlength="15" required="">
			                     
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>GSTIN TYPE</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['gstin_type']; ?>"  id="gstin_type" name="gstin_type" class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>TIN No.</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['tin_no']; ?>" id="tin_no" name="tin_no" class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Website</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['website']; ?>" id="website" name="website" class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Branch Code</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['branch_code']; ?>" id="branch_code" name="branch_code" maxlength="4" class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Network Tarrif</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['network_tarrif']; ?>" id="network_tarrif" name="network_tarrif"  class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>PNG Rate <br><code>(upto 45SCM)</code></label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['png_rate1']; ?>" id="png_rate1" name="png_rate1" maxlength="10" class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>PNG Rate <br><code>(above 45SCM)</code></label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="text" value="<?php echo $result['png_rate2']; ?>" id="png_rate2" name="png_rate2" maxlength="10" class="form-control" required="">         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Signature</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <input type="file" value="<?php echo $result['signature']; ?>" id="signature" name="signature"  >         
			                     <input type="hidden" value="<?php echo $result['signature']; ?>" id="signature_old" name="signature_old"  >         
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-3">
			                  <div class="form-group center_text">
			                     <label>Terms & Conditions</label>
			                  </div>
			               </div>
			               <div class="col-md-6">
			                  <div class="form-group">
			                     <textarea class="ckeditor" name="terms_condition" id="terms_condition" ><?php echo $result['terms_condition']; ?></textarea>        
			                  </div>
			               </div>
			               <div class="clearfix"></div>
			               <div class="col-md-9 col-sm-8 col-xs-12">
			                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update'; }else{ echo 'Submit';}?>"></center>
			               </div>
			               <input type="hidden" name="control" value="company_master"/>
			               <input type="hidden" name="edit" value="1"/>
			               <input type="hidden" name="task" value="save"/>
			               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
			            </div>
			         </div>
			         <!--account details ends here-->
			      </form>
			   </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
	$('#country').change(function(){
		id = $(this).val();
		$.get( "script/popup_scripts/state.php?id="+id, function( data ) {
      
      	$("#state").html(data);
      });
	});

	$('#state').change(function(){
		id = $(this).val();
		$.get( "script/popup_scripts/city.php?id="+id, function( data ) {
      
      	$("#branch_city").html(data);
      });
	});
</script>

