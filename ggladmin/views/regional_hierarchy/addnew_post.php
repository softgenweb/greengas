<?php session_start(); ?>

<div class="col-md-12" style="margin-top:7%;">
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                    <u><h3>  <?php foreach($results as $result) { }  ?>
                  <?php if($result['id']!='') { ?> <u><h3>Edit Designation</h3></u> <?php } else { ?><u><h3>Add New Designation</h3></u><?php } ?></h3></u>
                    </div>
                    <br>
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

					<div class="col-md-2">
                    <div class="form-group" style="margin-top:5%;">
                   Designation Name:
                    </div></div>
                    <div class="col-md-4"><div class="form-group">
          <input type="text" class="form-control" name="post_name" id="post" placeholder="Enter Designation Name" value="<?php echo $result['post_name']; ?>"/>
          <span id="msgname" style="color:red;"></span>
                    </div></div>
                    
                   
                    
                  <div class="col-md-12" align="center"><div class="form-group">
                <button type="submit" class="btn btn-primary"><?php if($result[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
         <!--<button type="submit" class="btn btn-primary">Send</button>-->
                    </div></div>
                    
                      <input type="hidden" name="control" value="regional_hierarchy"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save_post"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="student_id" id="student_id" value="<?php echo $_SESSION['student_id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                      

		</form>

                        </div>
					    </div>			
					    </div>


<script type="text/javascript">
 
  function isEmail(text)
{
	
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	//"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
	var regex = new RegExp( pattern );
	return regex.test(text);
	
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}

</script>
<script type="text/javascript">
function validation()
{
	var chk=1;
if(document.getElementById('email').value == '') { 
	document.getElementById('msgemail').innerHTML ="*Required field.";
	chk=0;
	}else if(!isEmail(document.getElementById('email').value)){ 
	document.getElementById('msgemail').innerHTML ="*Must be email-id only.";
	chk=0;	
    }else {
	document.getElementById('msgemail').innerHTML = "";
	}
	
	if(document.getElementById('name').value == '') { 
	document.getElementById('msgname').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgname').innerHTML = "";
	}
	
	if(document.getElementById('subject').value == '') { 
	document.getElementById('msgsubject').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgsubject').innerHTML = "";
	}
	
	if(document.getElementById('message').value == '') { 
	document.getElementById('msgmessage').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgmessage').innerHTML = "";
	}
	
	if(document.getElementById('mobile').value == '') { 
	document.getElementById('msgmobile').innerHTML ="*Required field.";
    chk=0;
	}else if(!numeric(document.getElementById('mobile').value)){ 
	document.getElementById('msgmobile').innerHTML ="*Must be numeric only.";
	chk=0;	
    }
	else {
	document.getElementById('msgmobile').innerHTML = "";
	}
	
	
	if(document.getElementById('filename').value == '') {
	chk = 0;
	document.getElementById('msgfilename').innerHTML ="This field is required";	
	}else {
	var image = document.getElementById('filename').value;
	var imagefzipLength = image.length;
	var imagefzipDot = image.lastIndexOf(".");
	var imagefzipType = image.substring(imagefzipDot,imagefzipLength);
	if(image) {
		
			if((imagefzipType==".jpg")||(imagefzipType==".jpeg")||(imagefzipType==".gif")||(imagefzipType==".png")||(imagefzipType==".doc")||(imagefzipType==".docx")) {
				   document.getElementById('msgfilename').innerHTML = "";
				   
				      var fsize = $('#filename')[0].files[0].size; //get file size
				if(fsize>102400) 
				{   alert("Upload Max 100Kb File");
					document.getElementById('msgfilename').innerHTML = "Please reduce the size of your file using an image editor."; 
					chk =0;
				}
			}
			else {	chk = 0;
				 document.getElementById('msgfilename').innerHTML = "Invalid file format only (jpg,gif,png,doc)"; 
			}
		 }
	}
	
	
	if(chk) {	
		return true;
		}
		else {
		return false;		
		}	
}

</script>
