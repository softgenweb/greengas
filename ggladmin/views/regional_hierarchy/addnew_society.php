<?php session_start(); ?>
<div class="col-md-12">
        <div class="row">
			<div class="col-lg-12">            
			   <ol class="breadcrumb">    
				<li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=regional_hierarchy&task=show_society">Society</a></li>
                <li class="active">Add New Society</li>
			</ol>
			</div>
		</div><!--/.row-->
      </div>  
<div class="col-md-12" >
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                    <u><h3>  <?php foreach($results as $result) { }  ?>
                  <?php if($result['id']!='') { ?> <u><h3>Edit Society</h3></u> <?php } else { ?><u><h3>Add New Society</h3></u><?php } ?></h3></u>
                    </div>
                    <br>
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

			<div class="col-md-6 col-md-offset-3">

            <div class="col-md-4">
            <div class="form-group" >
            Country Name:
            </div></div>
            <div class="col-md-8"><div class="form-group">
			<select name="country_id" id="country_id" class="form-control" onchange='state_change(this.value);'>
			<option value="">---Select---</option>
			<?php $sel=mysql_query("select * from country");
			while($select=mysql_fetch_array($sel))
			{ ?>
			<option value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
			<?php } ?>
			</select>
			<span id="msgcountry_name" style="color:red;"></span>
			</div></div>


			<div class="col-md-4">
			<div class="form-group" >
			State Name:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			
			<select name="state_id" id="state_id" class="form-control" onchange='city_change(this.value);'>
			<option value="">---Select State---</option>
			<?php $sel=mysql_query("select * from state where country_id='".$result['country_id']."' and status=1");
			while($select=mysql_fetch_array($sel))
			{ ?>
			<option value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
			<?php } ?>
			</select>
			<span id="msgname" style="color:red;"></span>
			</div></div>

                    
            <div class="col-md-4">
			<div class="form-group" >
			City Name:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<select class="form-control" id="txtcity" name="city_id" onchange='area_change(this.value);'>
			<option value="">--Select City--</option>
			<?php
			
			 $projectquery11=mysql_query("SELECT * FROM  city where state_id='".$result['state_id']."'  and status=1");
			 
			 while($projectdata11=mysql_fetch_array($projectquery11))
			 {
			?>
            <option value="<?php echo $projectdata11['id']; ?>" <?php if($result['city_id']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['city_name']; ?></option>
            
				<?php }
				?>
           	 </select>
             <span id="msgname" style="color:red;"></span>
			</div></div>
            
            <div class="col-md-4">
			<div class="form-group" >
			Area Name:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<select class="form-control" id="txtarea" name="area_id">
			<option value="">--Select Area--</option>
			<?php			
			 $projectquery11=mysql_query("SELECT * FROM  area where city_id='".$result['city_id']."'  and status=1");
			 
			 while($projectdata11=mysql_fetch_array($projectquery11))
			 {
			?>
            <option value="<?php echo $projectdata11['id']; ?>" <?php if($result['area_id']==$projectdata11['id']) {?> selected="selected" <?php } ?>><?php echo $projectdata11['area_name']; ?></option>
            
				<?php }
				?>
           	 </select>
			<span id="msgname" style="color:red;"></span>
			</div></div>
                    
                   	<div class="col-md-4">
                    <div class="form-group" >
                   Society Name:
                    </div></div>
                    <div class="col-md-8"><div class="form-group">
          <input type="text" class="form-control" name="society_name" id="society_name"  value="<?php echo $result['society_name']; ?>"/>
          <span id="msgname" style="color:red;"></span>
                    </div></div>
                    
                    
                  
            <div class="col-md-4">
			<div class="form-group" >
			Owner Type:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<select class="form-control" id="owner_type" name="owner_type" required>
			<option value="">--Select--</option>
	<option value="Only Owner" <?php if($result['owner_type']=="Only Owner") {?> selected="selected" <?php } ?>>Only Owner</option>
    <option value="Owner/Tenant" <?php if($result['owner_type']=="Owner/Tenant") {?> selected="selected" <?php } ?>>Owner/Tenant</option>
		 </select>
			<span id="msgowner_type" style="color:red;"></span>
			</div></div>   
                     
            <div class="col-md-4">
			<div class="form-group" >
			Start Date:
			</div></div>
			<div class="col-md-8"><div class="form-group">
               <input class="form-control" type="text" name="start_date" id="start_date" placeholder="Start Date" readonly=""  value="<?php echo $result['start_date']; ?>">
			</div></div>  
            
                    
            <div class="col-md-4">
			<div class="form-group" >
			Close Date:
			</div></div>
			<div class="col-md-8"><div class="form-group">
             <?php if($_SESSION['department_id']!='3'){?>   
               <input class="form-control" type="text" name="end_date" id="end_date" placeholder="Close Date" readonly=""  value="<?php echo $result['end_date']; ?>">
               <?php } else { ?>
               <input class="form-control" type="text" name="end_date" readonly=""  value="<?php echo $result['end_date']; ?>">
               <?php } ?>
			</div></div>   
          <!------------------------------------------For Marketing Login----------------------------------------->           
         <?php if($_SESSION['department_id']=='3'){?>   
            <div class="col-md-4">
			<div class="form-group" >
			End Date:
			</div></div>
			<div class="col-md-8"><div class="form-group">
               <input class="form-control" type="text" name="marketing_end_date" id="marketing_end_date" placeholder="Close Date" readonly=""  value="<?php echo $result['end_date']; ?>">
			</div></div> 
            <?php   } ?>   
            
        <!------------------------------------------For Marketing Login----------------------------------------->
                    
                  <div class="col-md-12" align="center"><div class="form-group">
                <button type="submit" class="btn btn-primary"><?php if($result[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
         <!--<button type="submit" class="btn btn-primary">Send</button>-->
                    </div></div>
                    
               </div>     
                    
                    
                      <input type="hidden" name="control" value="regional_hierarchy"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save_society"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                      

		</form>
   

                        </div>
					    </div>			
					    </div>
                        
 
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>  
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
   <script>
$('#start_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#end_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

$('#marketing_end_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

</script>                     
                                     
          


<script type="text/javascript">
 
  function isEmail(text)
{
	
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	//"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
	var regex = new RegExp( pattern );
	return regex.test(text);
	
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}

</script>

  
  
  
    

  
<script type="text/javascript">
function validation()
{
	var chk=1;
if(document.getElementById('email').value == '') { 
	document.getElementById('msgemail').innerHTML ="*Required field.";
	chk=0;
	}else if(!isEmail(document.getElementById('email').value)){ 
	document.getElementById('msgemail').innerHTML ="*Must be email-id only.";
	chk=0;	
    }else {
	document.getElementById('msgemail').innerHTML = "";
	}
	
	if(document.getElementById('name').value == '') { 
	document.getElementById('msgname').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgname').innerHTML = "";
	}
	
	if(document.getElementById('subject').value == '') { 
	document.getElementById('msgsubject').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgsubject').innerHTML = "";
	}
	
	if(document.getElementById('message').value == '') { 
	document.getElementById('msgmessage').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgmessage').innerHTML = "";
	}
	
	if(document.getElementById('mobile').value == '') { 
	document.getElementById('msgmobile').innerHTML ="*Required field.";
    chk=0;
	}else if(!numeric(document.getElementById('mobile').value)){ 
	document.getElementById('msgmobile').innerHTML ="*Must be numeric only.";
	chk=0;	
    }
	else {
	document.getElementById('msgmobile').innerHTML = "";
	}
	
	
	if(document.getElementById('filename').value == '') {
	chk = 0;
	document.getElementById('msgfilename').innerHTML ="This field is required";	
	}else {
	var image = document.getElementById('filename').value;
	var imagefzipLength = image.length;
	var imagefzipDot = image.lastIndexOf(".");
	var imagefzipType = image.substring(imagefzipDot,imagefzipLength);
	if(image) {
		
			if((imagefzipType==".jpg")||(imagefzipType==".jpeg")||(imagefzipType==".gif")||(imagefzipType==".png")||(imagefzipType==".doc")||(imagefzipType==".docx")) {
				   document.getElementById('msgfilename').innerHTML = "";
				   
				      var fsize = $('#filename')[0].files[0].size; //get file size
				if(fsize>102400) 
				{   alert("Upload Max 100Kb File");
					document.getElementById('msgfilename').innerHTML = "Please reduce the size of your file using an image editor."; 
					chk =0;
				}
			}
			else {	chk = 0;
				 document.getElementById('msgfilename').innerHTML = "Invalid file format only (jpg,gif,png,doc)"; 
			}
		 }
	}
	
	
	if(chk) {	
		return true;
		}
		else {
		return false;		
		}	
}

</script>

