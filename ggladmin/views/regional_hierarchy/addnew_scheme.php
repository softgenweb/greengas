<?php session_start(); ?>
<div class="col-md-12">
        <div class="row">
			<div class="col-lg-12">            
			   <ol class="breadcrumb">    
				<li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=regional_hierarchy&task=show_scheme">Scheme</a></li>
                <li class="active">Add New Scheme</li>
			</ol>
			</div>
		</div><!--/.row-->
      </div>  
<div class="col-md-12" >
				<div class="panel panel-default">
					<div class="panel-body">
                    <div class="panel-heading">
                    <u><h3>  <?php foreach($results as $result) { }  ?>
                  <?php if($result['id']!='') { ?> <u><h3>Edit Scheme</h3></u> <?php } else { ?><u><h3>Add New Scheme</h3></u><?php } ?></h3></u>
                    </div>
                    <br>
		<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">

			<div class="col-md-6 col-md-offset-3">

            <div class="col-md-4">
            <div class="form-group" >
           Scheme Type:
            </div></div>
            <div class="col-md-8"><div class="form-group">
			<input type="text" name="scheme_type" id="scheme_type" class="form-control" value="<?php echo $result['scheme_type']; ?>" required>
			
			<span id="msgscheme_type" style="color:red;"></span>
			</div></div>


			<div class="col-md-4">
			<div class="form-group" >
			Security Deposit:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="security_deposite" id="security_deposite" class="form-control" value="<?php echo $result['security_deposite']; ?>" required>		
			<span id="msgregistration_fee" style="color:red;"></span>
			</div></div>
            
			<div class="col-md-4">
			<div class="form-group" >
			Gas Supply:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="gas_supply" id="gas_supply" class="form-control" value="<?php echo $result['gas_supply']; ?>" required>		
			<span id="msgregistration_fee" style="color:red;"></span>
			</div></div>
			<div class="col-md-4">
			<div class="form-group" >
			Non-Refund/Registration:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="non_refund" id="non_refund" class="form-control" value="<?php echo $result['non_refund']; ?>" required>		
			<span id="msgregistration_fee" style="color:red;"></span>
			</div></div>
            
			<div class="col-md-4">
			<div class="form-group" >
			Total Amount:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="registration_fee" id="registration_fee" class="form-control" value="<?php echo $result['registration_fee']; ?>" required>		
			<span id="msgregistration_fee" style="color:red;"></span>
			</div></div> 
            
            
			<div class="col-md-4">
			<div class="form-group" >
			MOU Type:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<select class="form-control" id="mou" name="mou" onchange="mou_change(this.value);" required="">
			<option value="">Select</option>
			     <option value="1" <?php if($result['mou']==1){ echo "Selected";} ?>>Yes</option>
                 <option value="2" <?php if($result['mou']==2){ echo "Selected";} ?>>No</option>
            </select>
			</div></div>
            
            <div id="mou_data" style="display:<?php if($result['mou']==1){echo 'block';}else{ echo "none";} ?>;">
            
			<div class="col-md-4">
			<div class="form-group" >
			MOU Value Deducted Against Registration:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="mou_value_deducted_against_registration" id="mou_value_deducted_against_registration" class="form-control" value="<?php echo $result['mou_value_deducted_against_registration']; ?>" >	
			</div></div>
            		<div class="clearfix"></div>
                    
                    
            <div class="col-md-4">
			<div class="form-group" >
			Value to be paid by customer:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="mou_customer_paid" id="mou_customer_paid" class="form-control" value="<?php echo $result['mou_customer_paid']; ?>" >	
			</div></div>
            
            
			<div class="col-md-4">
			<div class="form-group" >
			MOU Total Amount (Total Payment Received):
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="mou_total_amount" id="mou_total_amount" class="form-control" value="<?php echo $result['mou_total_amount']; ?>" >	
			</div></div>
            <div class="clearfix"></div>
            
            
			<div class="col-md-4">
			<div class="form-group" >
			MOU Remaining Amount:
			</div></div>
			<div class="col-md-8"><div class="form-group">
			<input type="text" name="mou_remaining_amount" id="mou_remaining_amount" class="form-control" value="<?php echo $result['mou_remaining_amount']; ?>" >	
			</div></div>
            </div>
            
            <div class="col-md-4">
            <div class="form-group" >
            EMI:
            </div></div>
            <div class="col-md-8"><div class="form-group">
            <select class="form-control" name="emi" id="emi" onchange="emi_option(this.value);" required>
            <option value="">Select EMI</option>
            <option value="Yes" <?php if($result['emi']=='Yes') { ?> selected="selected" <?php } ?>>Yes</option>
            <option value="No"  <?php if($result['emi']=='No') { ?> selected="selected" <?php } ?>>No</option>
            </select>			
            <span id="msgemi" style="color:red;"></span>
            </div></div>
           
           	<div id="emi_grp" style="display:<?php if($result['emi']=='No') { echo "none"; } else { echo "bolck";} ?>">  
            
                <div class="col-md-4">
                <div class="form-group" >
                EMI Amount:
                </div></div>
                <div class="col-md-8"><div class="form-group">
                <input type="text" name="emi_amount" id="emi_amount" class="form-control" value="<?php echo $result['emi_amount']; ?>" onkeypress="return isNumber(event)">
                <span id="msgemi" style="color:red;"></span>
                </div></div>
            
                <div class="col-md-4">
                <div class="form-group" >
                Number of EMI:
                </div></div>
                <div class="col-md-8"><div class="form-group">
                <input type="text" name="emi_installment" id="emi_installment" class="form-control" value="<?php echo $result['emi_installment']; ?>" onkeypress="return isNumber(event)">
                <span id="msgemi" style="color:red;"></span>
                </div></div>
              
                
            </div>
            <div class="col-md-4">
			<div class="form-group" >
			Rental:
			</div></div>
			<div class="col-md-8"><div class="form-group">
            <input type="text" name="rental" id="rental" class="form-control" value="<?php echo $result['rental']; ?>" required>
			
			<span id="msgrental" style="color:red;"></span>
			</div></div>
                    
           <!--   <div class="col-md-4">
			<div class="form-group" >
			Remark:
			</div></div>
			<div class="col-md-8"><div class="form-group">
           
           <textarea name="remark" id="remark" class="form-control"><?php echo $result['remark']; ?></textarea>
			
			<span id="msgrental" style="color:red;"></span>
			</div></div>-->
                    
                    
                    
                  
            
			   
                    
                    
                  <div class="col-md-12" align="center"><div class="form-group">
                <button type="submit" class="btn btn-primary"><?php if($result[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
         <!--<button type="submit" class="btn btn-primary">Send</button>-->
                    </div></div>
                    
               </div>     
                    
                    
                      <input type="hidden" name="control" value="regional_hierarchy"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save_scheme"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
                      

		</form>

                        </div>
					    </div>			
					    </div>


<script type="text/javascript">
 
  function isEmail(text)
{
	
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	//"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.\+[\\w]+[\\w]$";
	var regex = new RegExp( pattern );
	return regex.test(text);
	
}

function numeric(sText)
{
 var ValidChars = "0123456789,.";
 var IsNumber=true;	
 for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {			 
		 IsNumber = false;
         }
      }
  	 return IsNumber;
}

</script>

<script type="text/javascript">

function mou_change(str){
	if(str == 1) { 
	document.getElementById('mou_data').style.display = "block";
	}else{
	document.getElementById('mou_data').style.display = "none";	
	document.getElementById('mou_value_deducted_against_registration').value = "";	
	document.getElementById('mou_customer_paid').value = "";	
	document.getElementById('mou_total_amount').value = "";		
	document.getElementById('mou_remaining_amount').value = "";	
	}
}


function validation(){
	var chk=1;
if(document.getElementById('email').value == '') { 
	document.getElementById('msgemail').innerHTML ="*Required field.";
	chk=0;
	}else if(!isEmail(document.getElementById('email').value)){ 
	document.getElementById('msgemail').innerHTML ="*Must be email-id only.";
	chk=0;	
    }else {
	document.getElementById('msgemail').innerHTML = "";
	}
	
	if(document.getElementById('name').value == '') { 
	document.getElementById('msgname').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgname').innerHTML = "";
	}
	
	if(document.getElementById('subject').value == '') { 
	document.getElementById('msgsubject').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgsubject').innerHTML = "";
	}
	
	if(document.getElementById('message').value == '') { 
	document.getElementById('msgmessage').innerHTML ="*Required field.";
	chk=0;
	}
	else {
	document.getElementById('msgmessage').innerHTML = "";
	}
	
	if(document.getElementById('mobile').value == '') { 
	document.getElementById('msgmobile').innerHTML ="*Required field.";
    chk=0;
	}else if(!numeric(document.getElementById('mobile').value)){ 
	document.getElementById('msgmobile').innerHTML ="*Must be numeric only.";
	chk=0;	
    }
	else {
	document.getElementById('msgmobile').innerHTML = "";
	}
	
	
	if(document.getElementById('filename').value == '') {
	chk = 0;
	document.getElementById('msgfilename').innerHTML ="This field is required";	
	}else {
	var image = document.getElementById('filename').value;
	var imagefzipLength = image.length;
	var imagefzipDot = image.lastIndexOf(".");
	var imagefzipType = image.substring(imagefzipDot,imagefzipLength);
	if(image) {
		
			if((imagefzipType==".jpg")||(imagefzipType==".jpeg")||(imagefzipType==".gif")||(imagefzipType==".png")||(imagefzipType==".doc")||(imagefzipType==".docx")) {
				   document.getElementById('msgfilename').innerHTML = "";
				   
				      var fsize = $('#filename')[0].files[0].size; //get file size
				if(fsize>102400) 
				{   alert("Upload Max 100Kb File");
					document.getElementById('msgfilename').innerHTML = "Please reduce the size of your file using an image editor."; 
					chk =0;
				}
			}
			else {	chk = 0;
				 document.getElementById('msgfilename').innerHTML = "Invalid file format only (jpg,gif,png,doc)"; 
			}
		 }
	}
	
	
	if(chk) {	
		return true;
		}
		else {
		return false;		
		}	
}

function emi_option(str)
{
	if(str=='Yes')
	{
		document.getElementById("emi_grp").style.display="block";
		document.getElementById("emi_installment").required = true;
		document.getElementById("emi_amount").required = true;
	}
	else
	{
		document.getElementById("emi_grp").style.display="none";
		document.getElementById("emi_installment").required = false;
		document.getElementById("emi_amount").required = false;
		document.getElementById("emi_installment").value ='';
		document.getElementById("emi_amount").value = '';
	}
	
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
</script>
