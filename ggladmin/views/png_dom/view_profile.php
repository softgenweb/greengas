<?php session_start(); ?>
   <?php foreach($results as $result) { }  ?>
<div class="col-md-12">
        <div class="row">
			<div class="col-lg-12">            
			   <ol class="breadcrumb">    
				<li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=png_dom&task=show">New PNG </a></li>
                <li class="active"> <?php if($result['id']!='') { ?> View <?php } else { ?>Add<?php } ?> New PNG </li>
			</ol>
			</div>
		</div><!--/.row-->
      </div>  
<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
                        <div class="panel-heading">
                        
                        <?php if($result['id']!='') { ?> <h3>View New PNG Connection Detail </h3> <?php } else { ?><u><h3>Add New PNG Connection</h3></u><?php }
                        
                        ?>  
                        </div>
                    <br>
		
           <form class="form-horizontal" action="" method="post" role="form" enctype="multipart/form-data" >
        
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          
             <!-- <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="form_id">Form No.</label>
                    <div class="col-sm-7">
                      <input type="text" id="form_id" style="text-align:left;" class="form-control" name="form_id" value="<?php echo $result['form_id']; ?>"  required>
                    </div>
                  </div>
                  
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="sn">SN</label>
                <div class="col-sm-7">
                  <input type="text" id="sn" style="text-align:left;" class="form-control" name="sn"  value="<?php echo $result['sn']; ?>"  >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="interim_crn">Interim CRN</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="interim_crn" name="interim_crn" value="<?php echo $result['interim_crn']; ?>" >
                </div>
              </div> 
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="interim_crn">Form No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="form_no" name="form_no" value="<?php echo $result['form_no']; ?>" readonly="readonly">
                </div>
              </div>-->
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="new_crn">Present CRN</label>
                <div class="col-sm-7">
                <input type="text" class="form-control"  id="present_crn" name="present_crn" value="<?php echo $result['present_crn']; ?>" readonly="readonly">
                </div>
              </div> 
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="asset">Asset</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="asset" name="asset"    value="<?php echo $result['asset']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="name">Name</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="name" name="name" pattern="[a-zA-Z.\s]+"  value="<?php echo $result['name']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="address">Address</label>
                <div class="col-sm-7">
                <textarea class="form-control" id="address" name="address" readonly="readonly"><?php echo $result['address']; ?></textarea>                
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand">Block/Sec/Khand</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="block_sec_khand" name="block_sec_khand" value="<?php echo $result['block_sec_khand']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="apartment_society">Apartment/Society</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="apartment_society" name="apartment_society" value="<?php echo $result['apartment_society']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mou_flats_individual">Mou/Flats/Individual</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mou_flats_individual" name="mou_flats_individual" value="<?php echo $result['mou_flats_individual']; ?>" readonly="readonly">
                </div>
              </div>
           
              <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="charge_uncharge_area">Charge/Uuncharge/Area</label>
                    <div class="col-sm-7">
                      <input type="text" id="charge_uncharge_area" style="text-align:left;" class="form-control" name="charge_uncharge_area" value="<?php echo $result['charge_uncharge_area']; ?>" readonly="readonly">
                    </div>
                  </div>
                  
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="area">Area</label>
                <div class="col-sm-7">
                  <input type="text" id="area" style="text-align:left;" class="form-control" name="area" value="<?php echo $result['area']; ?>"  readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mobile">Mobile</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mobile" name="mobile"  pattern="[6789][0-9]{9}" maxlength="10" value="<?php echo $result['mobile']; ?>" readonly="readonly">
                </div>
              </div> 
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="landline">Landline</label>
                <div class="col-sm-7">
                 <input type="text" class="form-control"  id="landline" name="landline" maxlength="15" value="<?php echo $result['landline']; ?>" readonly="readonly">
                </div>
              </div>   
                         
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="email">Email</label>
                <div class="col-sm-7">
                  <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"  value="<?php echo $result['email']; ?>" readonly="readonly">
                </div>
              </div>
              
               <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="fnl_status_z">Final Status Z</label>
                    <div class="col-sm-7">
                      <input type="text" id="fnl_status_z" style="text-align:left;" class="form-control" name="fnl_status_z" value="<?php echo $result['fnl_status_z']; ?>"  readonly="readonly">
                    </div>
                  </div>
                  
              
                
              <!--<div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="registration_date">Registration Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="registration_date" name="registration_date" value="<?php echo $result['registration_date']; ?>"  readonly="readonly">
                </div>
              </div>
            
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="status">Master Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="master_status" name="master_status" value="<?php echo $result['master_status']; ?>" >
                </div>
              </div>-->
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="rfc_fnl_z">RFC Final Z</label>
                <div class="col-sm-7">
                  <input type="text" id="rfc_fnl_z" style="text-align:left;" class="form-control" name="rfc_fnl_z" value="<?php echo $result['rfc_fnl_z']; ?>"  readonly="readonly">
                </div>
              </div>
              
               
              
          </div>
         
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
         
         		 
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="rfc_date_1st">RFC Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="rfc_date" name="rfc_date" value="<?php echo $result['rfc_date']; ?>" readonly="readonly">
                </div>
              </div>
                <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mktg_update">Marketing Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mktg_status" name="mktg_status" value="<?php echo $result['mktg_status']; ?>" readonly="readonly">
                </div>
              </div> 
            <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="ninety_days_pendency_sch">90 days pendency Sch</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="ninety_days_pendency_sch" name="ninety_days_pendency_sch" value="<?php echo $result['ninety_days_pendency_sch']; ?>" readonly="readonly">
                </div>
              </div>
                
                 <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_gi_contractor_alloted">Project GI Contractor Alloted</label>
                <div class="col-sm-7">
                 
                    <?php $sql_cont = mysql_fetch_array(mysql_query("select * from contractor_master where id ='".$result['proj_gi_contractor_alloted']."'")); ?>
                    
					  <input type="text" class="form-control" id="proj_gi_contractor_alloted" name="proj_gi_contractor_alloted" value="<?php echo $sql_cont['company_name']; ?>" readonly="readonly">
                      
                  
                </div>
              </div>  
             <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="meter_no">Meter No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="meter_no" name="meter_no" value="<?php echo $result['meter_no']; ?>" readonly="readonly">
                </div>
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="initial_reading">Initial Reading</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="initial_reading" name="initial_reading" value="<?php echo $result['initial_reading']; ?>" readonly="readonly">
                </div>
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_ng_dt">Project NG Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="proj_ng_dt" name="proj_ng_dt" value="<?php echo $result['proj_ng_dt']; ?>" readonly="readonly">
                </div>
              </div>
               
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_rfc_dt">JMR No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="jmr_no" name="jmr_no"  value="<?php echo $result['jmr_no']; ?>" readonly="readonly">
                </div>
              </div>
               <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">Project Remarks</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="proj_remarks" name="proj_remarks" value="<?php echo $result['proj_remarks']; ?>" readonly="readonly">
                </div>
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_rfc_dt">Marketing RFA Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="proj_rfc_dt" name="proj_rfc_dt"  value="<?php echo $result['proj_rfc_dt']; ?>" readonly="readonly">
                </div>
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mktg_update">Marketing Remark</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mktg_remark" name="mktg_remark" value="<?php echo $result['mktg_remark']; ?>" readonly="readonly">
                </div>
              </div>
               <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">NG Progress fy1920</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="ng_progress_fy1920" name="ng_progress_fy1920" value="<?php echo $result['ng_progress_fy1920']; ?>" readonly="readonly">
                </div>
              </div>
               <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">Registration Index</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="registration_index" name="registration_index" value="<?php echo $result['registration_index']; ?>" readonly="readonly">
                </div>
              </div> 
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">Overall Comments</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="overall_comments" name="overall_comments" value="<?php echo $result['overall_comments']; ?>" readonly="readonly">
                </div>
              </div>
             <!-- <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="2nd_meter_no">2<sup>nd</sup> Meter No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="2nd_meter_no" name="2nd_meter_no"  value="<?php echo $result['2nd_meter_no']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="sap_meter_no">SAP Meter No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="sap_meter_no" name="sap_meter_no" value="<?php echo $result['sap_meter_no']; ?>" >
                </div>
              </div>
              
                            
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="2nd_initial_reading">2<sup>nd</sup> Initial Reading</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="2nd_initial_reading" name="2nd_initial_reading" value="<?php echo $result['2nd_initial_reading']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="rfc_contractor">RFC Contractor</label>
                    <div class="col-sm-7">
                    <select class="form-control" name="rfc_contractor" id="rfc_contractor">
                    <option value="">Select</option>
                    <?php $contract = mysql_query("select * from contractor_master where status='1'");
					while($contract_detail = mysql_fetch_array($contract)) { ?>
                    <option value="<?php echo $contract['id'];?>" <?php if($contract_detail['id']==$result['rfc_contractor']) { ?> selected="selected" <?php } ?>><?php echo $contract_detail['company_name']; ?></option>
                    <?php } ?>
                    </select>-->
               <!--<input type="text" id="rfc_contractor" style="text-align:left;" class="form-control" name="rfc_contractor" value="<?php echo $result['rfc_contractor']; ?>"  >-->
                 <!--   </div>
                  </div>
                  
              
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="rfc_date_2nd">RFC Date 2<sup>nd</sup></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="rfc_date_2nd" name="rfc_date_2nd" value="<?php echo $result['rfc_date_2nd']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="ng_contractor">NG Contractor</label>
                <div class="col-sm-7">
                
                 <select class="form-control" name="ng_contractor" id="ng_contractor">
                    <option value="">Select</option>
                    <?php $contract = mysql_query("select * from contractor_master where status='1'");
					while($contract_detail = mysql_fetch_array($contract)) { ?>
                    <option value="<?php echo $contract['id'];?>" <?php if($contract_detail['id']==$result['ng_contractor']) { ?> selected="selected" <?php } ?>><?php echo $contract_detail['company_name']; ?></option>
                    <?php } ?>
                    </select>
                </div>
              </div>
                  
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="ng_date">NG Data</label>
                <div class="col-sm-7">
                  <input type="text" id="ng_date" style="text-align:left;" class="form-control" name="ng_date"  value="<?php echo $result['ng_date']; ?>" readonly="readonly">
                </div>
              </div>-->
              
          <!--<div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="father'sname">NG Date 2</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="ng_date_2" name="ng_date_2" value="<?php echo $result['ng_date_2']; ?>" readonly="readonly">
                </div>
              </div>
                         
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="s_last_bill_date">S Last Bill Date</label>
                <div class="col-sm-7">
                 <input type="text" class="form-control" id="s_last_bill_date" name="s_last_bill_date"  value="<?php echo $result['s_last_bill_date']; ?>" readonly="readonly">
                </div>
              </div>
                
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="s_bill_status">S Bill Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="s_bill_status" name="s_bill_status" value="<?php echo $result['s_bill_status']; ?>" >
                </div>
              </div>
            
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="s_customer_status">S Customer Status</label> 
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="s_customer_status" name="s_customer_status" value="<?php echo $result['s_customer_status']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="s_sd_balance">S SD Bbalance</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="s_sd_balance" name="s_sd_balance" value="<?php echo $result['s_sd_balance']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="s_outstanding">S Outstanding</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="s_outstanding" name="s_outstanding" value="<?php echo $result['s_outstanding']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mktg_update">Marketing Update</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mktg_update" name="mktg_update" value="<?php echo $result['mktg_update']; ?>" >
                </div>
              </div> -->
           
            
              
          <!--<div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="gm_rfc_dt">GM RFC Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="gm_rfc_dt" name="gm_rfc_dt" value="<?php echo $result['gm_rfc_dt']; ?>" readonly="readonly">
                </div>
              </div> 
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="gm_ng_dt">GM NG Date</label>
                <div class="col-sm-7">
                 <input type="text" class="form-control"  id="gm_ng_dt" name="gm_ng_dt" value="<?php echo $result['gm_ng_dt']; ?>" readonly="readonly">
                </div>
              </div>   
                         
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="gm_proj_status">GM Project Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="gm_proj_status" name="gm_proj_status"  value="<?php echo $result['gm_proj_status']; ?>">
                </div>
              </div>
                
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="gm_jmr_serial">GM JMR Serial</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="gm_jmr_serial" name="gm_jmr_serial" value="<?php echo $result['gm_jmr_serial']; ?>" >
                </div>
              </div>
            
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="gm_jmr_hover">GM JMR Hover</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="gm_jmr_hover" name="gm_jmr_hover" value="<?php echo $result['gm_jmr_hover']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="gm_gi_cont">GM GI Cont</label>
                <div class="col-sm-7">
                  <input type="tetx" class="form-control" id="gm_gi_cont" name="gm_gi_cont"  value="<?php echo $result['gm_gi_cont']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="am_rfc_dt">AM RFC Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="am_rfc_dt" name="am_rfc_dt" value="<?php echo $result['am_rfc_dt']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="am_ng_dt">AM NG Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="am_ng_dt" name="am_ng_dt" value="<?php echo $result['am_ng_dt']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="am_proj_status">AM Project Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="am_proj_status" name="am_proj_status" value="<?php echo $result['am_proj_status']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="am_jmr_status">AM JMR Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="am_jmr_status" name="am_jmr_status" value="<?php echo $result['am_jmr_status']; ?>" >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="am_jmr_sub_dt">AM JMR Sub Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="am_jmr_sub_dt" name="am_jmr_sub_dt" value="<?php echo $result['am_jmr_sub_dt']; ?>" readonly="readonly">
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="am_gi_contractor">AM GI Contractor</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="am_gi_contractor" name="am_gi_contractor" value="<?php echo $result['am_gi_contractor']; ?>" >
                </div>
              </div>  --> 
              
              
            
              
           
              
             
              
              
             
              
          </div>
         
        
          <div class="clearfix"></div>       
        
         <input type="hidden" name="employee_id" id="employee_id" value="<?php echo  $_SESSION['adminid'];?>" />
               
           <!--<div class="col-md-12" align="center"><div class="form-group">
                <button type="submit" class="btn btn-primary"><?php if($result[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
          </div></div>-->
                    
               </div>     
                    
                    
                    
           </form>


                        </div>
					    </div>			
					    </div>
