<?php session_start(); ?>
   <?php foreach($results as $result) { }  ?>
<div class="col-md-12">
        <div class="row">
			<div class="col-lg-12">            
			   <ol class="breadcrumb">    
				<li><a href="Index.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="index.php?control=png_dom&task=show_agra">New Agra PNG </a></li>
                <li class="active"> <?php if($result['id']!='') { ?> View <?php } else { ?>Add<?php } ?> Agra PNG </li>
			</ol>
			</div>
		</div><!--/.row-->
      </div>  
<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
                        <div class="panel-heading">
                        
                        <?php if($result['id']!='') { ?> <h3>View Agar PNG Connection Detail </h3> <?php } else { ?><u><h3>Add Agar PNG Connection</h3></u><?php }
                        
                        ?>  
                        </div>
                    <br>
                    
                    <form class="form-horizontal" action="" method="post" role="form" enctype="multipart/form-data" autocomplete="off">
        
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
         
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="new_crn">Present CRN</label>
                <div class="col-sm-7">
                <input type="text" class="form-control"  id="present_crn" name="present_crn" value="<?php echo $result['present_crn']; ?>" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?> >
                </div>
              </div> 
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="asset">Asset</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="asset" name="asset"    value="<?php echo $result['asset']; ?>" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?> >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="name">Name</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="name" name="name" pattern="[a-zA-Z.\s]+"  value="<?php echo $result['name']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?> >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="address">Address</label>
                <div class="col-sm-7">
                <textarea class="form-control" id="address" name="address" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?> ><?php echo $result['address']; ?></textarea>                
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="block_sec_khand">Block/Sec/Khand</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="block_sec_khand" name="block_sec_khand" value="<?php echo $result['block_sec_khand']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="apartment_society">Apartment/Society</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="apartment_society" name="apartment_society" value="<?php echo $result['apartment_society']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mou_flats_individual">Mou/Flats/Individual</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mou_flats_individual" name="mou_flats_individual" value="<?php echo $result['mou_flats_individual']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
           
              <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="charge_uncharge_area">Charge/Uuncharge/Area</label>
                    <div class="col-sm-7">
                      <input type="text" id="charge_uncharge_area" style="text-align:left;" class="form-control" name="charge_uncharge_area" value="<?php echo $result['charge_uncharge_area']; ?>"   <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                    </div>
                  </div>
                  
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="area">Area</label>
                <div class="col-sm-7">
                  <input type="text" id="area" style="text-align:left;" class="form-control" name="area" value="<?php echo $result['area']; ?>"   <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mobile">Mobile</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mobile" name="mobile"  pattern="[6789][0-9]{9}" maxlength="10" value="<?php echo $result['mobile']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div> 
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="landline">Landline</label>
                <div class="col-sm-7">
                 <input type="text" class="form-control"  id="landline" name="landline" maxlength="15" value="<?php echo $result['landline']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>   
                         
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="email">Email</label>
                <div class="col-sm-7">
                  <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"  value="<?php echo $result['email']; ?>" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
              
               <div class="form-group form-group-sm">
                    <label class="col-sm-5 control-label" style="text-align:left;" for="fnl_status_z">Final Status Z</label>
                    <div class="col-sm-7">
                      <input type="text" id="fnl_status_z" style="text-align:left;" class="form-control" name="fnl_status_z" value="<?php echo $result['fnl_status_z']; ?>"  <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?> >
                    </div>
                  </div>
                  
              	 <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="rfc_fnl_z">RFC Final Z</label>
                <div class="col-sm-7">
                  <input type="text" id="rfc_fnl_z" style="text-align:left;" class="form-control" name="rfc_fnl_z" value="<?php echo $result['rfc_fnl_z']; ?>"   <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
             
          </div>
         
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
         
         	<div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="rfc_date_1st">RFC Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="rfc_date" name="rfc_date" value="<?php echo $result['rfc_date']; ?>" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
              
                <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mktg_update">Marketing Status</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mktg_status" name="mktg_status" value="<?php echo $result['mktg_status']; ?>" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div> 
            <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="ninety_days_pendency_sch">90 days pendency Sch</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="ninety_days_pendency_sch" name="ninety_days_pendency_sch" value="<?php echo $result['ninety_days_pendency_sch']; ?>" <?php if($_SESSION['utype']!='Admin'){ echo "readonly";}?>>
                </div>
              </div>
                
                 <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_gi_contractor_alloted">Project GI Contractor Alloted</label>
                <div class="col-sm-7">
                   <?php if($_SESSION['department_id']=='2' || $_SESSION['utype']=='Admin'){
					      $cityId =  $_SESSION['utype']!='Admin'?" and city_id = '".$_SESSION['city_id']."'":'';?> 
                  <select class="form-control" type="text" name="proj_gi_contractor_alloted" id="proj_gi_contractor_alloted" >
                    <option value="">Proj. Contractor</option>
                    <?php $sql_cont = mysql_query("select * from contractor_master where status='1'  $cityId"); 
					     while($contract = mysql_fetch_array($sql_cont)){?>
                    <option value="<?php echo $contract['id']; ?>" <?php if($contract['id']==$result['proj_gi_contractor_alloted']){echo "selected";} ?> ><?php echo $contract['company_name']; ?></option>
                    <?php } ?>
                  
                  </select>
                  <?php }else{  $gi_contractor = mysql_fetch_array(mysql_query("select * from contractor_master where id='".$result['proj_gi_contractor_alloted']."'"));  ?>
                  <input type="text" class="form-control" id="proj_gi_contractor_alloted" name="proj_gi_contractor_alloted" value="<?php echo $gi_contractor['company_name']; ?>" <?php if($_SESSION['department_id']!='2'){ echo "readonly";}?> >
                  <?php } ?>
                </div>
              </div>  
             <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="meter_no" >Meter No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="meter_no" name="meter_no" value="<?php echo $result['meter_no']; ?>"  <?php if($_SESSION['department_id']!='2' && $_SESSION['utype']!='Admin'){ echo "readonly";}?>  >
                </div>
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="initial_reading">Initial Reading</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="initial_reading" name="initial_reading" value="<?php echo $result['initial_reading']; ?>"  <?php if($_SESSION['department_id']!='2' && $_SESSION['utype']!='Admin'){ echo "readonly";}?> >
                </div>
              </div>
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_ng_dt">Project NG Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="proj_ng_dt" name="proj_ng_dt" value="<?php echo $result['proj_ng_dt']; ?>" placeholder="Enter Proj NG Date (dd-mm-YYYY)"  <?php if($_SESSION['department_id']!='2' && $_SESSION['utype']!='Admin'){ echo "readonly";}?> >
                </div>
              </div>
               
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_rfc_dt">JMR No.</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="jmr_no" name="jmr_no"  value="<?php echo $result['jmr_no']; ?>"  <?php if($_SESSION['department_id']!='2' && $_SESSION['utype']!='Admin'){ echo "readonly";}?>  >
                </div>
              </div>
               <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">Project Remarks</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="proj_remarks" name="proj_remarks" value="<?php echo $result['proj_remarks']; ?>"  <?php if($_SESSION['department_id']!='2' && $_SESSION['utype']!='Admin'){ echo "readonly";}?>  >
                </div>
              </div>
              
            
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_rfc_dt">Marketing RFA Date</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="proj_rfc_dt" name="proj_rfc_dt"  value="<?php echo $result['proj_rfc_dt']; ?>" placeholder="Enter Proj RFA Date (dd-mm-YYYY)"  <?php if($_SESSION['department_id']!='3' && $_SESSION['utype']!='Admin'){echo 'readonly';} ?>  >
                </div>
              </div>
              
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="mktg_update">Marketing Remark</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="mktg_remark" name="mktg_remark" value="<?php echo $result['mktg_remark']; ?>"  <?php if($_SESSION['department_id']!='3' && $_SESSION['utype']!='Admin'){echo 'readonly';} ?>  >
                </div>
              </div>
              
              
               <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">NG Progress fy1920</label>
                <div class="col-sm-7"> 
                        
                  <input type="text" class="form-control" id="ng_progress_fy1920" name="ng_progress_fy1920" value="<?php echo $result['ng_progress_fy1920']; ?>" <?php if($_SESSION['utype']!='Admin'){echo 'readonly';} ?> >
                   
                </div>
               </div>
               <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">Registration Index</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="registration_index" name="registration_index" value="<?php echo $result['registration_index']; ?>" <?php if($_SESSION['utype']!='Admin'){echo 'readonly';} ?> >
                </div>
              </div> 
              <div class="form-group form-group-sm">
                <label class="col-sm-5 control-label" style="text-align:left;" for="proj_remarks">Overall Comments</label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" id="overall_comments" name="overall_comments" value="<?php echo $result['overall_comments']; ?>" >
                </div>
              </div>
               
              
          </div>
         
        
          <div class="clearfix"></div>       
        
         <input type="hidden" name="employee_id" id="employee_id" value="<?php echo  $_SESSION['adminid'];?>" />
                <input type="hidden" name="department" id="department" value="<?php echo $_SESSION['department_id'];?>" />
           <div class="col-md-12" align="center"><div class="form-group">
                <button type="submit" class="btn btn-primary"><?php if($result[0]['id']) { echo "Update"; } else { echo "Submit";} ?></button>
          </div></div>
                    
                
                    
                    
                      <input type="hidden" name="control" value="png_dom"/>
                      <input type="hidden" name="edit" value="1"/>
                      <input type="hidden" name="task" value="save_agra"/>
                      <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
                      <input type="hidden" name="tpages" id="tpages" value="<?php echo $_REQUEST['tpages']; ?>"  />
                      <input type="hidden" name="page" id="page" value="<?php echo $_REQUEST['page'];?>"  />
           </form>
		
           


                        </div>
					    </div>			
					    </div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>  
<script src="assets/date_picker/jquery.js"></script>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
$('#registration_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
<?php if($_SESSION['department_id']!='2') { ?>
$('#rfc_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
<?php } ?>
$('#rfc_date_2nd').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#ng_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#ng_date_2').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#s_last_bill_date').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#gm_rfc_dt').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#gm_ng_dt').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#am_rfc_dt').datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'d/m/Y',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});

var currentTime = new Date();
// First Date Of the month 
var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
// Last Date Of the Month 
var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
$("#proj_rfc_dt").datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
    format:'Y-m-d',
	formatDate:'d/m/Y',
    minDate: startDateFrom,
    maxDate: startDateTo
});
	
	
	var currentTime = new Date();
// First Date Of the month 
var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
// Last Date Of the Month 
var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
$("#proj_ng_dt").datetimepicker({
	yearOffset:0,
	lang:'ch',
	timepicker:false,
    format:'Y-m-d',
	formatDate:'d/m/Y',
    minDate: startDateFrom,
    maxDate: startDateTo
});

</script>

