<?php if($_SESSION['utype']=="Administrator"){ ?>
<style>
.dropdown-submenu{
	position:relative;
	display:block;
}
.dropdown-submenu>.dropdown-menu {
    top:1px;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block !important;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}


.navbar-nav a{ color:#FFF !important;
/*background-color:#30a5ff !important;*/
}
<!--.Active{ background-color:#30a5ff !important; }-->

.dropdown-menu {
  background-color: #125C95 !important;
}


.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
  background-color: #080808 !important;
  color: #262626;
  text-decoration: none;
}
</style>

<ul class="nav navbar-nav" >
           <!-- <style> a{ color:#FFF !important;} .Active{background-color:#30a5ff !important;}</style>-->
              <li class="panel-red"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Masters<b class="caret"></b></a>
                <ul class="dropdown-menu">
              <!--  <li><a href="index.php?control=customer">Customers / Introducer Registration</a></li>-->
                        
                        <li class="dropdown-submenu"><a href="index.php">Regional Hierarchy</a>
                        <ul class="dropdown-menu">
                        <li><a href="index.php?control=regional_hierarchy&task=show_zone">Add Zone</a></li>
                        <li><a href="index.php?control=regional_hierarchy&task=show_state">Add State</a></li>
                        <li><a href="index.php?control=regional_hierarchy&task=show_city">Add City</a></li>
                        <!--<li><a href="./mr_reporting/index.php?control=regional_hierarchy&task=show_area">Add Area</a></li>-->
                        </ul>
                        </li>
                        <li><a href="index.php?control=master_location">Location Master</a></li>
                        <li><a href="index.php?control=master_branch">Branch Master</a></li>
                        <!-- <li><a href="index.php?control=master_registration">Users Master</a></li>
                        <li><a href="index.php?control=master_scheme">Scheme Master</a></li>-->
                        <li><a href="index.php?control=master_loan">Loan Master</a></li>
                        <li><a href="index.php?control=master_lenders">Lenders Master</a></li>
                        <!--<li><a href="index.php?control=master_fd">Fixed Deposit Master</a></li>
                        <li><a href="index.php?control=master_rd">Recurring Deposit Master</a></li>
                        <li><a href="index.php?control=master_pdc">PDC Master</a></li>
                        <li><a href="index.php?control=master_tds">TDS Master</a></li>                          
                        <li><a href="index.php?control=master_charge">Charges Master</a></li>-->
                                              
                        <!--<li><a href="index.php?control=master_addcbank">Add Company Bank</a></li>
                        <li><a href="TemplateSettings.html">Template Master</a></li>-->
                         <li><a href="index.php?control=admin_menu">Menu Permission</a></li>
                
                </ul>
              </li> 
              
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">HRM<b class="caret"></b></a>
                <ul class="dropdown-menu">
                
                <li><a href="index.php?control=hrm_post">Employee Designation</a></li>
                <li><a href="index.php?control=hrm_registration">Employee Registration</a></li>
                <!-- <li><a href="index.php?control=hrm_salary">Employee Salary Detail</a></li>
                 <li><a href="index.php?control=hrm_salary&task=show_salary">Employee Monthly Salary</a></li>
               
                <li><a href="index.php?control=hrm_leave">Leave Management</a></li>
                <li><a href="index.php?control=hrm_information">Information Management</a></li>-->
                </ul>
              </li> 
               
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Customer<b class="caret"></b></a>
                <ul class="dropdown-menu">
                
                <li><a href="index.php?control=customer">Customers / Introducer Registration</a></li>
                 
                 <li><a href="index.php?control=customer_loan">Customer Loan Section</a></li>
                 <!--<li><a href="index.php?control=customer_scheme">Add Customer Scheme</a></li>
                 <li><a href="index.php?control=customer_fd">Customer FD Section</a></li>
                 <li><a href="index.php?control=customer_rd">Customer RD Section</a></li>
                 <li><a href="index.php?control=collection">Collection</a></li>
                 <li><a href="index.php?control=payouts">Payouts</a></li>
                 <li><a href="index.php?control=loan">Loan</a></li> -->
                </ul>
              </li> 
              
                    
              
               <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                <ul class="dropdown-menu">
                <li><a href="#">Loan Report</a></li>
                <li><a href="index.php?control=hrm_attendence&task=show_attendance">Employee Attendance</a></li>

                <!--<li><a href="index.php?control=reports&&task=show_daily_analysis">Daily Analysis</a></li>
                <li><a href="index.php?control=reports&&task=show_fortnightly_analysis">Fortnightly Reconciliation</a></li>
                <li><a href="index.php?control=reports&&task=show_yearly_reconciliation">Yearly Reconciliation</a></li>
                <li><a href="index.php?control=reports&&task=show_tds_statement">TDS Statement</a></li>
                <li><a href="index.php?control=reports&&task=show_maturity_statement">Maturity Statement</a></li>
                <li><a href="">Statement of Foreclosures </a></li>
                <li><a href="">Statement of Faulty </a></li>
                <li><a href="">Statement of Renewed Deposit </a></li>
                <li><a href="index.php?control=reports&&task=show_depositors_statement">A/C Statement for Depositors </a></li>
                <li><a href="index.php?control=reports&&task=show_collectors_statement">A/C Statement for Collectors/Agent</a></li>
                <li><a href="index.php?control=reports&&task=show_borrowers_statement">A/C Statement for Borrowers</a></li>-->

                </ul>
              </li>  
              
              
              <!--<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Introducer<b class="caret"></b></a>
                <ul class="dropdown-menu">
                
                <li><a href="index.php?control=introducer">Introducer Enquiry</a></li>
                <li><a href="introduceracstatement.html">Introducer A/C Statement</a></li>
                <li><a href="CustomersList.html">Customers List</a></li>
                <li><a href="CustomersSchemeList.html">Schemes List</a></li>
                </ul>
              </li> 
              
               <li><a href="#">Daily Visit Plan</a></li>-->
                <!--<li><a href="index.php?control=emailTemplate">Email Templates</a></li>-->
                <li><a href="index.php?control=master_target&task=show_target">Assign Target</a></li>
                 <li><a href="index.php?control=master_emi&task=show">EMI Calculater</a></li>
            </ul>
            <?php } else { ?>
			
            <ul class="nav navbar-nav" style="color:#FFF;">
            <style> a{ color:#FFF !important;} .Active{background-color:#30a5ff !important;}</style>
              <li class="panel-red"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
              <!--<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Masters<b class="caret"></b></a>
                <ul class="dropdown-menu">-->
              <!--  <li><a href="index.php?control=customer">Customers / Introducer Registration</a></li>-->
                       <!--  <li><a href="index.php?control=master_lenders">Landers Master</a></li>
                        <li><a href="index.php?control=master_location">Location Master</a></li>
                        <li><a href="index.php?control=master_branch">Branch Master</a></li>
                        <li><a href="index.php?control=master_registration">Users Master</a></li>
                        <li><a href="index.php?control=master_scheme">Scheme Master</a></li>
                        <li><a href="index.php?control=master_charge">Charges Master</a></li>
                        <li><a href="index.php?control=master_tds">TDS Master</a></li>
                        <li><a href="index.php?control=master_pdc">PDC Master</a></li>
                        <li><a href="index.php?control=master_addcbank">Add Company Bank</a></li>
                        <li><a href="TemplateSettings.html">Template Master</a></li>
                
                </ul>
              </li> -->
              
              <li class="dropdown">
                <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">HRM<b class="caret"></b></a>
		
		<ul class="dropdown-menu">
        <li><a href="index.php?control=hrm_registration&task=show_emp">Employee Detail</a></li>
        
        <li><a href="index.php?control=hrm_attendence">Add Attendance</a></li>
		<!--<li><a href="index.php?control=hrm_attendence&task=show_attendance">Add Attendance</a></li>
		<!--<li><a href="index.php?control=hrm_leave&task=show_leave">Add Leave</a></li>
		<li><a href="index.php?control=hrm_attendence&task=show_salary">Salary Detail</a></li>-->
		
		</ul>
		</li>
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Customer<b class="caret"></b></a>
                <ul class="dropdown-menu">
                
                <li><a href="index.php?control=customer">Customers / Introducer Registration</a></li>
                 
                 <li><a href="index.php?control=customer_loan">Customer Loan Section</a></li>
                 <!--<li><a href="index.php?control=customer_scheme">Add Customer Scheme</a></li>
                 <li><a href="index.php?control=customer_fd">Customer FD Section</a></li>
                 <li><a href="index.php?control=customer_rd">Customer RD Section</a></li>
                 <li><a href="index.php?control=collection">Collection</a></li>
                 <li><a href="index.php?control=payouts">Payouts</a></li>
                 <li><a href="index.php?control=loan">Loan</a></li> -->
                </ul>
              </li> 
         <li class="dropdown">
                <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">Target<b class="caret"></b></a>
		
		<ul class="dropdown-menu">
        <li><a href="index.php?control=master_target&task=show_emp_target">Target Assign</a></li>
		<li><a href="index.php?control=master_target&task=show_emp_target">Target Achived</a></li>
		<li><a href="index.php?control=master_target&task=show_emp_target">Target Pending</a></li>
		
		
		</ul>
		</li>
         <li><a href="index.php?control=master_meeting&task=show_emp_meeting">Meeting</a></li>
         <li><a href="#">Appointments</a></li>
        <!-- 
              <li><a href="index.php?control=customer_scheme">Add Customer Scheme</a></li>         
              <li><a href="index.php?control=collection">Collection</a></li>
              <li><a href="index.php?control=payouts">Payouts</a></li>
              <li><a href="index.php?control=loan">Loan</a></li>
               <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                <ul class="dropdown-menu">
                <li><a href="index.php?control=reports&&task=show_daily_analysis">Daily Analysis</a></li>
                <li><a href="index.php?control=reports&&task=show_fortnightly_analysis">Fortnightly Reconciliation</a></li>
                <li><a href="index.php?control=reports&&task=show_yearly_reconciliation">Yearly Reconciliation</a></li>
                <li><a href="index.php?control=reports&&task=show_tds_statement">TDS Statement</a></li>
                <li><a href="index.php?control=reports&&task=show_maturity_statement">Maturity Statement</a></li>
                <li><a href="">Statement of Foreclosures </a></li>
                <li><a href="">Statement of Faulty </a></li>
                <li><a href="">Statement of Renewed Deposit </a></li>
                <li><a href="index.php?control=reports&&task=show_depositors_statement">A/C Statement for Depositors </a></li>
                <li><a href="index.php?control=reports&&task=show_collectors_statement">A/C Statement for Collectors/Agent</a></li>
                <li><a href="index.php?control=reports&&task=show_borrowers_statement">A/C Statement for Borrowers</a></li>

                </ul>
              </li>  
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Introducer<b class="caret"></b></a>
                <ul class="dropdown-menu">
                
                <li><a href="index.php?control=introducer">Introducer Enquiry</a></li>
                <li><a href="introduceracstatement.html">Introducer A/C Statement</a></li>
                <li><a href="CustomersList.html">Customers List</a></li>
                <li><a href="CustomersSchemeList.html">Schemes List</a></li>
                </ul>
              </li>  -->
            </ul>
            <?php } ?>