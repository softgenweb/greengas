<?php include('../../configuration.php');
date_default_timezone_set('Asia/Kolkata');

   $branch_id = substr($_REQUEST['crn_no'],0,3);
   
   // echo "SELECT * FROM `company` WHERE `branch_code` LIKE '%".$branch_id."%'";
   $company = mysql_fetch_array(mysql_query("SELECT * FROM `company` WHERE `branch_code` LIKE '%".$branch_id."%'"));
   
   
   $bill = mysql_fetch_array(mysql_query("SELECT * FROM `customer` WHERE `crn`='".$_REQUEST['crn_no']."'"));
   $bill_history = mysql_fetch_array(mysql_query("SELECT * FROM `customer_bill_history` WHERE `crn_no`='".$_REQUEST['crn_no']."'"));
   
   $country = mysql_fetch_array(mysql_query("SELECT * FROM `country` WHERE `id`='".$company['country']."'"));
   $state = mysql_fetch_array(mysql_query("SELECT * FROM `state` WHERE `id`='".$company['state']."'"));
   $city = mysql_fetch_array(mysql_query("SELECT * FROM `city` WHERE `id`='".$company['branch_city']."'"));
   
    ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Green Gas Limited::Invoice (<?php echo $bill['invoice_no']; ?>)</title>
      <style type="text/css">
         body,td,th {
         font-family: Arial, Helvetica, sans-serif;
         font-size: 12px;
         }
         body {
         margin-left: 0px;
         margin-top: 0px;
         margin-right: 0px;
         margin-bottom: 0px;
         }
         .maintbl
         {
         width:1030px;
         border:1px solid #000;
         }
         .emptyrow {
         border-top: none;
         }
         .emptyrow {
         border-bottom: none;
         }
         tr.top td  { border-top: 1px solid;}
         tr.btm td  { border-bottom: 1px solid; }
         tr.rht td  { border-right: 1px solid;}
         tr.lft td  { border-left: 1px solid;}
         .top { border-top: 1px solid;}
         .btm { border-bottom: 1px solid;}
         .btmdot { border-bottom: 1px  dotted;}
         .rht { border-right: 1px solid;}
         .lft { border-left: 1px solid;}
         .bdr{ border: 1px solid #000;}
         .bghd{ background:#e3e3e3; }
         @media print{
         .bghd tr td{ background-color:#e3e3e3; }
         .hide_print{
          visibility: hidden;
         }
         }
.button, input[type="button"], input[type="reset"] {
  background: #175200 linear-gradient(to bottom, #2f9607 0%, #51be26 100%) repeat-x scroll 0 0;
  border: medium none;
  color: #eff806;
  margin-top: 10px;
  padding: 8px 15px;
  text-shadow: 0 -1px 0 rgba(23, 82, 0, 0.66);
  /* position: relative; */
  /* left: 150px; */
}
      </style>
   </head>
   <body>
      <table cellspacing="0" cellpadding="3" class="maintbl" align="center">
         <col width="100" />
         <col width="159" />
         <col width="93" />
         <col width="90" />
         <col width="111" />
         <col width="164" />
         <col width="100" />
         <col width="79" />
         <col width="134" />
         <tr>
            <td width="94" align="left" valign="top">
               <img src="invoice_clip_image002.gif" alt="" width="94" height="96" />
               <table cellpadding="0" cellspacing="0">
                  <tr>
                     <td width="100"></td>
                  </tr>
               </table>
            </td>
            <td colspan="8" align="center" style="line-height:23px"><strong>SALES INVOICE</strong><br />
               <span style="font-size:16px"><strong><?php echo $company['name']; ?></strong></span><br />
               <?php echo $company['address1']; ?>
               <br>
               <?php echo $company['address2']; ?>
               <br>
               <?php echo $city['city_name']; ?>-<?php echo $company['pin_no'].' '.$state['state_name']; ?> <br />
               <?php echo $country['country_name']; ?>
            </td>
         </tr>
         <tr>
            <td></td>
            <td colspan="8">&nbsp;</td>
         </tr>
         <tr class="btm">
            <td><strong>TIN NO</strong></td>
            <td width="182"><strong><?php echo $company['tin_no']; ?></strong></td>
            <td width="81">&nbsp;</td>
            <td width="80"><strong>GSTIN</strong></td>
            <td colspan="2"><strong><?php echo $company['gst_no']; ?></strong></td>
            <td width="85"><strong>GSTIN TYPE:</strong></td>
            <td colspan="2"><strong><?php echo $company['gstin_type']; ?></strong></td>
         </tr>
         <tr>
            <td colspan="3"><strong>Customer Name &amp;    Address</strong></td>
            <td class="lft btm rht bghd" width="100"><strong>New CRN No.</strong></td>
            <td width="130" class="btm bghd" ><strong><?php echo $bill['crn']; ?></strong></td>
            <td width="68" class="lft" >Invoice No</td>
            <td><strong><?php echo $bill['invoice_no']; ?> </strong></td>
            <td width="118" class="lft btm rht bghd">Meter No.</td>
            <td width="136" class="btm bghd "><?php echo $bill['meter_no']; ?></td>
         </tr>
         <tr>
            <td colspan="5"><strong><?php echo $bill['name']; ?></strong></td>
            <td class="lft">Invoice Date</td>
            <td><?php echo $bill['invoice_date']; ?></td>
            <td></td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="5" rowspan="4"><?php echo ucwords($bill['bill_to']); ?></td>
            <td class="lft">Due Date</td>
            <td><?php echo $bill['due_date']; ?></td>
            <td></td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="2" class="lft">Mode/Terms of Payment: Credit</td>
            <td></td>
            <td>&nbsp;</td>
         </tr>
         <tr class="btm">
            <td colspan="4" class="lft">Security Deposit Amount: <?php echo $bill['sd_amt']; ?></td>
         </tr>
         <tr>
            <td colspan="4" class="lft">Emergency Helpline No: <?php echo $company['helpline_no']; ?></td>
         </tr>
         <tr>
            <td colspan="5"><?php echo $bill['Mobile_No']; ?></td>
            <td colspan="4" class="lft">Complain/ Info Call: <?php echo $company['complain_no']; ?></td>
         </tr>
         <tr>
            <td colspan="5">&nbsp;</td>
            <td colspan="4" class="lft">Website: <?php echo $company['website']; ?></td>
         </tr>
         <tr class="top btm">
            <td class="bghd"><strong>Previous Dues</strong></td>
            <td class="lft bghd"><strong>Payments</strong></td>
            <td class="lft bghd"><strong>Adjustments</strong></td>
            <td  class="lft bghd" colspan="2"><strong>Current Charges</strong></td>
            <td  class="lft bghd" ><strong>Total Amount Due</strong></td>
            <td  class="lft bghd" ><strong>Due Date</strong></td>
            <td  class="lft bghd" colspan="2"><strong>Amount Payable after due date</strong></td>
         </tr>
         <tr class="btm" >
            <td><?php echo number_format(($bill['due_amount']+$bill['last_payment'])-$bill['current_charges'],2); ?></td>
            <td class="lft" ><?php echo number_format($bill['last_payment'],2); ?></td>
            <td class="lft" ><?php echo number_format($bill['due_amount']-$bill['current_charges'],2); ?></td>
            <td  class="lft" colspan="2"><?php echo number_format($bill['current_charges'],2); ?></td>
            <td class="lft" ><?php echo number_format($bill['due_amount'],2); ?></td>
            <td class="lft" ><?php echo $bill['due_date']; ?></td>
<!-- ================================================== -->

                <?php 
                  $today = date('Y-m-d');
                  $time = strtotime($bill['due_date']);
                  $due_date = date('Y-m-d', $time);
                  $late_amt = $bill['due_amount']*(0.02);
                  $with_due_amt = ($late_amt>50)?($bill['due_amount']+$late_amt):($bill['due_amount']+50);
                  if($due_date>=$today){
                  $total_final_amount = $bill['due_amount'];
                  $late_chrg = 0;
                  }else{
                  $total_final_amount = $with_due_amt;
                  $late_chrg = ($late_amt>50)?($late_amt):50;
                  }
                 ?>
<!-- ================================================== -->
            <td class="lft"  colspan="2"><?php echo $with_due_amt; ?>
          </td>
         </tr>
         <!-- K+ max(50, K*(.02))<?php //echo $bill['due_date']; ?> -->
         <tr class="btm">
            <td rowspan="2" class="bghd"><strong>Sr. No.</strong></td>
            <td rowspan="2" class="lft bghd" ><strong>Segment</strong></td>
            <td colspan="2" align="center" class="lft bghd" ><strong>Previous</strong></td>
            <td colspan="2" align="center" class="lft bghd" ><strong>Current</strong></td>
            <td rowspan="2" class="lft bghd" ><strong>Consumption</strong></td>
            <td rowspan="2" class="lft bghd" ><strong>Price</strong></td>
            <td rowspan="2" class="lft bghd" ><strong>Line Total</strong></td>
         </tr>
         <tr class="btm">
            <td class="lft bghd" ><strong>Date</strong></td>
            <td class="lft bghd" ><strong>Reading</strong></td>
            <td class="lft bghd" ><strong>Date</strong></td>
            <td class="lft bghd" ><strong>Reading</strong></td>
         </tr>
         <tr>
            <td rowspan="6" >1</td>
            <td rowspan="6" class="lft">Piped Natural Gas(New)</td>
            <td rowspan="6" class="lft"><?php echo $bill['Pre_Reading_Date']; ?></td>
            <td class="lft"><?php echo $bill['Pre_Reading_Line-0']; ?> </td>
            <td rowspan="6" class="lft"><?php echo $bill['Cur_Reading_Date']; ?></td>
            <td class="lft"><?php echo $bill['Cur_Reading_Line-0']; ?></td>
            <td class="lft"><?php echo number_format($bill['Cur_Reading_Line-0']-$bill['Pre_Reading_Line-0'],2); ?></td>
            <td class="lft"><?php echo $bill['Price_Line-0']; ?></td>
            <td class="lft"><?php echo number_format(($bill['Cur_Reading_Line-0']-$bill['Pre_Reading_Line-0'])*$bill['Price_Line-0'],2); ?></td>
         </tr>
         <tr>
            <td class="lft"><?php echo $bill['Pre_Reading_Line-1']; ?></td>
            <td class="lft"><?php echo $bill['Cur_Reading_Line-1']; ?></td>
            <td class="lft"><?php echo number_format($bill['Cur_Reading_Line-1']-$bill['Pre_Reading_Line-1'],2); ?></td>
            <td class="lft"><?php echo $bill['Price_Line-1']; ?></td>
            <td class="lft"><?php echo number_format(($bill['Cur_Reading_Line-1']-$bill['Pre_Reading_Line-1'])*$bill['Price_Line-1'],2); ?> </td>
         </tr>
         <tr>
            <td class="lft"><?php echo $bill['Pre_Reading_Line-2']; ?> </td>
            <td class="lft"><?php echo $bill['Cur_Reading_Line-2']; ?></td>
            <td class="lft"><?php echo number_format($bill['Cur_Reading_Line-2']-$bill['Pre_Reading_Line-2'],2); ?></td>
            <td class="lft"><?php echo $bill['Price_Line-2']; ?></td>
            <td class="lft"><?php echo number_format(($bill['Cur_Reading_Line-2']-$bill['Pre_Reading_Line-2'])*$bill['Price_Line-2'],2); ?> </td>
         </tr>
         <tr>
            <td class="lft"><?php echo $bill['Pre_Reading_Line-3']; ?> </td>
            <td class="lft"><?php echo $bill['Cur_Reading_Line-3']; ?></td>
            <td class="lft"><?php echo number_format($bill['Cur_Reading_Line-3']-$bill['Pre_Reading_Line-3'],2); ?></td>
            <td class="lft"><?php echo $bill['Price_Line-3']; ?></td>
            <td class="lft"><?php echo number_format(($bill['Cur_Reading_Line-3']-$bill['Pre_Reading_Line-3'])*$bill['Price_Line-3'],2); ?> </td>
         </tr>
         <tr>
            <td class="lft"><?php echo $bill['Pre_Reading_Line-4']; ?> </td>
            <td class="lft"><?php echo $bill['Cur_Reading_Line-4']; ?></td>
            <td class="lft"><?php echo number_format($bill['Cur_Reading_Line-4']-$bill['Pre_Reading_Line-4'],2); ?></td>
            <td class="lft"><?php echo $bill['Price_Line-4']; ?></td>
            <td class="lft"><?php echo number_format(($bill['Cur_Reading_Line-4']-$bill['Pre_Reading_Line-4'])*$bill['Price_Line-4'],2); ?> </td>
         </tr>
         <tr>
            <td class="lft"><?php echo $bill['Pre_Reading_Line-5']; ?> </td>
            <td class="lft"><?php echo $bill['Cur_Reading_Line-5']; ?></td>
            <td class="lft"><?php echo number_format($bill['Cur_Reading_Line-5']-$bill['Pre_Reading_Line-5'],2); ?></td>
            <td class="lft"><?php echo $bill['Price_Line-5']; ?></td>
            <td class="lft"><?php echo number_format(($bill['Cur_Reading_Line-5']-$bill['Pre_Reading_Line-5'])*$bill['Price_Line-5'],2); ?> </td>
         </tr>
         <tr class="btm top">
            <td colspan="5">Bill History</td>
            <td colspan="3" class="lft btm">PNG Consumption Cost</td>
            <td class="lft btm"><?php echo $bill['Line_Total']; ?></td>
         </tr>
         <tr >
            <td class="btm bghd"><strong>From Date</strong></td>
            <td class="lft btm bghd"><strong>To Date</strong></td>
            <td class="lft btm bghd"><strong>SCM</strong></td>
            <td class="lft btm bghd"><strong>Amount</strong></td>
            <td class="lft btm bghd"><strong>Remarks</strong></td>
            <td colspan="3" class="lft btm">Network Tarrif (@<?php echo $company['network_tarrif']; ?> * Q)</td>
            <td class="lft btm"><?php echo $bill['network_tariff']; ?></td>
         </tr>
         <tr >
            <td class="btm"><?php echo $bill_history['LINE-1-FROM_DATE']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-1-TO_DATE']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-1-SCM']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-1-AMOUNT']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-1-REMARKS']; ?></td>
            <td colspan="3" class="lft btm">Minimum Charges</td>
            <td class="lft btm"><?php echo $bill['Minimum_Charges']; ?> </td>
         </tr>
         <tr>
            <td class="btm"><?php echo ($bill_history['LINE-2-FROM_DATE']!='31-12-1969')?$bill_history['LINE-2-FROM_DATE']:''; ?></td>
            <td class="lft btm"><?php echo ($bill_history['LINE-2-TO_DATE']!='31-12-1969')?$bill_history['LINE-2-TO_DATE']:''; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-2-SCM']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-2-AMOUNT']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-2-REMARKS']; ?></td>
            <td colspan="3" class="lft btm">Late Payment Charges/Interest</td>
            <td class="lft btm"><?php echo $bill['late_payment']; ?> </td>
         </tr>
         <tr>
            <td class="btm"><?php echo ($bill_history['LINE-3-FROM_DATE']!='31-12-1969')?$bill_history['LINE-3-FROM_DATE']:''; ?></td>
            <td class="lft btm"><?php echo ($bill_history['LINE-3-TO_DATE']!='31-12-1969')?$bill_history['LINE-3-TO_DATE']:''; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-3-SCM']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-3-AMOUNT']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-3-REMARKS']; ?></td>
            <td colspan="3" rowspan="2" class="lft btm bghd"><strong>Total</strong></td>
            <td rowspan="2" class="lft btm bghd"><u><?php echo $bill['current_charges']; ?></u></td>
         </tr>
         <tr>
            <td class="btm"><?php echo ($bill_history['LINE-4-FROM_DATE']!='31-12-1969')?$bill_history['LINE-4-FROM_DATE']:''; ?></td>
            <td class="lft btm"><?php echo ($bill_history['LINE-4-TO_DATE']!='31-12-1969')?$bill_history['LINE-4-TO_DATE']:''; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-4-SCM']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-4-AMOUNT']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-4-REMARKS']; ?></td>
         </tr>
         <tr>
            <td class="btm"><?php echo ($bill_history['LINE-5-FROM_DATE']!='31-12-1969')?$bill_history['LINE-5-FROM_DATE']:''; ?></td>
            <td class="lft btm"><?php echo ($bill_history['LINE-5-TO_DATE']!='31-12-1969')?$bill_history['LINE-5-TO_DATE']:''; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-5-SCM']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-5-AMOUNT']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-5-REMARKS']; ?></td>
            <td colspan="3" class="lft btm"><strong>Adjustments</strong></td>
            <td class="lft btm"><strong><?php echo number_format($bill['due_amount']-$bill['current_charges'],2); ?></strong></td>
         </tr>
         <tr>
            <td class="btm"><?php echo ($bill_history['LINE-6-FROM_DATE']!='31-12-1969')?$bill_history['LINE-6-FROM_DATE']:''; ?></td>
            <td class="lft btm"><?php echo ($bill_history['LINE-6-TO_DATE']!='31-12-1969')?$bill_history['LINE-6-TO_DATE']:''; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-6-SCM']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-6-AMOUNT']; ?></td>
            <td class="lft btm"><?php echo $bill_history['LINE-6-REMARKS']; ?></td>
            <td colspan="3" class="lft btm bghd"><strong>Total Amount Payable</strong></td>
            <td class="lft btm bghd"><strong><?php echo number_format($bill['due_amount'],2);//$total_final_amount; ?></strong></td>
         </tr>
         <tr>
            <td colspan="9">&nbsp;</td>
         </tr>
         <tr>
            <td colspan="7" class="btm rht top">Only A/c payee cheques are accepted. You can make payment by    cheque only at GGL office / drop box provided at various convenient locations mentioned at our website: www.gglonline.net<br />
               <strong>CASH PAYMENTS ARE ONLY ALLOWED AT YES BANK BRANCHES</strong><br />
               <strong>CHEQUE TO BE DRAWN IN FAVOUR OF GREEN GAS LIMITED</strong>
            </td>
            <td colspan="2" rowspan="3" align="center" valign="middle"><strong>For, Green Gas Limited</strong><img src="../../media/company/<?php echo $company['signature']; ?>" alt="" width="197" height="102" /><br>(Authorised Signatory)</td>
         </tr>
       <tr>
            <td colspan="7"><strong><u>Terms &amp; conditions.</u></strong></td>
         </tr>
            <!--<tr>
            <td colspan="7">1. Payment shall be  accepted by cheque/DD/Online. No Cash is accepted</td>
         </tr>
         <tr>
            <td colspan="7">2. Please issue  crossed cheque in favour of 'Green Gas Limited' for all payments.</td>
         </tr>
         <tr>
            <td height="22" colspan="7">3. Payment delay    charges @2% on monthly basis to be charged on     Invoice value. Subject to minimum of Rs-50/- P.M.</td>
         </tr>
         <tr>
            <td colspan="7">4. Minimum Billing Charges Rs-50/- P.M.</td>
         </tr>
         <tr>
            <td colspan="7">5. For any complaints/ suggestions please Contact on- <?php echo $company['complain_no']; ?></td>
         </tr>
         <tr>
            <td colspan="7">6. At present PNG @    Rs <?php //echo $company['png_rate1']; ?>/scm, Upto 45SCM    bimonthly consumption(i.e. 0.75 scm/day). Beyond this PNG @ Rs <?php //echo $company['png_rate2']; ?>/scm.</td>
         </tr> -->
         <tr><td colspan="7" ><?php echo $company['terms_condition']; ?></td></tr>
         <tr>
            <td colspan="9" class="btmdot">&nbsp;</td>
         </tr>
         <tr>
            <td colspan="9" align="center"><strong><?php echo $company['name']; ?></strong></td>
         </tr>
         <tr>
            <td colspan="9" align="center"><strong><?php echo $company['address1'].' '.$company['address2'].' '.$city['city_name'].'-'.$company['pin_no'].', <br>'.$state['state_name'].', '.$country['country_name']; ?></strong></td>
         </tr>
         <tr>
            <td colspan="9" align="center"><strong><u>PAYMENT SLIP (To be filled by customer &amp; attached with cheque/DD)</u></strong></td>
         </tr>
         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td align="center"><strong>New CRN: </strong></td>
            <td align="center"><strong><?php echo $bill['crn']; ?></strong></td>
            <td></td>
            <td>Meter No.</td>
            <td class="bdr"><?php echo $bill['meter_no']; ?></td>
            <td></td>
            <td><strong>Invoice No.</strong></td>
            <td colspan="2"><strong><?php echo $bill['invoice_no']; ?> </strong></td>
         </tr>
         <tr>
            <td align="center">Name:</td>
            <td align="center"><?php echo $bill['name']; ?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Bank Name:</td>
            <td colspan="2" class="btm"></td>
         </tr>
         <tr>
            <td colspan="2">Cheque/ Demand Draft No.:</td>
            <td colspan="3" class="btm"></td>
            <td></td>
            <td>Branch name:</td>
            <td colspan="2" class="btm"></td>
         </tr>
         <tr>
            <td>Dated:</td>
            <td colspan="3" class="btm"></td>
            <td></td>
            <td></td>
            <td>Amount</td>
            <td colspan="2" class="btm"></td>
         </tr>
         <tr>
            <td colspan="9">The Cheque/DD realisation date will be considered as date of payment. For immediate realisation kindly make online payment.</td>
         </tr>
      </table>
      <center>
      <table class="hide_print">
        <tr>
          <td></td>
          	
          <td><a class="button btn btn-primary" href="../../../ggluser/crnAjax.php?crn_no=<?php echo $bill['crn']; ?>"><strong>Pay Online</strong></a></td>
         
          <td><a class="button" style="cursor: pointer;" onclick="window.print();"><strong>Print</strong></a></td>
          <td></td>
        </tr>
      </table></center>
   </body>
</html>

