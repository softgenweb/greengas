<?php
include('configuration.php');

$current_date = date('Y-m-d');

 $sel_date = mysql_query("SELECT * FROM `weight_measurement` WHERE status='1' and w_m_done_date !='' and w_m_due_date !='' ORDER BY `weight_measurement`.`id` ASC");
 
	while($fetch_date = mysql_fetch_array($sel_date)) {
	
	$due_date  = date('Y-m-d',strtotime($fetch_date['w_m_due_date']));
	$now = strtotime($current_date); 
	$your_date = strtotime($due_date);
	$diff=  $your_date- $now;
	$total_day=floor(($diff/(60*60*24))-1);
	
	$done_date  = date('Y-m-d',strtotime($fetch_date['w_m_done_date']));	
	$your_done_date = strtotime($done_date);
	$done_diff=  $your_done_date- $now;
	$total_done_day=floor(($done_diff/(60*60*24))-1);
	
	$fetch_station = mysql_fetch_array(mysql_query("SELECT * FROM `master_station` WHERE `id`= '".$fetch_date['station_id']."'"));
	$station_name = $fetch_station['name'];
	$equipment = $fetch_date['equipment'];
	$serial_no = $fetch_date['serial_no'];
	$vendor = $fetch_date['vendor'];
	$side =  $fetch_date['side'];
	$wm_done =  $fetch_date['w_m_done_date'];
	$wm_due =  $fetch_date['w_m_due_date'];
	
if($total_day > '0') {
		
	if($total_day == '45')  
	{
		$fetch_mail_id = mysql_query("SELECT * FROM `users` WHERE status ='1' and department_id IN('2','5')");
		while($fetch_mail = mysql_fetch_array($fetch_mail_id)) {
		$note= 'Before 45 days of "W&M Due date"';
		$to = $fetch_mail['email'];
		//$to = 'nagesh.k@softgentechnologies.com,uday.s@softgentechnologies.com';
		$subject = '45 days of "W&M Due date"';
		$message = "Following dispenser W&M verification is due so kindly submit the fees to concerned area of W&M";
		$message .= "\n\nStation name = ".$station_name."\nEquipment Details = ".$equipment."\nSerial No = ".$serial_no."\nVendor name = ".$vendor."\nSide = ".$side."\nW&M  Done Date = ".$wm_done."\nW&M Due Date = ".$wm_due.".\n\n";		
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$note.'<customercare.lko@gglonline.net>'.  "\r\n";
		//$headers .= 'Reply-To: '.$to.'' . "\r\n";
		mail($to, $subject, $message, $headers);
		}
	}
	
	if($total_day == '30') 
	{
		$fetch_mail_id = mysql_query("SELECT * FROM `users` WHERE status ='1' and department_id IN('2','5')");
		while($fetch_mail = mysql_fetch_array($fetch_mail_id)) {
		$note= 'Before 30 days of "W&M Due date"';
		$to = $fetch_mail['email'];
		//$to = 'nagesh.k@softgentechnologies.com,uday.s@softgentechnologies.com';
		$subject = '30 days of "W&M Due date"';
		$message = "Please upload details of verification fee against following dispenser";
		$message .= "\n\nStation name = ".$station_name."\nEquipment Details = ".$equipment."\nSerial No = ".$serial_no."\nVendor name = ".$vendor."\nSide = ".$side."\nW&M  Done Date = ".$wm_done."\nW&M Due Date = ".$wm_due.".\n\n";		
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$note.'<customercare.lko@gglonline.net>'.  "\r\n";
		//$headers .= 'Reply-To: '.$to.'' . "\r\n";
		mail($to, $subject, $message, $headers);
		}
	}
	
	if($total_day == '7') 
	{
		$fetch_mail_id = mysql_query("SELECT * FROM `users` WHERE status ='1' and department_id IN('2','5','1')");
		while($fetch_mail = mysql_fetch_array($fetch_mail_id)) {
		$note= 'Before 7 days of "W&M Due date"';
		$to = $fetch_mail['email'];
		$subject = '7 days of "W&M Due date"';
		$to = 'nagesh.k@softgentechnologies.com,uday.s@softgentechnologies.com';
		$message = "W&M stamping verification done or not. If done then please upload the details. If not done then take necessary approvals from concerned department\nDispenser detail-";
		$message .= "\n\nStation name = ".$station_name."\nEquipment Details = ".$equipment."\nSerial No = ".$serial_no."\nVendor name = ".$vendor."\nSide = ".$side."\nW&M  Done Date = ".$wm_done."\nW&M Due Date = ".$wm_due.".\n\n";		
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$note.'<customercare.lko@gglonline.net>'.  "\r\n";
		//$headers .= 'Reply-To: '.$to.'' . "\r\n";
		mail($to, $subject, $message, $headers);
		}
	}
}
	
	
	if($total_done_day == '6') 
	{
		$fetch_mail_id = mysql_query("SELECT * FROM `users` WHERE status ='1' and department_id IN('2','5','1')");
		while($fetch_mail = mysql_fetch_array($fetch_mail_id)) {
		$note= 'After 7 days of "W&M Done date"';
		$to = $fetch_mail['email'];
		//$to = 'nagesh.k@softgentechnologies.com,uday.s@softgentechnologies.com';
		$subject = 'After 7 days of "W&M Done date"';
		$message = "Please upload the stamped and verified certificate duly approved by W&M Inspector.\nDispenser detail-";
		$message .= "\n\nStation name = ".$station_name."\nEquipment Details = ".$equipment."\nSerial No = ".$serial_no."\nVendor name = ".$vendor."\nSide = ".$side."\nW&M  Done Date = ".$wm_done."\nW&M Due Date = ".$wm_due.".\n\n";		
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/plain; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: '.$note.'<customercare.lko@gglonline.net>'.  "\r\n";
		//$headers .= 'Reply-To: '.$to.'' . "\r\n";
		mail($to, $subject, $message, $headers);
		}
	}
}


?> 